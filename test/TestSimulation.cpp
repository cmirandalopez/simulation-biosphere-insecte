/*!
 * \file TestSimulation.cpp
 * \brief Tests unitaires de la classe Simulation
 * \author Carlos Miranda
 * \version 0.1
 */

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <netinet/in.h>
#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <exception>
#include "Simulation.hpp"
#include "Environnement.hpp"
#include "Abeille.hpp"
#include "Fourmi.hpp"
#include "Puceron.hpp"
#include "OeufDePuceron.hpp"
using namespace CppUnit;
using namespace std;

/** \class TestSimulation
 *  \brief Classe implémentant les tests unitaires de la classe Simulation.
 *
 */
class TestSimulation : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestSimulation);
    CPPUNIT_TEST(testConstructeur1);
    CPPUNIT_TEST(testConstructeur2);
    CPPUNIT_TEST(testConstructeur3);
    CPPUNIT_TEST(testConstructeur4);
    CPPUNIT_TEST(testConstructeur5);
    CPPUNIT_TEST(testInitRuches);
    CPPUNIT_TEST(testInitFourmilieres);
    CPPUNIT_TEST(testInitFleurs);
    CPPUNIT_TEST(testInitAgents);
    CPPUNIT_TEST_SUITE_END();

 public:
        void setUp(void) {}
        void tearDown(void) {}

 protected:
        void testConstructeur1(void);
        void testConstructeur2(void);
        void testConstructeur3(void);
        void testConstructeur4(void);
        void testConstructeur5(void);
        void testInitRuches(void);
        void testInitFourmilieres(void);
        void testInitFleurs(void);
        void testInitAgents(void);

 private:
        Environnement *environnement_10_5, *environnement_1_1;
        std::vector<Abeille *> *abeilles;
        std::vector<Fourmi *> *fourmis;
        std::vector<Puceron *> *pucerons;
};

/**
 * \brief Test du constructeur avec paramètres (sert comme test d'intégration aussi)
 *
 * Résultats attendus : hauteur de l'environnement = 20, longueur de l'environnement = 20, nombre d'abeilles = 10, nombre de fourmis = 9, nombre de puceron = 8
 */
void TestSimulation::testConstructeur1(void) {
    Simulation simulation(20, 19, 10, 9, 8);
    CPPUNIT_ASSERT(20 == simulation.get_environnement()->getHauteur());
    CPPUNIT_ASSERT(19 == simulation.get_environnement()->getLongueur());
    CPPUNIT_ASSERT(10 == simulation.get_abeilles().size());
    CPPUNIT_ASSERT(9 == simulation.get_fourmis().size());
    CPPUNIT_ASSERT(8 == simulation.get_pucerons().size());
}

/**
 * \brief Test du constructeur avec paramètres
 *
 * Dans ce cas, on crée une grille trop petite pour contenir les agents.
 *
 * Résultat attendu : une exception est levée
 */
void TestSimulation::testConstructeur2(void)
{
    bool exception_levee = false;
    try {
        Simulation simulation(5, 5, 10, 10, 6);
    } catch (...) {
        exception_levee = true;
    }

    CPPUNIT_ASSERT(exception_levee);
}

/**
 * \brief Test du constructeur par défaut
 *
 * Résultat attendu : un environnement vide, des listes d'agents vides.
 */
void TestSimulation::testConstructeur3(void)
{
    Simulation simulation;

    CPPUNIT_ASSERT(0 == simulation.get_environnement()->getHauteur());
    CPPUNIT_ASSERT(0 == simulation.get_environnement()->getLongueur());
    CPPUNIT_ASSERT(simulation.get_abeilles().empty());
    CPPUNIT_ASSERT(simulation.get_fourmis().empty());
    CPPUNIT_ASSERT(simulation.get_pucerons().empty());
}

/**
 * \brief Test du constructeur par défaut
 * 
 * On test le constructeur par défaut mais aussi les mutateurs et accesseurs. On crée un environnement
 * et des listes d'agents qu'on affecte à la simulation vide, puis on teste que les pointeurs
 * pointent vers les mêmes adresses mémoire, car on ne veut pas des copies mais les objets eux mêmes.
 *
 * Résultat attendu : le même environnement et les mêmes listes d'agents.
 */
void TestSimulation::testConstructeur4(void)
{
    Simulation simulation;
    environnement_10_5 = new Environnement(10, 5);
    simulation.set_environnement(environnement_10_5);
    abeilles = new std::vector<Abeille *>(3);
    fourmis = new std::vector<Fourmi *>(4);
    pucerons = new std::vector<Puceron *>(5);
    simulation.set_abeilles(abeilles);
    simulation.set_fourmis(fourmis);
    simulation.set_pucerons(pucerons);

    CPPUNIT_ASSERT(environnement_10_5->getHauteur() == simulation.get_environnement()->getHauteur());
    CPPUNIT_ASSERT(environnement_10_5->getLongueur() == simulation.get_environnement()->getLongueur());
    CPPUNIT_ASSERT(abeilles->size() == simulation.get_abeilles().size());
    CPPUNIT_ASSERT(fourmis->size() == simulation.get_fourmis().size());
    CPPUNIT_ASSERT(pucerons->size() == simulation.get_pucerons().size());

    // On teste que les adresses des objets soient les mêmes (on veut exactement les mêmes objets)
    for (int i = 0; i < environnement_10_5->getHauteur(); ++i) {
        for (int j = 0; j < environnement_10_5->getLongueur(); ++j) {
            CPPUNIT_ASSERT(environnement_10_5->getCaseAt(j, i) == simulation.get_environnement()->getCaseAt(j, i));
        }
    }

    for (unsigned i = 0; i < abeilles->size(); ++i) {
        CPPUNIT_ASSERT((*abeilles)[i] == simulation.get_abeilles()[i]);
    }

    for (unsigned i = 0; i < fourmis->size(); ++i) {
        CPPUNIT_ASSERT((*fourmis)[i] == simulation.get_fourmis()[i]);
    }

    for (unsigned i = 0; i < pucerons->size(); ++i) {
        CPPUNIT_ASSERT((*pucerons)[i] == simulation.get_pucerons()[i]);
    }
}


/**
 * \brief Test du constructeur par défaut
 *
 * Dans ce cas, on passe un environnement avec un grille trop petite pour contenir tous les agents
 *
 * Résultat attendu : Une exception est levée
 */
void TestSimulation::testConstructeur5(void)
{
    bool exception_levee = false;
    Simulation simulation;
    environnement_1_1 = new Environnement(1, 1);
    simulation.set_environnement(environnement_1_1);
    abeilles = new std::vector<Abeille *>;
    fourmis = new std::vector<Fourmi *>;
    pucerons = new std::vector<Puceron *>;
    abeilles->push_back(new Abeille(1, Position(0, 0), 1, 0));
    fourmis->push_back(new Fourmi(1, Position(0, 0), 1, NULL));
    pucerons->push_back(new Puceron(1, Position(0, 0), 1));
    try {
        simulation.set_abeilles(abeilles);
        simulation.set_fourmis(fourmis);
        simulation.set_pucerons(pucerons);
    }catch (...) {
        exception_levee = true;
    }

    CPPUNIT_ASSERT(exception_levee);
}

/**
 * \brief Test d'initialisation des ruches
 *
 * On teste le placement des ruches sur un environnement.
 *
 * Résultat attendu : Le nombre de cases de type Ruche coincide avec le nombre de ruches 
 * à ajouter.
 */
void TestSimulation::testInitRuches(void) {
    Simulation simulation;
    environnement_10_5 = new Environnement(10, 5);
    simulation.set_environnement(environnement_10_5);
    simulation.nb_ruches_init = 10;
    simulation.init_ruches();
    int cnt = 0;

    for (auto l : simulation.get_environnement()->getGrille()) {
        for (auto &c : l) {
            if (c->getTypeCase() == std::string("Ruche")) {
                cnt++;
            }
        }
    }

    CPPUNIT_ASSERT(10 == cnt);
}

/**
 * \brief Test d'initialisation des fourmilières
 *
 * On teste le placement des fourmilières sur un environnement.
 *
 * Résultat attendu : Le nombre de cases de type Fourmiliere coincide avec le nombre de fourmilières 
 * à ajouter.
 */
void TestSimulation::testInitFourmilieres(void) {
    Simulation simulation;
    environnement_10_5 = new Environnement(10, 5);
    simulation.set_environnement(environnement_10_5);
    simulation.nb_fourmilieres_init = 10;
    simulation.init_fourmilieres();
    int cnt = 0;

    for (auto l : simulation.get_environnement()->getGrille()) {
        for (auto &c : l) {
            if (c->getTypeCase() == std::string("Fourmiliere")) {
                cnt++;
            }
        }
    }

    CPPUNIT_ASSERT(10 == cnt);
}

/**
 * \brief Test d'initialisation des fleurs
 *
 * On teste le placement des fleurs sur un environnement.
 *
 * Résultat attendu : Le nombre de cases de type Fleur coincide avec le nombre de fleurs 
 * à ajouter.
 */
void TestSimulation::testInitFleurs(void) {
    Simulation simulation;
    environnement_10_5 = new Environnement(10, 5);
    simulation.set_environnement(environnement_10_5);
    simulation.nb_fleurs_init = 10;
    simulation.init_fleurs();
    int cnt = 0;

    for (auto l : simulation.get_environnement()->getGrille()) {
        for (auto &c : l) {
            if (c->getTypeCase() == std::string("Fleur")) {
                cnt++;
            }
        }
    }
    CPPUNIT_ASSERT(10 == cnt);
}

/**
 * \brief Test d'initialisation des agents
 *
 * On teste le placement d'agents sur un environnement. On teste si bien le nombre total que l'effectif
 * de chaque espèce.
 *
 * Résultat attendu : Le nombre d'agents coincide avec le nombre d'agents à ajouter.
 */
void TestSimulation::testInitAgents(void) {
    Simulation simulation;
    environnement_10_5 = new Environnement(10, 5);
    simulation.set_environnement(environnement_10_5);
    simulation.nb_abeilles_init = 10;
    simulation.nb_fourmis_init = 10;
    simulation.nb_pucerons_init = 10;
    simulation.init_agents();
    int cnt = 0, cnt_a = 0, cnt_f = 0, cnt_p = 0;

    for (auto l : simulation.get_environnement()->getGrille()) {
        for (auto &c : l) {
            if (c->getAgent() != NULL) {
                cnt++;
                if (c->getAgent()->getTypeAgent() == std::string("Abeille")) {
                    cnt_a++;
                } else if (c->getAgent()->getTypeAgent() == std::string("Fourmi")) {
                    cnt_f++;
                } else if (c->getAgent()->getTypeAgent() == std::string("Puceron")) {
                    cnt_p++;
                }
            }
        }
    }

    CPPUNIT_ASSERT(30 == cnt);
    CPPUNIT_ASSERT(10 == cnt_a);
    CPPUNIT_ASSERT(10 == cnt_f);
    CPPUNIT_ASSERT(10 == cnt_p);
}

CPPUNIT_TEST_SUITE_REGISTRATION(TestSimulation);

int main(int argc, char* argv[]) {

	// informs test-listener about testresults
	CPPUNIT_NS::TestResult testresult;
	// register listener for collecting the test-results
	CPPUNIT_NS::TestResultCollector collectedresults;
	testresult.addListener (&collectedresults);
	// register listener for per-test progress output
	CPPUNIT_NS::BriefTestProgressListener progress;
	testresult.addListener (&progress);
	// insert test-suite at test-runner by registry
	CPPUNIT_NS::TestRunner testrunner;
	testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
	testrunner.run(testresult);
	// output results in compiler-format
	CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
	compileroutputter.write ();
	// return 0 if tests were successful
	return collectedresults.wasSuccessful() ? 0 : 1;
}
