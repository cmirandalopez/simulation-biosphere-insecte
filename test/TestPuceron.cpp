/*!
 * \file TestPuceron.cpp
 * \brief Tests unitaires de la classe Puceron
 * \author FRAGNAUD Cedric
 * \author MARCUSANU Ana
 * \author PROTAIS Francois
 * \version 0.1
 */
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <netinet/in.h>
#include <iostream>
#include <string>
#include <list>
#include "Puceron.hpp"
#include "Abeille.hpp"
#include "Fourmi.hpp"
#include "Position.hpp"
#include "Fleur.hpp"
#include "Ruche.hpp"
#include "OeufDePuceron.hpp"
#include "Fourmiliere.hpp"
// #include "Environnement.hpp"

using namespace CppUnit;
using namespace std;

// -----------------------------------------------------------------------------
/*! \class TestPuceron
     * \brief Classe implémentant les tests unitaires de la classe Puceron.
     *
     */
class TestPuceron : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestPuceron);
    CPPUNIT_TEST(testmanger);
    CPPUNIT_TEST(testBouger);
    CPPUNIT_TEST(testVoirEnvironnementVide);
    CPPUNIT_TEST(testVoirEnvironnementPlein);
    CPPUNIT_TEST(testVoirEnvironnement_Puceron);
    CPPUNIT_TEST(testVoirEnvironnement_Abeille);
    CPPUNIT_TEST(testVoirEnvironnement_Fourmi);
    CPPUNIT_TEST(testVoirEnvironnement_Ruche);
    CPPUNIT_TEST(testVoirEnvironnement_Fourmiliere);
    CPPUNIT_TEST(testVoirEnvironnement_OeufDePuceron);
    CPPUNIT_TEST(testVoirEnvironnement_Fleur);
    CPPUNIT_TEST(testVoirEnvironnement_Alveole);
    CPPUNIT_TEST(testDecider1);
    CPPUNIT_TEST(testDecider2);
    CPPUNIT_TEST(testattaquer);
    CPPUNIT_TEST_SUITE_END();

 public:
    void setUp(void);
    void tearDown(void);
 protected:
    void testmanger(void);
    void testBouger(void);
    void testVoirEnvironnementVide(void);
    void testVoirEnvironnementPlein(void);
    void testVoirEnvironnement_Puceron(void);
    void testVoirEnvironnement_Abeille(void);
    void testVoirEnvironnement_Fourmi(void);
    void testVoirEnvironnement_Ruche(void);
    void testVoirEnvironnement_Fourmiliere(void);
    void testVoirEnvironnement_OeufDePuceron(void);
    void testVoirEnvironnement_Fleur(void);
    void testVoirEnvironnement_Alveole(void);
    void testDecider1(void);
    void testDecider2(void);
    void testattaquer(void);


 private:
    Puceron *P_100_7x_8y_N_10;    /*! Puceron avec 100 PdV, a la case (7, 8) face au nord, avec 10 pt d'attaque */
    Puceron *P_90_10x_8y_S_10;  /*! Puceron avec 90 PdV, a la case (10, 8) face au sud, avec 10 pt d'attaque */
};

// -----------------------------------------------------------------------------
void TestPuceron::setUp() {
    P_100_7x_8y_N_10 = new Puceron(100, Position(7, 8, 'N'), 10);
    P_90_10x_8y_S_10 = new Puceron(90, Position(10, 8, 'S'), 10);
}

void TestPuceron::tearDown() {
    delete P_100_7x_8y_N_10;
    delete P_90_10x_8y_S_10;
}

/*!
 *\brief Test la methode manger pour un cas nominal
 *
 * Resultat attendu tempsDEvolution fleur : 2
 */
void TestPuceron::testmanger(void) {
    Environnement *e = new Environnement(20, 20);
    Position pos(7, 7, 'N');
    Fleur *fleur = new Fleur (10, pos, 3);
    P_100_7x_8y_N_10->manger(fleur, e);
    CPPUNIT_ASSERT(2 == fleur->getTempsDEvol());
}

/*!
 *\brief Test de la methode bouger
 *
 * Resultat attendu : case ou l'agent se trouavait ne contienne plsu d'Agent
 *                    case(7, 7) contienne l'Agent
 *                    la postion de l'agent soit la nouvelle
 */
void TestPuceron::testBouger(void) {
    Environnement *e = new Environnement (20, 20);
    Position p(7, 7, 'N');
    P_100_7x_8y_N_10->bouger(p, e);
    CPPUNIT_ASSERT(p.getX() == P_100_7x_8y_N_10->getPosition().getX());
    CPPUNIT_ASSERT(p.getY() == P_100_7x_8y_N_10->getPosition().getY());
    CPPUNIT_ASSERT(p.getOrientation() == P_100_7x_8y_N_10->getPosition().getOrientation());
    CPPUNIT_ASSERT(e->getAgentAt(7, 8) == NULL);
    CPPUNIT_ASSERT(e->getAgentAt(7, 7) == P_100_7x_8y_N_10);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : grille vide = l'agent ne voit rien
 *
 */
void TestPuceron::testVoirEnvironnementVide(void) {
    // Environnement vide
    Environnement *e1 = new Environnement (20, 20);
    e1->setAgentAt(7, 8, P_100_7x_8y_N_10);
    std::list<Case*> ve1;
    // test de environnement vide
    ve1 = P_100_7x_8y_N_10->voirEnvironnement(e1);
    CPPUNIT_ASSERT(ve1.size() == 0);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : grille pleine de pucerons = l'agent voit les cases (6, 6) (7, 6) (8, 6) (6, 7) (7, 7) (8, 7)
 */
void TestPuceron::testVoirEnvironnementPlein(void) {
    // Environnement plein de pucerons
    Environnement *e3 = new Environnement (20, 20);
    for (int i = 0 ; i < 20 ; i++) {
        for (int j = 0 ; j < 20 ; j++) {
            Puceron *pus = new Puceron(100, Position(j, i, 'N'), 10);
            e3->setAgentAt(j, i, pus);
        }
    }
    e3->setAgentAt(7, 8, P_100_7x_8y_N_10);
    std::list<Case*> ve1;
    // test environnement plein de puceron
    ve1 = P_100_7x_8y_N_10->voirEnvironnement(e3);
    bool valider;
    int x;
    int y;
    valider = true;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if ((x != 6 || y != 6) && (x != 7 || y != 6) && (x != 7 || y != 6) && (x != 8 || y != 6)
              && (x != 6 || y != 7) && (x != 7 || y != 7) && (x != 8 || y != 7) && (x != 6 || y != 8) && (x != 8 || y != 8)) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 8);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit l'abeille dans (6, 6)
 */
void TestPuceron::testVoirEnvironnement_Puceron(void) {
    Environnement *e2 = new Environnement (20, 20);
    Puceron *pus = new Puceron(100, Position(6, 6, 'N'), 10);
    e2->setAgentAt(6, 6, pus);
    e2->setAgentAt(7, 8, P_100_7x_8y_N_10);
    std::list<Case*> ve1;
    ve1 = P_100_7x_8y_N_10->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 6 || y != 6) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit l'abeille dans (6, 6)
 */
void TestPuceron::testVoirEnvironnement_Abeille(void) {
    Environnement *e2 = new Environnement (20, 20);
    Abeille  *ab = new Abeille(100, Position(6, 6, 'N'), 10, 10);
    e2->setAgentAt(6, 6, ab);
    e2->setAgentAt(7, 8, P_100_7x_8y_N_10);
    std::list<Case*> ve1;
    ve1 = P_100_7x_8y_N_10->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 6 || y != 6) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit la fourmi dans (7, 6)
 */
void TestPuceron::testVoirEnvironnement_Fourmi(void) {
    Environnement *e2 = new Environnement (20, 20);
    Fourmi *fou = new Fourmi(100, Position(7, 6, 'N'), 10, NULL);
    e2->setAgentAt(7, 6, fou);
    e2->setAgentAt(7, 8, P_100_7x_8y_N_10);
    std::list<Case*> ve1;
    ve1 = P_100_7x_8y_N_10->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 7 || y != 6) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit la ruche dans (7, 6)
 */
void TestPuceron::testVoirEnvironnement_Ruche(void) {
    Environnement *e2 = new Environnement (20, 20);
    Ruche *ru = new Ruche(7, 6);
    e2->setCaseAt(7, 6, ru);
    e2->setAgentAt(7, 8, P_100_7x_8y_N_10);
    std::list<Case*> ve1;
    ve1 = P_100_7x_8y_N_10->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 7 || y != 6) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit la Fourmiliere dans (7, 6)
 */
void TestPuceron::testVoirEnvironnement_Fourmiliere(void) {
    Environnement *e2 = new Environnement (20, 20);
    Fourmiliere *f = new Fourmiliere(7, 6);
    e2->setCaseAt(7, 6, f);
    e2->setAgentAt(7, 8, P_100_7x_8y_N_10);
    std::list<Case*> ve1;
    ve1 = P_100_7x_8y_N_10->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 7 || y != 6) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit la OeufDePuceron dans (7, 7)
 */
void TestPuceron::testVoirEnvironnement_OeufDePuceron(void) {
    Environnement *e2 = new Environnement (20, 20);
    OeufDePuceron *o = new OeufDePuceron(false, false, 10, Position(7, 7, 'N'));
    e2->setCaseAt(7, 7, o);
    e2->setAgentAt(7, 8, P_100_7x_8y_N_10);
    std::list<Case*> ve1;
    ve1 = P_100_7x_8y_N_10->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 7 || y != 7) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit la Fleur dans (8, 7)
 */
void TestPuceron::testVoirEnvironnement_Fleur(void) {
    Environnement *e2 = new Environnement (20, 20);
    Fleur *fle = new Fleur(100, Position(8, 7, 'N'), 10);
    e2->setCaseAt(8, 7, fle);
    e2->setAgentAt(7, 8, P_100_7x_8y_N_10);
    std::list<Case*> ve1;
    ve1 = P_100_7x_8y_N_10->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 8 || y != 7) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit l'Alveole dans (7, 7)
 */
void TestPuceron::testVoirEnvironnement_Alveole(void) {
    Environnement *e2 = new Environnement (20, 20);
    Position pos(8, 7, 'N');
    Alveole *a = new Alveole(10, 10, true, pos, 10);
    e2->setCaseAt(8, 7, a);
    e2->setAgentAt(7, 8, P_100_7x_8y_N_10);
    std::list<Case*> ve1;
    ve1 = P_100_7x_8y_N_10->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 8 || y != 7) {
                    valider = false;
        }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}


/*!
 *\brief Test de la methode decider lorsqu'on lui passe une liste de cases vide
 *
 * Resultat attendu : toute décision sauf "erreur"
 */
void TestPuceron::testDecider1(void) {
     list<Case *> *listeCases = new list<Case *>;
     CPPUNIT_ASSERT(P_100_7x_8y_N_10->decider(listeCases) != "ERREUR");
}

/*!
 *\brief Test de la methode decider lorsqu'on lui passe une liste de cases contenant une alveole et une fleur
 *
 * Resultat attendu : toute décision sauf "erreur"
 */
void TestPuceron::testDecider2(void) {
    list<Case *> *listeCases = new list<Case *>;
    Position posf(7, 7);
    Position posa(9, 7);
    Fleur *fleur = new Fleur (0, posf, 2);
    Alveole *alveole = new Alveole (3, 6, false, posa, 10);
    listeCases->push_back(fleur);
    listeCases->push_back(alveole);
    CPPUNIT_ASSERT(P_100_7x_8y_N_10->decider(listeCases) != "ERREUR");
}

/*!
 *\brief Test de la methode attaquer pour un cas nominal
 *
 * Résultat attendu : nombre de points de vie de l'agent attaqué = son nombre de points de vie initial - le nombre de points d'attaque
 */
void TestPuceron::testattaquer(void)
{
  Agent *a = new Agent (20, Position(10, 9, 'N'), 15);
    P_100_7x_8y_N_10->attaquer(a);
    CPPUNIT_ASSERT(20-P_100_7x_8y_N_10->getPA() == a->getPV());
}

// -----------------------------------------------------------------------------
CPPUNIT_TEST_SUITE_REGISTRATION(TestPuceron);
int main(int argc, char* argv[]) {
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;
    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener(&collectedresults);
    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener(&progress);
    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
    testrunner.run(testresult);
    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write();
    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
