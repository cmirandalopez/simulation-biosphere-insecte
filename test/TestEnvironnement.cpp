/*!
 * \file TestEnvironnement.cpp
 * \brief Tests unitaires de la classe Environnement
 * \author Jorge OCHOA
 * \version 0.1
 */

#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <netinet/in.h>
#include "Environnement.hpp"
#include "Case.hpp"
#include "Puceron.hpp"
using namespace CppUnit;
using namespace std;
//using std::vector;

//-----------------------------------------------------------------------------
/*! \class TestCase
	 * \brief Classe implémentant les tests unitaires de la classe Environnement.
	 *
	 */
class TestEnvironnement : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(TestEnvironnement);
	CPPUNIT_TEST(testgetHauteur);
	CPPUNIT_TEST(testgetLongeur);
	CPPUNIT_TEST(testgetGrille);
	CPPUNIT_TEST(testsetCaseAt_getCaseAt);
	CPPUNIT_TEST(testsetAgentAt_getAgentAt);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp(void);
	void tearDown(void);
protected:
	void testgetHauteur(void);
	void testgetLongeur(void);
	void testgetGrille(void);
	void testsetCaseAt_getCaseAt(void);
	void testsetAgentAt_getAgentAt(void);
private:
	Environnement *Environnement_10_5_g; // Environnement 10 en longeur, 5 en hauteur et de grille g
	vector< vector<Case*>  > g;
	//Case g[10][5];
};

//-----------------------------------------------------------------------------
void TestEnvironnement::setUp() {
		vector< vector<Case*>  > te(10, vector<Case*>(5));
		int i, j;

    for (i = 0; i < 10; ++i) {
        for (j = 0; j < 5; ++j) {
            te[i][j] = new Case(j, i);
        }
    }
		TestEnvironnement::g = te;
		Environnement_10_5_g = new Environnement(10, 5, TestEnvironnement::g);
}

void TestEnvironnement::tearDown() {
    delete Environnement_10_5_g;
}

/*!
		 *\brief Test la méthode getHauteur
		 *
		 * Resultat attendu : hauteur == 5
		 */
void TestEnvironnement::testgetHauteur(void)
{
	CPPUNIT_ASSERT(5 == Environnement_10_5_g->getHauteur());
}

/*!
		 *\brief Test la méthode getLongeur
		 *
		 * Resultat attendu : longeur == 10
		 */
void TestEnvironnement::testgetLongeur(void)
{
	CPPUNIT_ASSERT(10 == Environnement_10_5_g->getLongueur());
}

/*!
		 *\brief Test la méthode getGrille
		 *
		 * Resultat attendu : grille = g (grille déclarée au début du test)
		 */
void TestEnvironnement::testgetGrille(void)
{
	CPPUNIT_ASSERT(TestEnvironnement::g == Environnement_10_5_g->getGrille());
}

/*!
		 *\brief Test la méthode setCaseAt et getCaseAt
		 *
		 * On veut changer la case à longeur = 2  et hauteur = 3 de la grille
		 * par une nouvelle case c et ensuite recuperer cette case
		 *
		 * Resultat getCaseAt(2, 3) == c
		 */
void TestEnvironnement::testsetCaseAt_getCaseAt(void)
{
	Case c(1, 2);
	Environnement_10_5_g->setCaseAt(2, 3, &c);
	CPPUNIT_ASSERT(&c == Environnement_10_5_g->getCaseAt(2, 3));
}

void TestEnvironnement::testsetAgentAt_getAgentAt(void){
	Puceron *pus = new Puceron(100, Position(2, 4, 'N'), 10);
	CPPUNIT_ASSERT(NULL == Environnement_10_5_g->getAgentAt(2, 4));
	Environnement_10_5_g->setAgentAt(2, 4, pus);
	CPPUNIT_ASSERT(pus == Environnement_10_5_g->getAgentAt(2, 4));
};

//-----------------------------------------------------------------------------
CPPUNIT_TEST_SUITE_REGISTRATION( TestEnvironnement );
int main(int argc, char* argv[])
{
	// informs test-listener about testresults
	CPPUNIT_NS::TestResult testresult;
	// register listener for collecting the test-results
	CPPUNIT_NS::TestResultCollector collectedresults;
	testresult.addListener (&collectedresults);
	// register listener for per-test progress output
	CPPUNIT_NS::BriefTestProgressListener progress;
	testresult.addListener (&progress);
	// insert test-suite at test-runner by registry
	CPPUNIT_NS::TestRunner testrunner;
	testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
	testrunner.run(testresult);
	// output results in compiler-format
	CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
	compileroutputter.write ();
	// return 0 if tests were successful
	return collectedresults.wasSuccessful() ? 0 : 1;
}
