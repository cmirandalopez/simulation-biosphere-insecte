/*!
 * \file TestFourmi.cpp
 * \brief Tests unitaires de la classe Fourmi
 * \author FRAGNAUD Cedric
 * \author MARCUSANU Ana
 * \author PROTAIS Francois
 * \version 0.1
 */
#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <netinet/in.h>
#include "Puceron.hpp"
#include "Abeille.hpp"
#include "Fourmi.hpp"
#include "Position.hpp"
#include "Fleur.hpp"
#include "Ruche.hpp"
#include "OeufDePuceron.hpp"
#include "Fourmiliere.hpp"

using namespace CppUnit;
using namespace std;

// -----------------------------------------------------------------------------
/*! \class TestFourmi
     * \brief Classe implémentant les tests unitaires de la classe Fourmi.
     *
     */
class TestFourmi : public CppUnit::TestFixture {
    CPPUNIT_TEST_SUITE(TestFourmi);
    CPPUNIT_TEST(testgetOeufPorte);
    CPPUNIT_TEST(testvolerUnOeuf1);
    CPPUNIT_TEST(testposerOeuf1);
    CPPUNIT_TEST(testattaquer);
    CPPUNIT_TEST(testBouger);
    CPPUNIT_TEST(testDecider);
    CPPUNIT_TEST(testVoirEnvironnementVide);
    CPPUNIT_TEST(testVoirEnvironnementPlein);
    CPPUNIT_TEST(testVoirEnvironnement_Puceron);
    CPPUNIT_TEST(testVoirEnvironnement_Abeille);
    CPPUNIT_TEST(testVoirEnvironnement_Fourmi);
    CPPUNIT_TEST(testVoirEnvironnement_Ruche);
    CPPUNIT_TEST(testVoirEnvironnement_Fourmiliere);
    CPPUNIT_TEST(testVoirEnvironnement_OeufDePuceron);
    CPPUNIT_TEST(testVoirEnvironnement_Fleur);
    CPPUNIT_TEST(testVoirEnvironnement_Alveole);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);
protected:
    void testgetOeufPorte(void);
    void testvolerUnOeuf1(void);
    void testposerOeuf1(void);
    void testattaquer(void);
    void testBouger(void);
    void testDecider(void);
    void testVoirEnvironnementVide(void);
    void testVoirEnvironnementPlein(void);
    void testVoirEnvironnement_Puceron(void);
    void testVoirEnvironnement_Abeille(void);
    void testVoirEnvironnement_Fourmi(void);
    void testVoirEnvironnement_Ruche(void);
    void testVoirEnvironnement_Fourmiliere(void);
    void testVoirEnvironnement_OeufDePuceron(void);
    void testVoirEnvironnement_Fleur(void);
    void testVoirEnvironnement_Alveole(void);


private:
    Fourmi *F_100_7x_8y_N_10_null;    /*! Fourmi avec 100 PdV, a la case (7, 8) face au nord, avec 10 pt d'attaque, sans oeuf */
    Fourmi *F_90_10x_8y_S_10_oeuf;  /*! Fourmi avec 90 PdV, a la case (10, 8) face au sud, avec 10 pt d'attaque, qui porte un oeuf */
    OeufDePuceron *oeuf;            /*! Oeuf porte par la deuxième fourmi */
};

// -----------------------------------------------------------------------------
void TestFourmi::setUp() {
    Position pos (10, 9, 'N');
    oeuf = new OeufDePuceron(true, false, 4, pos);
    F_100_7x_8y_N_10_null = new Fourmi(100, Position(7, 8, 'N') ,10, NULL);
    F_90_10x_8y_S_10_oeuf = new Fourmi(90, Position(10, 8, 'S'), 10, oeuf);
}

void TestFourmi::tearDown() {
    delete F_100_7x_8y_N_10_null;
    delete F_90_10x_8y_S_10_oeuf;
    delete oeuf;
}

/*!
         *\brief Test la methode getOeufPorte
         *
         * Resultat attendu oeufDePuceron pour la première fourmi: NULL
         * Resultat attendu oeufDePuceron pour la deuxième fourmi: oeuf
         */
void TestFourmi::testgetOeufPorte(void) {
    CPPUNIT_ASSERT(NULL == F_100_7x_8y_N_10_null->getOeufPorte());
    CPPUNIT_ASSERT(oeuf == F_90_10x_8y_S_10_oeuf->getOeufPorte());
}


/*!
         *\brief Test la methode volerUnOeuf pour un cas nominal
         *
         * Resultat attendu oeufDePuceron pour la première fourmi: oeuf2
         * Resultat attendu porte pour oeuf2: true
         */
void TestFourmi::testvolerUnOeuf1(void) {
    Environnement *e = new Environnement (20, 20);
    Position pos(7, 7, 'N');
    OeufDePuceron *oeuf2 = new OeufDePuceron (false, false, 4, pos);
    F_100_7x_8y_N_10_null->volerUnOeuf(oeuf2, e);
    CPPUNIT_ASSERT(oeuf2 == F_100_7x_8y_N_10_null->getOeufPorte());
    CPPUNIT_ASSERT(true == oeuf2->getPorte());
}


/*!
         *\brief Test la methode poserOeuf pour un cas nominal
         *
         * Resultat attendu oeufDePuceron pour la deuxième fourmi: NULL
         * Resultat attendu porte pour oeuf: false
         */
void TestFourmi::testposerOeuf1(void) {
    Environnement *e = new Environnement (20, 20);
    Position pos (10, 9, 'N');
    F_90_10x_8y_S_10_oeuf->poserOeuf(pos, e);
    CPPUNIT_ASSERT(NULL == F_90_10x_8y_S_10_oeuf->getOeufPorte());
    CPPUNIT_ASSERT(false == oeuf->getPorte());
}


/*!
         *\brief Test la methode attaquer pour un cas nominal
         *
         * Resultat attendu: nv PV de agent = 20-10=10 
         */
void TestFourmi::testattaquer(void) {
    Agent *a = new Agent (20, Position(10, 9, 'N'), 15);
    F_90_10x_8y_S_10_oeuf->attaquer(a);
    CPPUNIT_ASSERT(20-F_90_10x_8y_S_10_oeuf->getPA() == a->getPV());
}


/*!
         *\brief Test de la methode bouger
         *
         * Resultat attendu : case ou l'agent se trouvait ne contienne plus d'Agent
         *                    case(7, 7) contienne l'Agent
         *                    la postion de l'agent soit la nouvelle
         */
void TestFourmi::testBouger(void) {
    Environnement *e = new Environnement (20, 20);
    Position p(7, 7, 'N');
    F_90_10x_8y_S_10_oeuf->bouger(p, e);
    CPPUNIT_ASSERT(p.getX() == F_90_10x_8y_S_10_oeuf->getPosition().getX());
    CPPUNIT_ASSERT(p.getY() == F_90_10x_8y_S_10_oeuf->getPosition().getY());
    CPPUNIT_ASSERT(p.getOrientation() == F_90_10x_8y_S_10_oeuf->getPosition().getOrientation());
    CPPUNIT_ASSERT(e->getAgentAt(7, 8) == NULL);
    CPPUNIT_ASSERT(e->getAgentAt(7, 7) == F_90_10x_8y_S_10_oeuf);
}

/*!
         *\brief Test de la methode decider lorsqu'on lui passe une liste de cases contenant un oeuf de puceron et une fourmilière
         *
         * Resultat attendu : toute décision sauf "erreur"
         */
void TestFourmi::testDecider(void) {
     list<Case *> *listeCases = new list<Case *>;
     Position posO(7, 7);
     OeufDePuceron *oeuf = new OeufDePuceron (false, false, 2, posO);
     Fourmiliere *fourmiliere = new Fourmiliere;
     listeCases->push_back(oeuf);
     listeCases->push_back(fourmiliere);
     CPPUNIT_ASSERT(F_90_10x_8y_S_10_oeuf->decider(listeCases) != "ERREUR");
}



/*!
         *\brief Test de la methode voirEnvironnement
         *
         * Resultat attendu : grille vide = l'agent ne voit rien
         *
         */
void TestFourmi::testVoirEnvironnementVide(void) {
    // Environnement vide
    Environnement *e1 = new Environnement (20, 20);
    e1->setAgentAt(7, 8, F_100_7x_8y_N_10_null);
    std::list<Case*> ve1;
    // test de environnement vide
    ve1 = F_100_7x_8y_N_10_null->voirEnvironnement(e1);
    CPPUNIT_ASSERT(ve1.size() == 0 );

}

/*!
         *\brief Test de la methode voirEnvironnement
         *
         * Resultat attendu : grille pleine de fourmi = l'agent voit les cases (6..8, 7) (6..8, 6) (5..9, 5) (4..10, 4) (3..11, 3) (6,8) (8,8)
         */
void TestFourmi::testVoirEnvironnementPlein(void) {
    // Environnement plein de fourmi
    Environnement *e3 = new Environnement (20, 20);
    for (int i = 0 ; i<20 ; i++) {
        for (int j = 0 ; j<20 ; j++) {
            Fourmi *f = new Fourmi(100, Position(j, i, 'N'), 10, NULL);
            e3->setAgentAt(j, i, f);
        }
    }
    e3->setAgentAt(7, 8, F_100_7x_8y_N_10_null);
    std::list<Case*> ve1;
    // test environnement plein de fourmi
    ve1 = F_100_7x_8y_N_10_null->voirEnvironnement(e3);
    bool valider;
    int x;
    int y;
    valider = true;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if ((x<6 || x>8 || y != 7) && (x<6 || x>8 || y != 6) && (x<5 || x>9 || y != 5) && (x<4 || x>10 || y != 4) && (x<3 || x>11 || y != 3) && (x!=6 || y!=8) && (x!=8 || y!=8))  {
            valider = false;
        }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 29);
}

/*!
         *\brief Test de la methode voirEnvironnement
         *
         * Resultat attendu : l'agent voit le puceron dans (6, 6)
         */
void TestFourmi::testVoirEnvironnement_Puceron(void) {
    Environnement *e2 = new Environnement (20, 20);
    Puceron *pus = new Puceron(100, Position(6, 6, 'N'), 10);
    e2->setAgentAt(6, 6, pus);
    e2->setAgentAt(7, 8, F_100_7x_8y_N_10_null);
    std::list<Case*> ve1;
    ve1 = F_100_7x_8y_N_10_null->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 6 || y != 6) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
         *\brief Test de la methode voirEnvironnement
         *
         * Resultat attendu : l'agent voit l'abeille dans (6, 6)
         */
void TestFourmi::testVoirEnvironnement_Abeille(void) {
    Environnement *e2 = new Environnement (20, 20);
    Abeille  *ab = new Abeille(100, Position(6, 6, 'N'), 10, 10);
    e2->setAgentAt(6, 6, ab);
    e2->setAgentAt(7, 8, F_100_7x_8y_N_10_null);
    std::list<Case*> ve1;
    ve1 = F_100_7x_8y_N_10_null->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 6 || y != 6) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
         *\brief Test de la methode voirEnvironnement
         *
         * Resultat attendu : l'agent voit la fourmi dans (7, 6)
         */
void TestFourmi::testVoirEnvironnement_Fourmi(void) {
    Environnement *e2 = new Environnement (20, 20);
    Fourmi *fou = new Fourmi(100, Position(7, 6, 'N'), 10, NULL);
    e2->setAgentAt(7, 6, fou);
    e2->setAgentAt(7, 8, F_100_7x_8y_N_10_null);
    std::list<Case*> ve1;
    ve1 = F_100_7x_8y_N_10_null->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 7 || y != 6) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
         *\brief Test de la methode voirEnvironnement
         *
         * Resultat attendu : l'agent voit la ruche dans (8, 6)
         */
void TestFourmi::testVoirEnvironnement_Ruche(void) {
    Environnement *e2 = new Environnement (20, 20);
	Ruche *ru = new Ruche(7, 6);
	e2->setCaseAt(7, 6, ru);
	e2->setAgentAt(7, 8, F_100_7x_8y_N_10_null);
	std::list<Case*> ve1;
	ve1 = F_100_7x_8y_N_10_null->voirEnvironnement(e2);
	bool valider;
	valider = true;
	int x;
	int y;
	for (auto it = ve1.begin(); it != ve1.end(); it++) {
		x = (*it)->getPosition().getX();
		y = (*it)->getPosition().getY();
		if (x != 7 || y != 6) {
					valider = false;
				}
	}
	CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
         *\brief Test de la methode voirEnvironnement
         *
         * Resultat attendu : l'agent voit la Fourmiliere dans (6, 7)
         */
void TestFourmi::testVoirEnvironnement_Fourmiliere(void) {
	Environnement *e2 = new Environnement (20, 20);
	Fourmiliere *f= new Fourmiliere(7, 6);
	e2->setCaseAt(7, 6, f);
	e2->setAgentAt(7, 8, F_100_7x_8y_N_10_null);
	std::list<Case*> ve1;
	ve1 = F_100_7x_8y_N_10_null->voirEnvironnement(e2);
	bool valider;
	valider = true;
	int x;
	int y;
	for (auto it = ve1.begin(); it != ve1.end(); it++) {
		x = (*it)->getPosition().getX();
		y = (*it)->getPosition().getY();
		if (x != 7 || y != 6) {
					valider = false;
				}
	}
	CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
         *\brief Test de la methode voirEnvironnement
         *
         * Resultat attendu : l'agent voit la OeufDePuceron dans (7, 7)
         */
void TestFourmi::testVoirEnvironnement_OeufDePuceron(void) {
    Environnement *e2 = new Environnement (20, 20);
    OeufDePuceron *o = new OeufDePuceron(false, false, 10, Position(7, 7, 'N'));
    e2->setCaseAt(7, 7, o);
    e2->setAgentAt(7, 8, F_100_7x_8y_N_10_null);
    std::list<Case*> ve1;
    ve1 = F_100_7x_8y_N_10_null->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 7 || y != 7) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
         *\brief Test de la methode voirEnvironnement
         *
         * Resultat attendu : l'agent voit la Fleur dans (8, 7)
         */
void TestFourmi::testVoirEnvironnement_Fleur(void) {
    Environnement *e2 = new Environnement (20, 20);
    Fleur *fle = new Fleur(100, Position(8, 7, 'N'), 10);
    e2->setCaseAt(8, 7, fle);
    e2->setAgentAt(7, 8, F_100_7x_8y_N_10_null);
    std::list<Case*> ve1;
    ve1 = F_100_7x_8y_N_10_null->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 8 || y != 7) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
         *\brief Test de la methode voirEnvironnement
         *
         * Resultat attendu : l'agent voit l'Alveole dans (7, 7)
         */
void TestFourmi::testVoirEnvironnement_Alveole(void) {
    Environnement *e2 = new Environnement (20, 20);
    Position pos(8, 7, 'N');
    Alveole *a = new Alveole(10, 10, true, pos, 10);
    e2->setCaseAt(8, 7, a);
    e2->setAgentAt(7, 8, F_100_7x_8y_N_10_null);
    std::list<Case*> ve1;
    ve1 = F_100_7x_8y_N_10_null->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 8 || y != 7) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}





// -----------------------------------------------------------------------------
CPPUNIT_TEST_SUITE_REGISTRATION( TestFourmi );
int main(int argc, char* argv[]) {
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;
    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);
    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);
    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);
    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();
    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
