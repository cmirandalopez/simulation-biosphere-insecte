/*!
 * \file TestCase_projet.cpp
 * \brief Tests unitaires de la classe Case
 * \author Jorge OCHOA
 * \version 0.1
 */

#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <netinet/in.h>
#include "Case.hpp"
#include "Position.hpp"
#include "Puceron.hpp"
//#include "CBasicMath.hpp"
using namespace CppUnit;
using namespace std;

//-----------------------------------------------------------------------------
/*! \class TestCase_projet
	 * \brief Classe implémentant les tests unitaires de la classe Case.
	 *
	 */
class TestCase_projet : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(TestCase_projet);
	CPPUNIT_TEST(testgetPosition);
	CPPUNIT_TEST(testsetPosition);
	CPPUNIT_TEST(testSet_GetAgent);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp(void);
	void tearDown(void);
protected:
	void testgetPosition(void);
	void testsetPosition(void);
	void testSet_GetAgent(void);
private:
	Case *Case_1_2_N; // Case en position 1, 2 et orientation N
};

//-----------------------------------------------------------------------------
void TestCase_projet::setUp(void) {
		Position p(1, 2, 'N');
    Case_1_2_N = new Case(p);
}

void TestCase_projet::tearDown(void) {
    delete Case_1_2_N;
}
/*!
		 *\brief Test la méthode getPosition
		 *
		 * Resultat attendu position en x = 1
		 * Resultat attendu position en y = 2
		 * Resultat attendu orientation : N
		 */
void TestCase_projet::testgetPosition(void)
{
	CPPUNIT_ASSERT(1 == Case_1_2_N->getPosition().getX());
	CPPUNIT_ASSERT(2 == Case_1_2_N->getPosition().getY());
	CPPUNIT_ASSERT('N' == Case_1_2_N->getPosition().getOrientation());
}

/*!
		 *\brief Test la méthode setPosition
		 *
		 * On veut changer la position de la case à x = 4, y = 5, orientation = S
		 *
		 * Resultat attendu position en x = 4
		 * Resultat attendu position en y = 5
		 * Resultat attendu orientation : S
		 */
void TestCase_projet::testsetPosition(void)
{
		Position pos(4, 5, 'S');
    Case_1_2_N->setPosition(pos);
		CPPUNIT_ASSERT(4 == Case_1_2_N->getPosition().getX());
		CPPUNIT_ASSERT(5 == Case_1_2_N->getPosition().getY());
		CPPUNIT_ASSERT('S' == Case_1_2_N->getPosition().getOrientation());

}

/*!
		 *\brief Test les méthodes setAgent et getAgent
		 *
		 * On veut mettre dans la case x = 1, y = 2 un Puceron
		 *
		 * Resultat attendu que la case contienne le puceron
		 */
void TestCase_projet::testSet_GetAgent(void){
	Puceron *pus = new Puceron(100, Position(7, 8, 'N'), 10);
	CPPUNIT_ASSERT(NULL == Case_1_2_N->getAgent());
	Case_1_2_N->setAgent(pus);
	CPPUNIT_ASSERT(pus == Case_1_2_N->getAgent());
}
//-----------------------------------------------------------------------------
CPPUNIT_TEST_SUITE_REGISTRATION( TestCase_projet );
int main(int argc, char* argv[])
{
	// informs test-listener about testresults
	CPPUNIT_NS::TestResult testresult;
	// register listener for collecting the test-results
	CPPUNIT_NS::TestResultCollector collectedresults;
	testresult.addListener (&collectedresults);
	// register listener for per-test progress output
	CPPUNIT_NS::BriefTestProgressListener progress;
	testresult.addListener (&progress);
	// insert test-suite at test-runner by registry
	CPPUNIT_NS::TestRunner testrunner;
	testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
	testrunner.run(testresult);
	// output results in compiler-format
	CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
	compileroutputter.write ();
	// return 0 if tests were successful
	return collectedresults.wasSuccessful() ? 0 : 1;
}
