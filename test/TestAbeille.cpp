/*!
 * \file TestAbeille.cpp
 * \brief Tests unitaires de la classe Abeille
 * \author FRAGNAUD Cedric
 * \author MARCUSANU Ana
 * \author PROTAIS Francois
 * \version 0.1
 */

#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <netinet/in.h>
#include "Puceron.hpp"
#include "Abeille.hpp"
#include "Fourmi.hpp"
#include "Position.hpp"
#include "Fleur.hpp"
#include "Ruche.hpp"
#include "OeufDePuceron.hpp"
#include "Fourmiliere.hpp"
using namespace CppUnit;
using namespace std;
#define private public

//-----------------------------------------------------------------------------

/*! \class TestAbeille
     * \brief Classe implémentant les tests unitaires de la classe Abeille.
     *
     */


class TestAbeille : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestAbeille);
    CPPUNIT_TEST(testgetPollen);
    CPPUNIT_TEST(testbutiner1);
    CPPUNIT_TEST(testbutiner2);
    CPPUNIT_TEST(testcreerAlveole1);
    CPPUNIT_TEST(testcreerAlveole2);
    CPPUNIT_TEST(testconstruireAlveole1);
    CPPUNIT_TEST(testconstruireAlveole2);
    CPPUNIT_TEST(testconstruireAlveole3);
    CPPUNIT_TEST(testBouger);
    CPPUNIT_TEST(testDecider1);
    CPPUNIT_TEST(testDecider2);
    CPPUNIT_TEST(testattaquer);
    CPPUNIT_TEST(testVoirEnvironnementVide);
    CPPUNIT_TEST(testVoirEnvironnementPlein);
    CPPUNIT_TEST(testVoirEnvironnement_Puceron);
    CPPUNIT_TEST(testVoirEnvironnement_Abeille);
    CPPUNIT_TEST(testVoirEnvironnement_Fourmi);
    CPPUNIT_TEST(testVoirEnvironnement_Ruche);
    CPPUNIT_TEST(testVoirEnvironnement_Fourmiliere);
    CPPUNIT_TEST(testVoirEnvironnement_OeufDePuceron);
    CPPUNIT_TEST(testVoirEnvironnement_Fleur);
    CPPUNIT_TEST(testVoirEnvironnement_Alveole);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);
protected:
    void testgetPollen(void);
    void testbutiner1(void);
    void testbutiner2(void);
    void testcreerAlveole1(void);
    void testcreerAlveole2(void);
    void testconstruireAlveole1(void);
    void testconstruireAlveole2(void);
    void testconstruireAlveole3(void);
    void testBouger(void);
    void testDecider1(void);
    void testDecider2(void);
    void testattaquer(void);
    void testVoirEnvironnementVide(void);
    void testVoirEnvironnementPlein(void);
    void testVoirEnvironnement_Puceron(void);
    void testVoirEnvironnement_Abeille(void);
    void testVoirEnvironnement_Fourmi(void);
    void testVoirEnvironnement_Ruche(void);
    void testVoirEnvironnement_Fourmiliere(void);
    void testVoirEnvironnement_OeufDePuceron(void);
    void testVoirEnvironnement_Fleur(void);
    void testVoirEnvironnement_Alveole(void);
private:
    Abeille *A_100_7x_8y_N_10_0;    /*! Abeille avec 100 PdV, a la case (7, 8) face au nord, avec 10 pt d'attaque, sans pollen */
    Abeille *A_90_10x_8y_S_10_5;  /*! Abeille avec 90 PdV, a la case (10, 8) face au sud, avec 10 pt d'attaque, avec 5 unites de pollen */
};

//-----------------------------------------------------------------------------
void TestAbeille::setUp() {
    A_100_7x_8y_N_10_0 = new Abeille(100, Position(7, 8, 'N'), 10, 0);
    A_90_10x_8y_S_10_5 = new Abeille(90, Position(10, 8, 'S'), 10, 5);
}

void TestAbeille::tearDown() {
    delete A_100_7x_8y_N_10_0;
    delete A_90_10x_8y_S_10_5;
}

/*!
 *\brief Test la methode getPollen
 *
 * Resultat attendu pollen pour la première abeille: 0
 * Resultat attendu pollen pour la deuxième abeille: 5
 */
void TestAbeille::testgetPollen() {
    CPPUNIT_ASSERT(0 == A_100_7x_8y_N_10_0->getPollen());
    CPPUNIT_ASSERT(5 == A_90_10x_8y_S_10_5->getPollen());
}


/*!
 *\brief Test de la methode butiner pour un cas nominal
 *
 * Resultat attendu pollen pour la abeille: 1
 * Resultat attendu pollen pour la fleur: 9
 */
void TestAbeille::testbutiner1(void)
{
    Position pos(7, 7, 'N');
    Fleur *fleur = new Fleur(10, pos, 2);
    A_100_7x_8y_N_10_0->butiner(fleur);
    CPPUNIT_ASSERT(1 == A_100_7x_8y_N_10_0->getPollen());
    CPPUNIT_ASSERT(9 == fleur->getnbPollen());
}


/*!
 *\brief Test de la methode butiner pour le cas ou la fleur n'a pas de pollen
 *
 * Resultat attendu pollen pour la abeille: 5
 * Resultat attendu pollen pour la fleur: 0
 */
void TestAbeille::testbutiner2(void)
{
    Position pos (10, 9, 'N');
    Fleur *fleur = new Fleur(0, pos, 2);
    A_90_10x_8y_S_10_5->butiner(fleur);
    CPPUNIT_ASSERT(5 == A_90_10x_8y_S_10_5->getPollen());
    CPPUNIT_ASSERT(0 == fleur->getnbPollen());
}



/*!
 *\brief Test de la methode creerAlveole pour un cas nominal
 *
 * Resultat attendu pollen pour la abeille: 4
 */
void TestAbeille::testcreerAlveole1(void)
{
    Position pos (10, 9);
    Environnement *e = new Environnement (20, 20);
    A_90_10x_8y_S_10_5->creerAlveole(pos, e);
    CPPUNIT_ASSERT(4 == A_90_10x_8y_S_10_5->getPollen());
}


/*!
 *\brief Test de la methode creerAlveole pour le cas ou l'abeille n'a pas de pollen
 *
 * Resultat attendu pollen pour la abeille: 0
 */
void TestAbeille::testcreerAlveole2(void)
{
    Position pos (7, 7);
    Environnement *e = new Environnement (20, 20);
    A_100_7x_8y_N_10_0->creerAlveole(pos, e);
    CPPUNIT_ASSERT(0 == A_100_7x_8y_N_10_0->getPollen());
}



/*!
 *\brief Test de la methode construireAlveole pour un cas nominal
 *
 * Resultat attendu pollen pour la abeille: 4
 * Resultat attendu tempsConstruction pour la l'alveole: 5
 */
void TestAbeille::testconstruireAlveole1(void)
{
    Position pos (10, 9, 'N');
    Alveole *alveole = new Alveole (3, 6, false, pos, 10);
    A_90_10x_8y_S_10_5->construireAlveole(alveole);
    CPPUNIT_ASSERT(4 == A_90_10x_8y_S_10_5->getPollen());
    CPPUNIT_ASSERT(5 == alveole->getTempsCons());
}


/*!
 *\brief Test de la methode construireAlveole pour le cas ou l'abeille n'a pas de pollen
 *
 * Resultat attendu pollen pour la abeille: 0
 * Resultat attendu tempsConstruction pour la l'alveole: 6
 */
void TestAbeille::testconstruireAlveole2(void)
{
    Position pos (7, 7, 'N');
    Alveole *alveole = new Alveole (3, 6, false, pos, 10);
    A_100_7x_8y_N_10_0->construireAlveole(alveole);
    CPPUNIT_ASSERT(0 == A_100_7x_8y_N_10_0->getPollen());
    CPPUNIT_ASSERT(6 == alveole->getTempsCons());
}

/*!
 *\brief Test de la methode construireAlveole pour le cas ou l'alveole est deja construite (elle est en evolution)
 *
 * Resultat attendu pollen pour l'abeille: 5
 * Resultat attendu tempsConstruction pour l'alveole: 0
 */
void TestAbeille::testconstruireAlveole3(void)
{
    Position pos(10, 9, 'N');
    Alveole *alveole = new Alveole (3, 0, true, pos, 9);
    A_90_10x_8y_S_10_5->construireAlveole(alveole);
    CPPUNIT_ASSERT(5 == A_90_10x_8y_S_10_5->getPollen());
    CPPUNIT_ASSERT(0 == alveole->getTempsCons());
}

/*!
 *\brief Test de la methode bouger
 *
 * Resultat attendu : case ou l'agent se trouvait ne contienne plus d'Agent
 *                    case(7, 7) contienne l'Agent
 *                    la postion de l'agent soit la nouvelle
 */
void TestAbeille::testBouger(void){
    Environnement *e = new Environnement (20, 20);
    Position p(7, 7, 'N');
    A_100_7x_8y_N_10_0->bouger(p, e);
    CPPUNIT_ASSERT(p.getX() == A_100_7x_8y_N_10_0->getPosition().getX());
    CPPUNIT_ASSERT(p.getY() == A_100_7x_8y_N_10_0->getPosition().getY());
    CPPUNIT_ASSERT(p.getOrientation() == A_100_7x_8y_N_10_0->getPosition().getOrientation());
    CPPUNIT_ASSERT(e->getAgentAt(7, 8) == NULL);
    CPPUNIT_ASSERT(e->getAgentAt(7, 7) == A_100_7x_8y_N_10_0);
}

/*!
 *\brief Test de la methode decider lorsqu'on lui passe une liste de cases vide
 *
 * Resultat attendu : toute décision sauf "erreur"
 */
void TestAbeille::testDecider1(void){
     list<Case *> *listeCases = new list<Case *>;
     CPPUNIT_ASSERT(A_100_7x_8y_N_10_0->decider(listeCases) != "ERREUR");
}

/*!
 *\brief Test de la methode decider lorsqu'on lui passe une liste de cases contenant une alveole et une fleur
 *
 * Resultat attendu : toute décision sauf "erreur"
 */
void TestAbeille::testDecider2(void){
     list<Case *> *listeCases = new list<Case *>;
   Position posf(7, 7);
     Position posa(9, 7);
     Fleur *fleur = new Fleur (0, posf, 2);
     Alveole *alveole = new Alveole (3, 6, false, posa, 10);
     listeCases->push_back(fleur);
     listeCases->push_back(alveole);
     CPPUNIT_ASSERT(A_100_7x_8y_N_10_0->decider(listeCases) != "ERREUR");
}

/*!
 *\brief Test de la methode attaquer pour un cas nominal
 *
 * Résultat attendu : nombre de points de vie de l'agent attaqué = son nombre de points de vie initial - le nombre de points d'attaque
 */
void TestAbeille::testattaquer(void)
{
  Agent *a = new Agent (20, Position(10, 9, 'N'), 15);
    A_100_7x_8y_N_10_0->attaquer(a);
    CPPUNIT_ASSERT(20-A_100_7x_8y_N_10_0->getPA() == a->getPV());
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : grille vide = l'agent ne voit rien
 *
 */
void TestAbeille::testVoirEnvironnementVide(void){
    // Environnement vide
    Environnement *e1 = new Environnement (20, 20);
    e1->setAgentAt(7, 8, A_100_7x_8y_N_10_0);
    std::list<Case*> ve1;
    //test de environnement vide
    ve1 = A_100_7x_8y_N_10_0->voirEnvironnement(e1);
    CPPUNIT_ASSERT(ve1.size() == 0 );

}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : grille pleine de pucerons = l'agent voit les cases (6, 6) (7, 6) (8, 6) (6, 7) (7, 7) (8, 7)
 */
void TestAbeille::testVoirEnvironnementPlein(void){
    //Environnement plein de pucerons
    Environnement *e3 = new Environnement (20, 20);
    for ( int i = 0 ; i<20 ; i++) {
        for (int j = 0 ; j<20 ; j++){
            Puceron *pus = new Puceron(100, Position(j, i, 'N'), 10);
            e3->setAgentAt(j, i, pus);
        }
    }
    e3->setAgentAt(7, 8, A_100_7x_8y_N_10_0);
    std::list<Case*> ve1;
    //test environnement plein de puceron
    ve1 = A_100_7x_8y_N_10_0->voirEnvironnement(e3);
    bool valider;
    int x;
    int y;
    valider = true;
    for (auto it = ve1.begin(); it != ve1.end(); it++) {
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if ((x != 8 || y<7 || y>9) && (x != 9 || y<6 || y>10) && (x != 10 || y<5 || y>11) && (x != 11 || y<4 || y>12)
        &&  (x != 6 || y<7 || y>9) && (x != 5 || y<6 || y>10) && (x != 4 || y<5 || y>11) && (x != 3 || y<4 || y>12) && (x != 7 || y != 7)) {
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 49 );
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit le puceron dans (6, 6)
 */
void TestAbeille::testVoirEnvironnement_Puceron(void){
    Environnement *e2 = new Environnement (20, 20);
    Puceron *pus = new Puceron(100, Position(6, 7, 'N'), 10);
    e2->setAgentAt(6, 7, pus);
    e2->setAgentAt(7, 8, A_100_7x_8y_N_10_0);
    std::list<Case*> ve1;
    ve1 = A_100_7x_8y_N_10_0->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++){
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 6 || y != 7){
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit l'abeille dans (6, 6)
 */
void TestAbeille::testVoirEnvironnement_Abeille(void){
    Environnement *e2 = new Environnement (20, 20);
    Abeille  *ab = new Abeille(100, Position(6, 7, 'N'), 10, 10);
    e2->setAgentAt(6, 7, ab);
    e2->setAgentAt(7, 8, A_100_7x_8y_N_10_0);
    std::list<Case*> ve1;
    ve1 = A_100_7x_8y_N_10_0->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++){
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 6 || y != 7){
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit la fourmi dans (7, 6)
 */
void TestAbeille::testVoirEnvironnement_Fourmi(void){
    Environnement *e2 = new Environnement (20, 20);
    Fourmi *fou = new Fourmi(100, Position(11, 6, 'N'), 10, NULL);
    e2->setAgentAt(11, 6, fou);
    e2->setAgentAt(7, 8, A_100_7x_8y_N_10_0);
    std::list<Case*> ve1;
    ve1 = A_100_7x_8y_N_10_0->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++){
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 11 || y != 6){
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit la ruche dans (7, 6)
 */
void TestAbeille::testVoirEnvironnement_Ruche(void){
    Environnement *e2 = new Environnement (20, 20);
    Ruche *ru = new Ruche(11, 6);
    e2->setCaseAt(11, 6, ru);
    e2->setAgentAt(7, 8, A_100_7x_8y_N_10_0);
    std::list<Case*> ve1;
    ve1 = A_100_7x_8y_N_10_0->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++){
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 11 || y != 6){
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit la Fourmiliere dans (7, 6)
 */
void TestAbeille::testVoirEnvironnement_Fourmiliere(void){
    Environnement *e2 = new Environnement (20, 20);
    Fourmiliere *f= new Fourmiliere(11, 6);
    e2->setCaseAt(11, 6, f);
    e2->setAgentAt(7, 8, A_100_7x_8y_N_10_0);
    std::list<Case*> ve1;
    ve1 = A_100_7x_8y_N_10_0->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++){
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 11 || y != 6){
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit la OeufDePuceron dans (7, 7)
 */
void TestAbeille::testVoirEnvironnement_OeufDePuceron(void){
    Environnement *e2 = new Environnement (20, 20);
    OeufDePuceron *o = new OeufDePuceron(false, false, 10, Position(4, 9));
    e2->setCaseAt(4, 9, o);
    e2->setAgentAt(7, 8, A_100_7x_8y_N_10_0);
    std::list<Case*> ve1;
    ve1 = A_100_7x_8y_N_10_0->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++){
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 4 || y != 9){
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit la Fleur dans (8, 7)
 */
void TestAbeille::testVoirEnvironnement_Fleur(void){
    Environnement *e2 = new Environnement (20, 20);
    Fleur *fle = new Fleur(100, Position(3, 7), 10);
    e2->setCaseAt(3, 7, fle);
    e2->setAgentAt(7, 8, A_100_7x_8y_N_10_0);
    std::list<Case*> ve1;
    ve1 = A_100_7x_8y_N_10_0->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++){
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 3 || y != 7){
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

/*!
 *\brief Test de la methode voirEnvironnement
 *
 * Resultat attendu : l'agent voit l'Alveole dans (7, 7)
 */
void TestAbeille::testVoirEnvironnement_Alveole(void){
    Environnement *e2 = new Environnement (20, 20);
    Position pos(8, 9, 'N');
    Alveole *a = new Alveole(8, 10, true, pos, 10);
    e2->setCaseAt(8, 9, a);
    e2->setAgentAt(7, 8, A_100_7x_8y_N_10_0);
    std::list<Case*> ve1;
    ve1 = A_100_7x_8y_N_10_0->voirEnvironnement(e2);
    bool valider;
    valider = true;
    int x;
    int y;
    for (auto it = ve1.begin(); it != ve1.end(); it++){
        x = (*it)->getPosition().getX();
        y = (*it)->getPosition().getY();
        if (x != 8 || y != 9){
                    valider = false;
                }
    }
    CPPUNIT_ASSERT(valider && ve1.size() == 1);
}

//-----------------------------------------------------------------------------
CPPUNIT_TEST_SUITE_REGISTRATION( TestAbeille );
int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;
    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);
    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);
    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);
    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();
    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
