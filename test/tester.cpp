#include <cppunit/ui/text/TestRunner.h>
#include "TestFleur.cpp"

int main(int argc, char **argv) {
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(TestFleur::suite());
  runner.run();
  return 0;
}
