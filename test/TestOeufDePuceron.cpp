#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <netinet/in.h>
#include "OeufDePuceron.hpp"
using namespace CppUnit;
using namespace std;

/*!
 * \file TestOeufDePuceron.cpp
 * \brief Declaration de la classe TestOeufDePuceron
 * \author Clara Moriceau and Elsa Laval
 * \version 0.1
 */

/*! \class TestOeufDePuceron
   * \brief Classe representant les tests de la classe OeufDePuceron.
   *
   */

//-----------------------------------------------------------------------------

class TestOeufDePuceron : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(TestOeufDePuceron);
	CPPUNIT_TEST(testgetPorte);
	CPPUNIT_TEST(testsetPorte);
	CPPUNIT_TEST(testgetAdjacent);
	CPPUNIT_TEST(testsetAdjacent);
	CPPUNIT_TEST(testConstructeur);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp(void);
	void tearDown(void);
protected:
	void testgetPorte(void);
	void testsetPorte(void);
	void testgetAdjacent(void);
	void testsetAdjacent(void);
	void testConstructeur(void);
private:
	OeufDePuceron *Oeuf_f_F, *Oeuf_Nf_NF;
};

//----------------------------------------------------------------------------- 
/*!
 *  \brief Creation des objets necessaires aux tests
 *
 */

void TestOeufDePuceron::setUp() {
    Position pos(5, 5);
    Oeuf_f_F = new OeufDePuceron(true, true, 3, pos); 		/*!<porte et adjacent a une fourmiliere.*/
    Oeuf_Nf_NF = new OeufDePuceron(false, false, 3, pos);      /*!<non porte et non adjacent a une fourmiliere.*/
}

/*!
 *  \brief Destruction des objets necessaires aux tests
 *
 */

void TestOeufDePuceron::tearDown() {
    delete Oeuf_f_F;
    delete Oeuf_Nf_NF;
}

/*!
 *  \brief Test la methode getPorte de OeufDePuceron
 *
 */

void TestOeufDePuceron::testgetPorte(void)
{
	CPPUNIT_ASSERT(true == Oeuf_f_F->getPorte());
	CPPUNIT_ASSERT(false == Oeuf_Nf_NF->getPorte());  
}

/*!
 *  \brief Test la methode getAdjacent de OeufDePuceron
 *
 */

void TestOeufDePuceron::testgetAdjacent(void)
{
    	CPPUNIT_ASSERT(true == Oeuf_f_F->getAdjacent());
	CPPUNIT_ASSERT(false == Oeuf_Nf_NF->getAdjacent()); 
}

/*!
 *  \brief Test la methode setPorte de OeufDePuceron
 *
 */

void TestOeufDePuceron::testsetPorte(void)
{
    	Oeuf_f_F->setPorte(false);
	CPPUNIT_ASSERT(false== Oeuf_f_F->getPorte());
  
}

/*!
 *  \brief Test la methode setAdjacent de OeufDePuceron
 *
 */

void TestOeufDePuceron::testsetAdjacent(void)
{
    Oeuf_f_F->setAdjacent(false);
    CPPUNIT_ASSERT(false == Oeuf_f_F->getAdjacent()); 
}

/*!
 *  \brief Test le constructeur de OeufDePuceron
 *
 */

void TestOeufDePuceron::testConstructeur(void)
{
    	Position labonneposition(5, 5);
  	CPPUNIT_ASSERT(true == Oeuf_f_F->getPorte()); 
	CPPUNIT_ASSERT(true == Oeuf_f_F->getAdjacent()); 
	CPPUNIT_ASSERT(3 == Oeuf_f_F->getTempsDEvol());
	CPPUNIT_ASSERT(labonneposition.getX() == Oeuf_f_F->getPosition().getX()); //and labonneposition->getY()== Oeuf_f_F->getPosition().getY());
}

//-----------------------------------------------------------------------------
/*!
*  \brief Programme principal des tests
*
*/

CPPUNIT_TEST_SUITE_REGISTRATION( TestOeufDePuceron );
int main(int argc, char* argv[])
{
	// informs test-listener about testresults
	CPPUNIT_NS::TestResult testresult;
	// register listener for collecting the test-results
	CPPUNIT_NS::TestResultCollector collectedresults;
	testresult.addListener (&collectedresults);
	// register listener for per-test progress output
	CPPUNIT_NS::BriefTestProgressListener progress;
	testresult.addListener (&progress);
	// insert test-suite at test-runner by registry
	CPPUNIT_NS::TestRunner testrunner;
	testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
	testrunner.run(testresult);
	// output results in compiler-format
	CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
	compileroutputter.write ();
	// return 0 if tests were successful
	return collectedresults.wasSuccessful() ? 0 : 1;
}

