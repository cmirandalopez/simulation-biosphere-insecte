#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <netinet/in.h>
#include "Dynamique.hpp"
#include "OeufDePuceron.hpp"
#include "Alveole.hpp"
#include "Fleur.hpp"
#include "Agent.hpp"

using namespace CppUnit;
using namespace std;

/*!
 * \file TestDynamique.cpp
 * \brief Declaration de la classe TestDynamique
 * \author Clara Moriceau and Elsa Laval
 * \version 0.1
 */

/*! \class TestDynamique
   * \brief Classe representant les tests de la classe Dynamique.
   *
   */
//-----------------------------------------------------------------------------

class TestDynamique : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(TestDynamique);
	CPPUNIT_TEST(testgetTempsDEvol);
	CPPUNIT_TEST(testsetTempsDEvol);
	CPPUNIT_TEST(testmiseAJour);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp(void);
	void tearDown(void);
protected:
	void testgetTempsDEvol(void);
	void testmiseAJour(void);
	void testsetTempsDEvol(void);
private:
	Alveole *A_2_2_F;        /*!< Alveole avec deux voisins, un temps de construction de 2 et qui n'est pas en evolution.*/
	Alveole *A_2_0_T;        /*!<Alveole avec deux voisins, un temps de construction de 0 et qui est en evolution.*/
	Fleur *F_3; 			/*!< Fleur avec 3 pollens.*/
	OeufDePuceron *O_F_F_3;  /*!<Oeuf de puceron qui n'est pas porte, non adjacent a une fourmiliere.*/
	OeufDePuceron *O_T_T_3;	/*!<Oeuf de puceron est porte, adjacent à une fourmiliere.*/
	
};

//----------------------------------------------------------------------------- 
/*!
 *  \brief Creation des objets necessaires aux tests
 *
 */
void TestDynamique::setUp() {
	
    Position pos1(1, 1);	
    A_2_2_F = new Alveole(2, 2, false, pos1, 0); 
    Position pos2(2, 2);	
    A_2_0_T = new Alveole(2, 0, true, pos2, 3); 
    Position pos3(4, 4);	
        F_3 = new Fleur(3, pos3, 3);
    Position pos5(5, 1);	
    O_F_F_3 = new OeufDePuceron(false, false, 3, pos5);
    Position pos6(1, 5);	
    O_T_T_3 = new OeufDePuceron(true, true, 3, pos6);
   /*!<les temps d'evolution de toutes les entites dynamiques sont tous a 3.*/
}

/*!
 *  \brief Destruction des objets necessaires aux tests
 *
 */

void TestDynamique::tearDown() {
    delete A_2_2_F;
    delete A_2_0_T;
    delete F_3;
    delete O_F_F_3;
    delete O_T_T_3;
}


/*!
 *  \brief Test la methode getTempsDEvol de la classe Dynamique
 *
 */

void TestDynamique::testgetTempsDEvol(void)
{
	CPPUNIT_ASSERT(0 == A_2_2_F->getTempsDEvol()); 	/*!<on verifie que le temps d'evolution est bon pour l'alveole.*/
	CPPUNIT_ASSERT(3 == F_3->getTempsDEvol());		/*!<on verifie que le temps d'evolution est correct pour la fleur.*/
	CPPUNIT_ASSERT(3 == O_F_F_3->getTempsDEvol());	/*!<on verifie que le temps d'evolution est correct pour l'oeuf.*/
	
}

/*!
 *  \brief Test la methode setTempsDEvol de la classe Dynamique
 *
 */

void TestDynamique::testsetTempsDEvol(void)
{
	A_2_2_F->setTempsDEvol(2);
	CPPUNIT_ASSERT(2 == A_2_2_F->getTempsDEvol()); 	/*!<on verifie que le temps d'evolution est bon pour l'alveole.*/
	
}


/*!
 *  \brief Test la methode miseAJour de la classe Dynamique
 *
 */

void TestDynamique::testmiseAJour(void)
{
	Agent* res;
	Environnement* e=new Environnement(30, 30); /*!< Creation d'un environnement de 30*30.*/
  	res = A_2_2_F->miseAJour(e);
	CPPUNIT_ASSERT(0 == A_2_2_F->getTempsDEvol());	/*!<le temps n'est pas modifie puisque l'alveole n'est en pas evolution.*/
	CPPUNIT_ASSERT(res->getTypeAgent() == std::string("Abeille")); 			/*!<le temps d'évolution est à 0, miseAJour renvoie une abeille.*/
	
	res = A_2_0_T->miseAJour(e);
	CPPUNIT_ASSERT(2 == A_2_0_T->getTempsDEvol());	/*!<le temps est decremente de 1 car l'alveole est en evolution.*/
	CPPUNIT_ASSERT(NULL == res); 			/*!<L'alvéole est en évolution donc ne doit pas être supprimée.*/

	res = O_F_F_3->miseAJour(e);
	CPPUNIT_ASSERT(2 == O_F_F_3->getTempsDEvol());	/*!<l'oeuf n'est pas porte donc le temps d'évolution est décrémente de 1.*/
	CPPUNIT_ASSERT(NULL == res); 			/*!<l'oeuf est en évolution donc on ne doit pas supprimer l'oeuf.*/

	res = O_T_T_3->miseAJour(e);
	CPPUNIT_ASSERT(3 == O_T_T_3->getTempsDEvol());	/*!<l'oeuf est porte donc le temps n'est pas modifie.*/
	CPPUNIT_ASSERT(NULL == res); 			/*!<l'oeuf est en évolution donc on ne doit pas supprimer l'oeuf.*/
	
	O_T_T_3->setTempsDEvol(0);
	O_T_T_3->setPorte(false);
	res = O_T_T_3->miseAJour(e);
	CPPUNIT_ASSERT(res->getTypeAgent() == std::string("Fourmi")); 	/*!<l'oeuf n'est pas en évolution donc on doit le supprimer.*/

	res = F_3->miseAJour(e);
	CPPUNIT_ASSERT(2 == F_3->getTempsDEvol());	/*!<le temps est decremente de 1 à chaque tour pour la fleur.*/
	CPPUNIT_ASSERT(NULL == res);				/*!<la fourmi est en évolution donc on ne doit pas la supprimer.*/
}

/*!
*  \brief Programme principal des tests
*
*/
CPPUNIT_TEST_SUITE_REGISTRATION( TestDynamique );
int main(int argc, char* argv[])
{
	// informs test-listener about testresults
	CPPUNIT_NS::TestResult testresult;
	// register listener for collecting the test-results
	CPPUNIT_NS::TestResultCollector collectedresults;
	testresult.addListener (&collectedresults);
	// register listener for per-test progress output
	CPPUNIT_NS::BriefTestProgressListener progress;
	testresult.addListener (&progress);
	// insert test-suite at test-runner by registry
	CPPUNIT_NS::TestRunner testrunner;
	testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
	testrunner.run(testresult);
	// output results in compiler-format
	CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
	compileroutputter.write ();
	// return 0 if tests were successful
	return collectedresults.wasSuccessful() ? 0 : 1;
}

