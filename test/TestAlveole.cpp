#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <netinet/in.h>
#include "Alveole.hpp"
using namespace CppUnit;
using namespace std;

/*!
 * \file TestAlveole.cpp
 * \brief Declaration de la classe TestAlveole
 * \author Clara Moriceau and Elsa Laval
 * \version 0.1
 */

/*! \class TestAlveole
   * \brief Classe representant les tests de la classe Alveole.
   *
   */

//-----------------------------------------------------------------------------

class TestAlveole : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(TestAlveole);
	CPPUNIT_TEST(testConstructeur);
	CPPUNIT_TEST(testconstruire);
	CPPUNIT_TEST(testlancerEvolution);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp(void);
	void tearDown(void);
protected:
	void testconstruire(void);
	void testlancerEvolution(void);
        void testConstructeur(void);
private:
	Alveole *A_2_5_F_5_5_6; /*!<Alveole avec deux voisins, un temps de construction de 5 et qui n'est pas encore en évolution, qui se situe sur la position x=5 y=5 et qui à un temps d'évolution de 6 .*/
	Alveole *A_2_2_F; /*!<Alveole avec deux voisins, un temps de construction de 2 et qui n'est pas encore en évolution, qui se situe sur la position x=5 y=5 et qui à un temps d'évolution de 6.*/
};

//----------------------------------------------------------------------------- 
/*!
 *  \brief Creation des objets necessaires aux tests
 *
 */

void TestAlveole::setUp() {
    Position pos(5, 5);
    A_2_5_F_5_5_6 = new Alveole(2, 5, false, pos, 6);   
    A_2_2_F = new Alveole(2, 2, false, pos, 6);     
}

/*!
 *  \brief Destruction des objets necessaires aux tests
 *
 */

void TestAlveole::tearDown() {
    delete A_2_5_F_5_5_6;
    delete A_2_2_F;
}
/*!
 *  \brief Destruction des objets necessaires aux tests
 *
 */
void TestAlveole::testConstructeur() {
	Position* labonnepos= new Position(5, 5);
	CPPUNIT_ASSERT(2 ==  A_2_5_F_5_5_6->getNbVoisins());
	CPPUNIT_ASSERT(5 ==  A_2_5_F_5_5_6->getTempsCons());
	CPPUNIT_ASSERT(false ==  A_2_5_F_5_5_6->getEnEvolution());
	CPPUNIT_ASSERT((labonnepos->getX() ==  A_2_5_F_5_5_6->getPosition().getX()) and (labonnepos->getY() ==  A_2_5_F_5_5_6->getPosition().getY()));
	CPPUNIT_ASSERT(6 ==  A_2_5_F_5_5_6->getTempsDEvol());
}
/*!
 *  \brief Test la méthode miseAJour de la classe Dynamique
 *
 */

void TestAlveole::testconstruire(void)
{
   	A_2_5_F_5_5_6->construire(3);
	CPPUNIT_ASSERT(2 == A_2_5_F_5_5_6->getTempsCons());
	A_2_2_F->construire(3);
	CPPUNIT_ASSERT(0 == A_2_2_F->getTempsCons());    /*!<Temps construit supérieur au temps de construction restant.*/
}

/*!
 *  \brief Test la methode lancerEvolution de la classe Alveole
 *
 */

void TestAlveole::testlancerEvolution(void)
{
    A_2_5_F_5_5_6->lancerEvolution();
    CPPUNIT_ASSERT(true == A_2_5_F_5_5_6->getEnEvolution());
}

/*!
 *  \brief Programme principal des tests
 *
 */

//-----------------------------------------------------------------------------
CPPUNIT_TEST_SUITE_REGISTRATION( TestAlveole );
int main(int argc, char* argv[])
{
	// informs test-listener about testresults
	CPPUNIT_NS::TestResult testresult;
	// register listener for collecting the test-results
	CPPUNIT_NS::TestResultCollector collectedresults;
	testresult.addListener (&collectedresults);
	// register listener for per-test progress output
	CPPUNIT_NS::BriefTestProgressListener progress;
	testresult.addListener (&progress);
	// insert test-suite at test-runner by registry
	CPPUNIT_NS::TestRunner testrunner;
	testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
	testrunner.run(testresult);
	// output results in compiler-format
	CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
	compileroutputter.write ();
	// return 0 if tests were successful
	return collectedresults.wasSuccessful() ? 0 : 1;
}

