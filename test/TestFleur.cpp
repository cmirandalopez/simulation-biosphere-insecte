#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <netinet/in.h>
#include "Fleur.hpp"

using namespace CppUnit;
using namespace std;

/*!
 * \file TestFleur.cpp
 * \brief Declaration de la classe de test TestFleur
 * \author Clara Moriceau and Elsa Laval
 * \version 0.1
 */


//-----------------------------------------------------------------------------
/*! \class TestFleur
   * \brief Classe implementant les tests unitaires de la classe Fleur
   *
   * Elle herite de Dynamique. 
   */
class TestFleur : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(TestFleur);
	CPPUNIT_TEST(testConstructeur);
	CPPUNIT_TEST(testperdrePollen);
	CPPUNIT_TEST(testaPollen);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp(void);
	void tearDown(void);
protected:
	void testperdrePollen(void);
	void testaPollen(void);
	void testConstructeur(void);
private:
	Fleur *f_10_7_8_4;  /*!<Fleur avec un quantite de pollen de 10 qui se situe dans la position x=7 y=8 avec un temps d'évolution de 4 */
	Fleur *f_0;   /*!<Fleur sans pollen qui se situe dans la position x=7 y=8 avec un temps d'évolution de 4 */
};

//----------------------------------------------------------------------------- 
/*!
*  \brief Creation des objets necessaires aux tests
*/

void TestFleur::setUp() {
    Position pos(7, 8);
    f_10_7_8_4 = new Fleur(10, pos, 4);
    f_0 = new Fleur(0, pos, 4);        
}
/*!
*  \brief Destruction des objets necessaires aux tests
*/

void TestFleur::tearDown() {
    delete  f_10_7_8_4;
    delete f_0;
}
/**
 * \brief Test du constructeur 
 */

void TestFleur::testConstructeur() {
	
	Position* labonnepos = new Position(7, 8);	
	CPPUNIT_ASSERT(10 == f_10_7_8_4->getnbPollen()); /*!< on verifie que le pollen est bien initialise a zero */
	CPPUNIT_ASSERT(labonnepos->getX() == f_10_7_8_4->getPosition().getX() and labonnepos->getY() == f_10_7_8_4->getPosition().getY()); /*!< on verifie que la case attribué correspond à la bonne case */
	CPPUNIT_ASSERT(4 == f_10_7_8_4->getTempsDEvol()); /*!< on verifie que le temps d'evolution est bien initialise a zero */
}

/*!
*  \brief Test de la methode perdrePollen
*/
void TestFleur::testperdrePollen(void)
{
    f_10_7_8_4->perdrePollen();
    f_0->perdrePollen();
	CPPUNIT_ASSERT(9 == f_10_7_8_4->getnbPollen());
	CPPUNIT_ASSERT(0 == f_0->getnbPollen()); /*!< Si une fleur n'a plus deja plus de pollen, on ne decremente pas */
}

/*!
*  \brief Test de la methode testaPollen
*/
void TestFleur::testaPollen(void)
{
	CPPUNIT_ASSERT(true == f_10_7_8_4->aPollen());
	CPPUNIT_ASSERT(false == f_0->aPollen());
}

//-----------------------------------------------------------------------------
/*!
*  \Programme principal du test
*/

CPPUNIT_TEST_SUITE_REGISTRATION( TestFleur );
int main(int argc, char* argv[])
{
	// informs test-listener about testresults
	CPPUNIT_NS::TestResult testresult;
	// register listener for collecting the test-results
	CPPUNIT_NS::TestResultCollector collectedresults;
	testresult.addListener (&collectedresults);
	// register listener for per-test progress output
	CPPUNIT_NS::BriefTestProgressListener progress;
	testresult.addListener (&progress);
	// insert test-suite at test-runner by registry
	CPPUNIT_NS::TestRunner testrunner;
	testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
	testrunner.run(testresult);
	// output results in compiler-format
	CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
	compileroutputter.write ();
	// return 0 if tests were successful
	return collectedresults.wasSuccessful() ? 0 : 1;
}

