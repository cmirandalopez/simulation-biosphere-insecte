#ifndef INCLUDE_AGENT_HPP_
#define INCLUDE_AGENT_HPP_

/*!
 * \file Agent.hpp
 * \brief Declaration de la classe Agent
 * \author F.Protais
 * \version 0.1
 */

#include <string>
#include <list>
#include "Environnement.hpp"
#include "Position.hpp"
#include "Case.hpp"

/*!
 * \class Agent
 * \author F.Protais
 * \author Carlos MIRANDA <carlos.miranda_lopez@insa-rouen.fr>
 * \brief Class représentant les agents
 */
class Agent {
 protected:
  int points_vie; /*!<Représente la vie de l'agent.*/
  Position position; /*!< Position de l'agent.*/
  int points_attaque; /*!< Repr"sente les points d'attaque de l'agent.*/

  /*!
   * \brief Permet à l'agent d'attaquer un autre agent.
   * \param a : agent attconstaquer.
   *
   * L'agent attaquer un autre agent, avec le nombre de point d'attaque qu'il possede
   */
  virtual void attaquer(Agent *const a) const;

  /*!
   * \brief Fait bouger l'Agent
   * \param p : Position.
   * \param e : environnement dans lequel l'agent évolue.
   *
   * L'agent bouge sur la position donnée
   */
  virtual void bouger(Position p, Environnement *const e);

  /*!
   * \brief Fait bouger l'Agent
   * \param cases : liste de Case (correspond à la vue de l'agent).
   *
   * En regardant les cases qu'il voit, l'agent va faire un choix sur l'action a effectuer. Il est a noter que l'action donnée sera "bonne" et ne necessitera pas de verification. La sortie est une chaine de caracteres qui detaille l'action voulue. Elle sera interpreter dans la fonction appelante.
   */
  virtual std::string decider(const std::list<Case*> *cases) const;

  /*!
   * \brief Permet à l'agent de voir les cases visibles par lui-meme
   * \param env : Environnement.
   *
   * Selon les criteres de vue choisis, cette fonction va renvoyer la liste des cases importantes que vois l'agent. Elle pourra considerer les autres comme vide.
   */
  virtual std::list<Case*> voirEnvironnement(const Environnement *const env) const;


 public:
  /*!
   * \brief Constructeur pour agent
   *
   * \param PdV : nombre de point de vie de l'abeille.
   * \param position : position de l'abeille.
   * \param PdA : point d'attaque de l'abeille.
   */
  Agent(int PdV, Position pos, int PdA);

  /*!
   * \brief Desttructeur de agent
   *
   */
  ~Agent();

  /*!
   * \brief L'agent recoit l'attaque d'un autre agent
   * \param pa : Point d'attaque.
   *
   * Cette methode permet à des agents exterieurs d'attaquer l'agent. Il perdra un nombre de point de vie egale au point d'attaque.
   */
  void seFaireAttaquer(int pa);

  /*!
   * \brief Renvoie le nombre de point de vie de l'Agent.
   * \return entier representant le nombre de point de vue.
   *
   *  Cette fonction permet à un agent exterieur de se renseigner sur les points de vie de l'agent.
   */
  int getPV() const;

  /*!
   * \brief Renvoie le nombre de points d'attaque de l'Agent.
   * \return entier representant le nombre de point d'attaque.
   *
   *  Cette fonction permet à un agent exterieur de se renseigner sur les points d'attaque de l'agent.
   */
  int getPA() const;

  /*!
   * \brief Retourne la position de l'Agent.
   * \return : La position de l'agent.
   */
  Position getPosition() const;

	/*!
	 * \brief fait effectuer une action a l'Agent
	 * \param env : environnement dans lequel l'agent évolue.
	 *
	 * Cette methode va demander à l'agent d'effectuer une action. Il va donc d'abord regarder son environnement, puis decider de l'action qu'il va effectuer, puis la faire.
	 */
  virtual bool effectuerAction(Environnement *const env);

 /**
   * \brief Renvoie le type de l'agent
   *
   * Renvoie une chaîne représentant le type de l'agent.
   */
  virtual std::string getTypeAgent() const;

  /*!
   * \brief Permet de savoir la quantite de pollen que l'abeille transporte.
   *
   * Methode qui retourne la quantite de pollen que l'abeille transporte. Pour les autres agents, renvoie 0.
   *
   * \return un entier representant la quantite de pollen que l'abeille transporte.
   */
  virtual int getPollen() const;

  /*!
  *  \brief Recupère l'oeuf porté par la fourmi.
  *
  *  Methode qui permet de recuperer l'oeuf de puceron porté par la fourmi sous la forme d'un pointeur. Pour les autres agents, renvoie un nullptr.
  *
  *  \return un objet de type OeufDePuceron désignant l'oeuf porté par la fourmi.
  */
  virtual Case *getOeufPorte() const;
};

#endif  // INCLUDE_AGENT_HPP_
