#ifndef ENVIRONNEMENT_H
#define ENVIRONNEMENT_H
	/*!
	 * \file Environnement.hpp
	 * \brief Declaration de la classe Environnement
	 * \author Jorge OCHOA
	 * \version 0.1
	 */

#include <vector>
#include <cstddef>
#include "Case.hpp"
using std::vector;
/// class Agent;

	/*! \class Environnement
	   * \brief Classe representant la grille sur laquelle se déroulera la simulation.
	   *
	   */

	class Environnement {

		private :
			int longueur; /*!< longueur de la grille.*/
			int hauteur;  /*!< hauteur de la grille.*/
			vector<vector<Case*> > grille; /*!< Grille de la simultaion. (premier indice = ligne = position en y ,
													deuxieme = colonne = position en x)
													x croit vers la droite
													y croit vers le bas*/


		public :
			/*!
	    		 		 *  \brief Constructeur
	    				 *
	    				 *  Constructeur de la classe Environnement.
	    				 *\param l longueur de la grille.
		        		 *\param h hauteur de la grille.
	    				 */
			Environnement(int l, int h);

			~Environnement();

			/*!
					 *  \brief Constructeur
					 *
					 *  Constructeur de la classe Environnement.
					 *\param l longueur de la grille.
					 *\param h hauteur de la grille.
					 *\param tab grille pour la simulation.
					 */
			Environnement(int l, int h, vector<vector<Case*> > tab);

			/*!
					 *  \brief Getter
					 *
					 *  Permet de recuperer la hauteur de la grille.
					 * \return Retourne la hauteur de la grille.
					 */
			int getHauteur() const;

			/*!
					 *  \brief Getter
					 *
					 *  Permet de recuperer longueur de la grille.
					 * \return Retourne longeur de la grille.
					 */
			int getLongueur() const;

			/*!
					 *  \brief Getter
					 *
					 *  Permet de recuperer la grille.
				 * \return Retourne la grille.
					 */
			vector<vector<Case*> > getGrille() const;

			/*!
					 *  \brief Getter
					 *
					 *  Permet de récupérer une case spécifique de la grille.
					 * \param x position en x de la case (correspond a la colonne)
					 * \param y postion en y de la case (correspond a la ligne)
					 * \return Retourne un pointeur sur la case spécifique de la grille.
					 */
			Case* getCaseAt(int x, int y) const;

			/*!
					 *  \brief Setter
					 *
					 *  Permet d'atribuer une case donnée dans la grille.
					 * \param x position en x de la case (correspond a la colonne)
					 * \param y postion en y de la case (correspond a la ligne)
					 * \param cel pointeur la case que l'on veut mettre dans le tableau
					 */
			void setCaseAt(int x, int y, Case* cel);

			/*!
					 *  \brief Setter
					 *
					 *  Permet d'atribuer un agent donnée à une case dans la grille.
					 * \param x position en x de la case (correspond a la colonne)
					 * \param y postion en y de la case (correspond a la ligne)
					 * \param a pointeur sur l'agent qu'on veut mettre dans le tableau
					 */
			void setAgentAt(int x, int y, Agent* a);

			/*!
					 *  \brief Getter
					 *
					 *  Permet de récupérer l'agent à une case donnée.
					 * \param x position en x de la case (correspond a la colonne)
					 * \param y postion en y de la case (correspond a la ligne)
					 * \return Retourne un pointeur sur l'agent ou NULL si la case
					 *				 ne contient pas d'agent.
					 */
			Agent *getAgentAt(int x, int y) const;


			/*!
					 *  \brief Mise à jour
					 *
					 *  Appelle miseAJour sur chaque case de la grille
					 */
			std::vector<Agent *> miseAJour();

	};
#endif
