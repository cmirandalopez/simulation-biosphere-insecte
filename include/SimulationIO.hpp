#ifndef INCLUDE_SIMULATIONIO_HPP_
#define INCLUDE_SIMULATIONIO_HPP_

/**
 * \file SimulationIO.hpp
 * \brief Déclaration de la classe SimulationIO
 * \author Carlos Miranda
 * \version 0.1
 */

#include <string>
#include "Simulation.hpp"

using std::string;
using std::istream;
using std::ostream;
using std::string;
using std::cout;
using std::endl;
using std::cin;

/**
 * \class SimulationIO
 * \brief Classe de contrôle d'entrées/sorties pour une simulation
 *
 * Cette classe permet l'affichage, lecture et écriture d'une simulation.
 */
class SimulationIO {
 public:
  /**
   * \brief Constructeur par défaut
   */
  SimulationIO();

  /**
   * \brief Destructeur
   */
  ~SimulationIO();

  /**
   * \brief Constructeur par affectation (pour écriture)
   *
   * \param simulation : une simulation
   */
  explicit SimulationIO(const Simulation * const simulation);

  /**
   * \brief Initialise l'interface et affiche le menu général
   */
  void menu_general();

 private:
  /**
   * \brief Construction d'une simulation à partir d'un fichier
   *
   * Construit une simulation à partir d'un fichier donné (dont le nom est donné par fichier_lecture).
   */
  int lire();

  /**
   * \brief Construction d'une simulation par interaction avec l'utilisateur
   *
   * Demande les paramètres dans la console.
   */
  void menu_creation();

  /**
   * \brief Sauvegarde de la simulation dans un fichier
   *
   * Ecriture de l'état actuel de la simulation dans un fichier.
   *
   * \param nom_fichier : Nom du fichier d'écriture
   */
  void ecrire() const;

  /**
   * \brief Remplissage de l'environnement de façon interactive
   * 
   * Remplissage de l'environnement via la ligne de commandes
   */
  void remplir_environnement_interactif(
    int longueur, int hauteur, int nb_abeilles,
    int nb_fourmis, int nb_pucerons);

  /**
   * \brief Ecrit la simulation à l'étape actuelle de façon graphique
   * 
   * La simulation est écrite dans deux fichiers XXX_genvY.txt et XXX_gagtY.txt avec XXX la racine
   * d'écriture définie par l'utilisateur et Y le numéro de l'itération courante.
   */
  void ecrire_graphique() const;

  /**
   * \brief Lecture d'un ensemble de fichiers
   * 
   * Demande à l'utilisateur d'entrée la racine des fichiers à lire puis crée une simulation
   * à partir des données contenues dans ces fichiers.
   */
  void menu_lecture();

  /**
   * \brief Ecriture d'un ensemble de fichiers
   * 
   * Demande à l'utilisateur de configurer l'écriture (fréquence d'écriture, racine et écriture graphique).
   * En plus, permet l'écriture de la simulation à l'étape actuelle.
   */
  void menu_ecriture();

  /**
   * \brief Exécution de la simulation
   * 
   * Permet de déterminer le nombre de pas à exécuter et d'exécuter la simulation.
   */
  void menu_execution();

  /**
   * \brief Affiche de l'information sur ce projet
   */
  void information();

  /**
   * \brief  Permet la configuration de l'écriture (fréquence d'écriture, racine et écriture graphique)
   */
  void configuration_ecriture();

  /**
   * \brief Lance la simulation
   * 
   * Lance la simulation et effectue un certain nombre de pas déterminé par nb_iterations_arret et 
   * le numéro de la simulation actuelle (voir Simulation::get_iterations).
   */
  void executer();

  Simulation simulation; /** Simulation à manipuler */
  std::string fichier_lecture;  /** Racine des fichiers de lecture */
  std::string fichier_ecriture;  /** Racine des fichiers d'écriture */
  int frequence_ecriture;  /** Fréquence d'écriture */
  bool ecriture_graphique;  /** Ecrire graphiquement ou pas */
  int nb_iterations_arret;  /** Nombre d'itérations avant arrêt */
};

#endif  // INCLUDE_SIMULATIONIO_HPP_
