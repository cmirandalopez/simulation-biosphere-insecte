//! Cette classe représente une case de type Fourmilière.
#ifndef INCLUDE_FOURMILIERE_HPP_
#define INCLUDE_FOURMILIERE_HPP_

/**
 * \file Fourmiliere.hpp
 * \brief Déclaration de la classe Fourmiliere
 * \author Carlos Miranda
 * \version 0.1
 */

#include <string>
#include "Statique.hpp"
#include "Case.hpp"

/** \class Fourmiliere
 *  \brief Classe représentant une case de type Fourmiliere.
 *  Cette classe représente une case de type Fourmilière. Le but est de caractériser
 *  les cases autour desquelles les fourmis pourront déposer les oeufs de
 *  puceron. Cette classe est vide, elle ne sert qu'à identifier un type de
 *  case.
 */
class Fourmiliere: public Statique {
public:

	/*!
	*  \brief Constructeur
	*
	*  Constructeur de la classe Fourmiliere
	*/
  Fourmiliere();

  ~Fourmiliere();

  Fourmiliere(int x, int y);

	/**
   * \brief Renvoie le type de la case
   *
   * Renvoie une chaîne représentant le type de la case.
   */

  virtual std::string getTypeCase() const;
};

#endif  // INCLUDE_FOURMILIERE_HPP_
