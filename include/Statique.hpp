//! Cette classe représente les éléments statiques de l'environnement.
#ifndef INCLUDE_STATIQUE_HPP_
#define INCLUDE_STATIQUE_HPP_

/**
 * \file Statique.hpp
 * \brief Déclaration de la classe Statique
 * \author Carlos Miranda
 * \version 0.1
 */

#include <string>
#include "Case.hpp"

/** \class Statique
 *  \brief Classe représentant une case de type Statique.
 *  Cette classe représente les éléments statiques de l'environnement
 *  (Fourmilière et Ruche). Elle hérite de Case et est abstraite.
 */
class Statique: public Case {
 public:
  /**
   *  \brief Constructeur
   *
   *  Constructeur de la classe Environnement.
   */
  Statique();

  ~Statique();

  Statique(int x, int y);

  /**
   * \brief Renvoie le type de la case
   *
   * Renvoie une chaîne représentant le type de la case.
   */
  virtual std::string getTypeCase() const;
};

#endif  // INCLUDE_STATIQUE_HPP_
