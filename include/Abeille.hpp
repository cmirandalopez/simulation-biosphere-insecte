#ifndef INCLUDE_ABEILLE_HPP_
#define INCLUDE_ABEILLE_HPP_

/*!
* \file Abeille.hpp
* \brief Declaration de la classe Abeille
* \author FRAGNAUD Cedric
* \author MARCUSANU Ana
* \author PROTAIS Francois
* \version 0.1
*/

#include <string>
#include <list>
#include "Agent.hpp"
#include "Fleur.hpp"
#include "Position.hpp"
#include "Alveole.hpp"

/*! \class Abeille
* \brief Classe representant les abeilles.
*
* Elle herite de Agent.
*/
class Abeille: public Agent {

    #ifdef UNIT_TEST
      friend class TestAbeille;
    #endif

 private:
  int pollen; /*!< Entier designant la quantite de pollen que l'abeille transporte.*/


  /*!
   * \brief Permet a l'abeille d'augmenter la quantité de pollen qu'elle transporte.
   *
   * Méthode qui incremente l'attribut pollen de l'abeille de 1, et appelle perdrePollen de la fleur rentrée en paramètre.
   *
   * \param fleur: fleur que l'abeille butine.
   */
  int butiner(Case *const fleur);

  /*!
   * \brief L'abeille crée une alvéole à partir du pollen qu'elle transporte.
   *
   * Méthode qui crée une alvéole (la vérification quant à la quantité de pollen transportée doit être faite avant l'appel de cette fonction).
   *
   * \param pos : position à laquelle l'abeille crée l'alvéole.
   * \param env: Environnement dans lequel on crée l'alvéole
   */
  void creerAlveole(const Position pos, Environnement *env);

  /*!
   * \brief L'abeille construit l'alvéole à partir du pollen qu'elle transporte.
   *
   * Méthode qui permet la construction d'une alvéole créée auparavant (la vérification quant à la quantité de pollen transportée doit être faite avant l'appel de cette fonction).
   *
   * \param alveole : alvéole que l'abeille construit.
   */
  void construireAlveole(Case *alveole);

  public:
  /*!
   * \brief Constructeur
   *
   * Constructeur de la classe Abeille.
   *
   * \param PdV : nombre de point de vie de l'abeille.
   * \param position : position de l'abeille.
   * \param PdA : point d'attaque de l'abeille.
   * \param pol: quantite de pollen que l'abeille transporte.
   */
  Abeille(int PdV, Position pos, int PdA, int pol);

  /*!
   * \brief Desttructeur
   *
   * Destructeur de la classe Abeille.
   *
   */
  virtual ~Abeille();

  int getPollen() const;

  // class virtual à declarer :
  void attaquer(Agent *const a) const override;

  void bouger(Position p, Environnement *const e) override;

  std::string decider(const std::list<Case*> *cases) const override;

  std::list<Case *> voirEnvironnement(const Environnement *const env) const override;

  bool effectuerAction(Environnement *const env) override;

  std::string getTypeAgent() const;
};


#endif  // INCLUDE_ABEILLE_HPP_
