#ifndef INCLUDE_FLEUR_HPP_
#define INCLUDE_FLEUR_HPP_

#include <string>
#include "Case.hpp"
#include "Position.hpp"
#include "Dynamique.hpp"
#include "Environnement.hpp"


/*!
 * \file Fleur.hpp
 * \brief Declaration de la classe Fleur
 * \author Clara Moriceau and Elsa Laval and Winnie the Pooh
 * \version 0.1
 */

/*! \class Fleur
   * \brief Classe representant les fleurs.
   *
   * Elle herite de Dynamique.
   */

class Fleur : public Dynamique{
 private:
  int pollen; /*!< Entier qui represente le nombre de pollen qu'il reste a la fleur.*/
  static int tempsMiseAJour; /*!< entier statique qui represente le moment ou toutes les fleurs recuperent leur pollen.
  * (Similaire a une saison, toutes les fleurs regagnent tout leur pollen en meme temps).
	* Cet entier est mis a jour à chaque tour. */


 public:
  /*!
   *  \brief Constructeur
   *
   *  Constructeur de la classe Fleur
   *
   * \param nbPollen : nombre de pollens que la fleur aura.
   * \param pos: La position à laquelle se trouvera la fleur.
   * \param tempsDEvol : temps que la fleur va mettre à se faire manger.
   */
  Fleur(int nbPollen, Position pos, int tempsDEvol);

  ~Fleur();

  /*!
   *  \brief Savoir combien de pollen a la fleur (Accesseur)
   *
   *  \return le nombre de pollen qu'a la fleur
   */
  int getnbPollen() const;

  /*!
   *  \brief Fait perdre du pollen a la fleur
   *
   *  Methode qui decremente le pollen de 1 lorsque la fleur se fait butiner.
   *
   */
  void perdrePollen();

  /*!
   *  \brief Savoir si la fleur a du pollen
   *
   *  Methode qui permet de savoir si la fleur a du pollen, c'est a dire si l'entier pollen est  egal ou different de 0.
   *
   *  \return vrai s'il reste du pollen a la fleur, faux sinon.
   */
  bool aPollen();

  /*!
  *  \brief Redéfinition de la méthode miseAjour
  *
  *
  *  \return 1 si on doit effacer la fleur
  */
  Agent* miseAJour(Environnement *e);

 /**
   * \brief Renvoie le type de la case
   *
   * Renvoie une chaîne représentant le type de la case.
   */
  virtual std::string getTypeCase() const;
};
#endif  // INCLUDE_FLEUR_HPP_
