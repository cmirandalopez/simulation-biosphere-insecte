#ifndef INCLUDE_PUCERON_HPP_
#define INCLUDE_PUCERON_HPP_

/*!
* \file Puceron.hpp
* \brief Declaration de la classe Puceron
* \author FRAGNAUD Cedric
* \author MARCUSANU Ana
* \author PROTAIS Francois
* \version 0.1
*/

#include <string>
#include <iostream>
#include <list>
#include "Agent.hpp"
#include "Position.hpp"
#include "Environnement.hpp"
#include "Case.hpp"
#include "Fleur.hpp"

/*! \class Puceron
* \brief Classe representant les pucerons.
*
* Elle herite de Agent.
*/
class Puceron : public Agent {

    public:
        /*!
        * \brief Constructeur
        *
        * Constructeur de la classe Puceron.
        *
        * \param PdV : nombre de point de vie du puceron.
        * \param position : position du puceron.
        * \param PdA : point d'attaque du puceron.
        */
        Puceron(int PdV, Position position, int PdA);

    virtual ~Puceron();

    // class virtual à declarer :
    void attaquer(Agent *const a) const;

        void bouger(Position p, Environnement *const env);

        std::string decider(const std::list<Case*> *cases) const;

        bool effectuerAction(Environnement *const env) override;

        std::list<Case *> voirEnvironnement(const Environnement *const env) const override;

    /// private:
    /*!
    *  \brief Le puceron mange une fleur donnée.
    *
    *  Méthode qui diminue le temps d’évolution de la fleur.
    *
    * \param fleur : fleur que le puceron mange.
    * \param env: environnement dans lequel le puceron et la fleur évoluent.
    */
    void manger(Case *const fleur, Environnement *const env);

    virtual std::string getTypeAgent() const;

};

#endif  // INCLUDE_PUCERON_HPP_
