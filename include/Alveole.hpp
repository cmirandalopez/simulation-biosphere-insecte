#ifndef INCLUDE_ALVEOLE_HPP_
#define INCLUDE_ALVEOLE_HPP_

#include "Case.hpp"
#include "Position.hpp"
#include "Dynamique.hpp"
#include "Environnement.hpp"

/*!
 * \file Alveole.hpp
 * \brief Declaration de la classe Alveole
 * \author Clara Moriceau
 * \author Elsa Laval
 * \version 0.1
 */

/*! \class Alveole
   * \brief Classe representant les alveoles.
   *
   * Elle herite de Dynamique. Les alveoles sont propres aux abeilles.
   */
class Alveole : public Dynamique {
 private:
  int nbVoisins; /*!< Entier qui décrit le nombre d'alveoles voisines que l'objet a.*/
  int tempsConstruction; /*!< Entier qui decrit le temps restant a l'alveole avant d'etre construite. */
  bool enEvolution; /*!< Boolean qui precise si l'alveole est en evolution, c'est a dire si elle cree une abeille. */

 public:
  /*!
   *  \brief Constructeur
   *
   *  Constructeur de la classe Alveole
   *
   * \param nb : nombre de voisins actuels a l'alveole
   * \param temps : temps initial de construction
   * \param evolution : vrai si l'alveole est dans le processus de creation d'une abeille, faux sinon.
   * \param pos : position de l'alveole.
   * \param tempsDEvol : temps d'évolution de l'alvéole.
   */
  Alveole(int nb, int temps, bool evolution, Position pos, int tempsDEvol);

  ~Alveole();

  /*!
   *  \brief Recuperer le nombre de voisins
   *
   *  Methode qui permet de recuperer le nombre de voisins associes a l'objet alveole.
   *
   *  \return un entier correspondant au nombre de voisins actuels.
   */
  int getNbVoisins() const;

  /*!
   *  \brief Parametrer le nombre de voisins.
   *
   *  Methode qui permet de changer le nombre de voisins associes a l'objet alveole.
   *
   *  \param nombre : un entier qui correspond au nombre de voisins.
   */
  void setNbVoisins(int nombre);

  /*!
   *  \brief Decremente le temps de construction
   *
   *  \param tempsConstruit : temps qui doit etre deduit de tempsConstruction
   */
  virtual void construire(int tempsConstruit);

  /*!
   *  \brief Recuperer le temps de construction restant.
   *
   *  Methode qui permet de recuperer le temps de construction restant a l'alveole.
   *
   *  \return un entier correspondant au temps restant.
   */
  int getTempsCons() const;

  /*!
   *  \brief Lance l'evolution de l'alveole
   *
   *  Methode qui met le boolean enEvolution a true.
   *
   */
  void lancerEvolution();

  /*!
   *  \brief Recupérer le booléen
   *
   *  \return un booléen
   */
  bool getEnEvolution() const;

  /*!
   * \brief Redéfinition de la méthode miseAjour
   *
   * \return 1 si on doit effacer la fleur
   */
  Agent* miseAJour(Environnement *e);

 /**
   * \brief Renvoie le type de la case
   *
   * Renvoie une chaîne représentant le type de la case.
   */
  virtual std::string getTypeCase() const;
};
#endif  // INCLUDE_ALVEOLE_HPP_
