#ifndef INCLUDE_SIMULATION_HPP_
#define INCLUDE_SIMULATION_HPP_

/**
 * \file Simulation.hpp
 * \brief Declaration de la classe Simulation
 * \author Carlos Miranda
 * \version 0.1
 */

#include <vector>
#include "Environnement.hpp"
#include "Abeille.hpp"
#include "Fourmi.hpp"
#include "Puceron.hpp"

/** 
 * \class Simulation
 * \brief Classe representant une simulation.
 *
 * Une simulation regroupe des paramètres d'execution, les agents
 * et l'environnement.
 */
class Simulation {
  #ifdef UNIT_TEST
  friend class TestSimulation;
  #endif

 public:
  /**
   * \brief Constructeur
   * 
   * Constructeur de la classe Simulation. Génère un environnement et positionne les agents aléatoirement.
   *
   * \param hauteur : hauteur de la grille d'environnement
   * \param longueur : longueur de la grille d'environnement
   * \param nb_abeilles : le nombre initial d'abeilles
   * \param nb_fourmis : le nombre initial de fourmis
   * \param nb_pucerons : le nombre initial de pucerons
   */
  Simulation(
          int hauteur,
          int longueur,
          int nb_abeilles,
          int nb_fourmis,
          int nb_pucerons);

  /**
   * \brief Constructeur par copie
   * 
   * Constructeur par copie (superficielle).
   * 
   * \param Simulation : la simulation à copier
   */
  explicit Simulation(Simulation const * const simulation);

  /**
   * \brief Constructeur par défaut
   *
   * Constructeur par défaut (les paramètres sont à spécifier après)
   */
  Simulation();

  /**
   * \brief Destructeur
   *
   * Les agents sont alloués dynamiquement par la simulation, donc on les détruits. On ne 
   * peut pas les détruire dans le destructeur de Case ou d'Environnement car ces classes ne
   * connaissent qu'une forward declaration de la classe Agent.
   */
  ~Simulation();

  /**
   * \brief Affectation d'un environnement
   *
   * Permet l'affectation dynamique d'un environnement (p.e. lu à partir d'un fichier)
   * 
   * \param environnement : l'environnement à affecter
   */
  void set_environnement(Environnement * const environnement);

  /**
   * \brief Renvoie l'environnement de la simulation
   *
   * Permet la récuperation de l'environnement (e.g. pour écriture dans un fichier)
   */
  const Environnement *get_environnement() const;

  /**
   * \brief Affectation d'abeilles
   *
   * Permet l'affectation dynamique d'une liste d'abeilles.
   *
   * \param abeilles : une liste d'abeilles
   */
  void set_abeilles(std::vector<Abeille *> * const abeilles);

  /**
   * \brief Renvoie la liste d'abeilles
   *
   * Permet la récupération des abeilles de la simulation.
   */
  std::vector<const Abeille *> get_abeilles() const;

  /**
   * \brief Affectation des fourmis
   *
   * Permet l'affectation dynamique d'une liste de fourmis.
   *
   * \param fourmis : une liste des fourmis
   */
  void set_fourmis(std::vector<Fourmi *> *const fourmis);

  /**
   * \brief Renvoie la liste des fourmis
   *
   * Permet la récupération des fourmis de la simulation.
   */
  std::vector<const Fourmi *> get_fourmis() const;

  /**
   * \brief Affectation de pucerons
   *
   * Permet l'affectation dynamique d'une liste de pucerons.
   *
   * \param pucerons : une liste de pucerons
   */
  void set_pucerons(std::vector<Puceron *> *const pucerons);

  /**
   * \brief Renvoie la liste de pucerons
   *
   * Permet la récupération des pucerons de la simulation.
   */
  std::vector<const Puceron *> get_pucerons() const;

  /**
   * \brief Exécution d'un pas de la simulation
   *
   * Exécution d'un seul pas de la simulation.
   */
  void executer_un();

  /**
   * \brief Affectation du numéro de l'itération.
   *
   * Permet l'affectation dynamique du numéro de l'itération courante.
   *
   * \param nb_iterations : le nombre d'itérations
   */
  void set_iterations(const int nb_iterations);

  /**
   * \brief Renvoie le numéro de l'itération actuelle
   *
   * Permet la récupération du numéro de l'itération actuelle.
   */
  int get_iterations() const;

 private:
  /**
   * \brief Place les cases de type Ruche
   *
   * Modifie l'environnement en ajoutant des cases de type Ruche selon le nb_ruches_init.
   */
  void init_ruches();

  /**
   * \brief Place les cases de type Fourmiliere
   *
   * Modifie l'environnement en ajoutant des cases de type Fourmiliere selon nb_fourmilieres_init.
   */
  void init_fourmilieres();

  /**
   * \brief Place les cases de type Fleur
   *
   * Modifie l'environnement en ajoutant des cases de type Fleur selon nb_fleurs_init.
   */
  void init_fleurs();

  /**
   * \brief Place les agents dans l'environnement de façon aléatoire
   *
   * Modifie l'environnement en ajoutant des agents selon nb_abeilles_init, nb_fourmis_init et nb_pucerons_init.
   */
  void init_agents();

  Environnement environnement; /** Environnement de la simulation */
  std::vector<Abeille *> abeilles; /** Liste d'agents de type Abeille */
  std::vector<Fourmi *> fourmis; /** Liste d'agents de type Fourmi */
  std::vector<Puceron *> pucerons; /** Liste d'agents de type Puceron */
  int nb_ruches_init, nb_fourmilieres_init, nb_fleurs_init;
  int nb_abeilles_init, nb_fourmis_init, nb_pucerons_init;
  int nb_iterations;  /** L'itération actuelle de la simulation */
};

#endif  // INCLUDE_SIMULATION_HPP_
