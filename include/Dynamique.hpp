#ifndef INCLUDE_DYNAMIQUE_HPP_
#define INCLUDE_DYNAMIQUE_HPP_

#include "Dynamique.hpp"
#include <string>
#include "Case.hpp"
#include "Position.hpp"
#include "Environnement.hpp"

/*!
 * \file Dynamique.hpp
 * \brief Declaration de la classe Dynamique
 * \author Clara Moriceau and Elsa Laval
 * \version 0.1
 */

/*! \class Dynamique
   * \brief Classe representant les entites dynamiques comme les alveoles, oeufs de pucerons et fleurs.
   *
   * \author Clara Moriceau and Elsa Laval
   * \author Carlos MIRANDA <carlos.miranda_lopez@insa-rouen.fr>
   * C'est la classe mere des classes Alveoles, OeufsDePucerons et Fleurs.
   */
class Dynamique : public Case {
 private :
  int tempsDEvolution; /*!< Entier qui decrit le temps que l’alveole met à faire une abeille, que l’oeuf met a faire une fourmi ou un puceron et que la fleur met a se faire manger.*/

 public :
  /*!
   *  \brief Constructeur
   *
   *  Constructeur de la classe Dynamique
   *
   * \param temps : temps initial d'evolution
   * \param pos : la position où se trouve l'objet
   */
  Dynamique(int temps, Position pos);

  ~Dynamique();

	/*!
   *  \brief Recuperer le temps d'evolution
   *
   *  Methode qui permet de recuperer le temps d'evolution en cours de l'objet Dynamique
   *
   *  \return un entier correspondant au temps d'evolution restant
   */
  int getTempsDEvol() const;

	/*!
   *  \brief Change le temps d'evolution
   *
   *  \param tempsE : le nouveau temps d'evolution
   */
  void setTempsDEvol(int tempsE);

  /*!
   *  \brief Met a jour l'objet de type Dynamique
   *
   * Appelée à chaque fin de tour sur tous les objets dynamiques.
   * Regarde si le tempsDEvolution est a 0 et agit en fonction.
   * Pour l’alveole, appelle creerAbeille.
   * Pour l’oeuf, appelle creerFourmi ou creerPuceron en fonction du booleen adjacentFourmiliere.
   * Pour la fleur, elle disparaît.
   * Si tempsDEvolution est different de 0, on verifie pour l’alveole si elle est en evolution
   * ou pour l’oeuf s’il n’est pas porte, si oui on le decremente.
   */
  virtual Agent* miseAJour(Environnement *e) = 0;

   /**
   * \brief Renvoie le type de la case
   *
   * Renvoie une chaîne représentant le type de la case.
   */
  virtual std::string getTypeCase() const;
};
#endif  // INCLUDE_DYNAMIQUE_HPP_
