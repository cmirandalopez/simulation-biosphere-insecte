
/*!
 * \file Case.hpp
 * \brief Declaration de la classe Case
 * \author Jorge OCHOA
 * \author Carlos MIRANDA
 * \version 0.1
 */

#ifndef INCLUDE_CASE_HPP_
#define INCLUDE_CASE_HPP_

#include <string>
#include "Position.hpp"

class Agent;

/*! \class Case
  * \brief Classe representant une case de la grille sur laquelle se deroulera la simulation.
  *
  */
class Case {
 protected:
  Position position; /*!< Position sur la grille. */
  Agent* agent; /*!< Pointeur sur l'agent contenu dans la case */

 public:
  /*!
   * \brief Constructeur
   *
   * Constructeur de la classe Case
   */
  Case();

  virtual ~Case();

  /*!
   * \brief Constructeur
   *
   * Constructeur de la classe Case
   *
   * \param pos : Position de la case sur la grille.
   */
  explicit Case(Position pos);

  /*!
   * \brief Constructeur
   *
   * Constructeur de la classe Case
   *
   * \param x : Position en x de la case sur la grille (correspond a la colonne).
   * \param y : Position en y de la case sur la grille (correspond a la ligne).
   */
  Case(int x, int y);

  /**
   * \brief Renvoie le type de la case
   *
   * Renvoie une chaîne représentant le type de la case.
   */
  virtual std::string getTypeCase() const;

  /*!
   *  \brief Getter
   *
   *  Permet de recuperer la position de la case courante
   * \return Retourne la position a laquelle se trouve la case
   */
  Position getPosition();

  /*!
   *  \brief Setter
   *
   *  Permet de fixer la position de la case courante
   *
   * \param pos : Position de la case sur la grille.
   */
  void setPosition(Position pos);

  /*!
   *  \brief Setter
   *
   *  Permet de fixer l'agent contenu dans la case
   *
   * \param a : pointeur sur l'agent que l'on veut ajouter.
   */
  void setAgent(Agent* a);

  /*!
   *  \brief Getter
   *
   *  Permet de récupérer l'agent contenu dans la case
   *
   * \param a : pointeur sur l'agent que l'on veut ajouter.
   */
  Agent *getAgent();

/*!
   *  \brief Decremente le temps de construction
   *
   * utilisé dans Alveole
   *  \param tempsConstruit : temps qui doit etre deduit de tempsConstruction
   */
  virtual void construire(int tempsConstruit);

  /*!
   		  *  \brief Fait perdre du pollen a la fleur
   		  *
   		  *  Methode qui decremente le pollen de 1 lorsque la fleur se fait butiner.
   		  *
   		  */
 virtual void perdrePollen();
		/*!
   		  *  \brief Savoir si la fleur a du pollen
   		  *
   		  *  Methode qui permet de savoir si la fleur a du pollen, c'est a dire si l'entier pollen est egal ou different de 0.
   		  *
   		  *  \return vrai s'il reste du pollen a la fleur, faux sinon.
   		  */
		virtual bool aPollen();

    /*!
     *  \brief Recupérer le booléen
     *
     *  \return un booléen
     */
    virtual bool getEnEvolution() const;

		/*!
   		  *  \brief Changer le boolean porteFourmi.
   		  *
   		  *  \param porte : un boolean a vrai si l'oeuf est porte par une fourmi, faux sinon.
   		  */
		virtual void setPorte(bool porte);

	/*!
   *  \brief Change le temps d'evolution
   *
   *  \param tempsE : le nouveau temps d'evolution
   */
  virtual void setTempsDEvol(int tempsE);

	/*!
   *  \brief Recuperer le temps d'evolution
   *
   *  Methode qui permet de recuperer le temps d'evolution en cours de l'objet Dynamique
   *
   *  \return un entier correspondant au temps d'evolution restant
   */
  virtual int getTempsDEvol() const;

  /*!
      *  \brief Change le boolean adjacentFourmiliere
      *
      *  \param adjacent : un boolean a vrai si l'oeuf est adjacent a une fourmiliere, faux sinon.
      */
  void setAdjacent(bool adjacent);

  virtual int getNbVoisins() const;

  virtual int getTempsCons() const;

  virtual int getnbPollen() const;

  virtual bool getAdjacent() const;

  virtual bool getPorte() const;
};
#endif  // INCLUDE_CASE_HPP_
