#ifndef POSITION_H
#define POSITION_H
/*!
 * \file Position.hpp
 * \brief Declaration de la classe Position
 * \author F.Protais
 * \version 0.1
 */

 #include <string>

/*!
* \class Position
* \author F.Protais
* \brief Class servant a definir une position contenant une orientation
* Cette classe est utilisé pour representer tout les positions des objets. Elle peut etre utilise avec ou sans orientation. Les attributs de Position sont accessibles par des accesseurs
*/
class Position{

  private:int x; /*!< Entier qui decrit une position en x.*/
  private:int y; /*!< Entier qui decrit une position en y.*/
  private:char orientation; /*!< Caractere qui decrit l'orientation, 'O', 'N', 'E', 'S' ou 'x' si il n'y a pas d'oriention.*/

	/*!
	* \brief Constructeur de position.
	* \param x : Position en x.
	* \param y : Position en y.
	* \param orientation : Orientation.
	*
	* Constructeur permettant de d'initialiser une position avec une orientation.
	*/
  public:Position(int x, int y, char orientation);

  	/*!
	* \brief Constructeur de position.
	* \param x : Position en x.
	* \param y : Position en y.
	*
	* Constructeur permettant de d'initialiser une position sans orientation. L'oriention est donc fixe par default a 'x'.
	*/
  public:Position(int x, int y);

  /*!
* \brief Constructeur par default
*
* initalise à 0
*/
  public:Position();


	/*!
	* \brief Retourne la position en X.
	* \return Entier.
	*/
  public:int getX() const;
  	/*!
	* \brief Retourne la position en Y.
	* \return Entier.
	*/
  public:int getY() const;
	/*!
	* \brief Retourne l'orientation
	* \return Caractere.
	*/
  public:char getOrientation() const;
 	/*!
	* \brief Change la valeur de X de la position.
	* \param x : nouvelle position en X
	*/
  public:void setX(const int x);
  	/*!
	* \brief Change la valeur de Y de la position.
	* \param y : nouvelle position en Y
	*/
  public:void setY(const int y);
  	/*!
	* \brief Change la valeur de l'orientation de la position.
	* \param orientation : nouvelle orientation : 'N', 'O', 'S', 'E' ou 'x' (pour les objets sans orientation).
	*/
  public:void setOrientation(const char orientation);


  /*!
  *\brief convertit une position et une direction en une chaine de caracteres "MOUV posX posY" ou "TURN or"
  */
  static std::string convertitEnMouv(Position pos, int DIR);

  /**
  *\brief Cette fonction prend deux positions et donne le chemin a prendre pour se rendre à l'objet.
  *\ return MV LT, 2 MV FW, 3 MV RT, 4 TURN LT, 5 TURN AROUND, 6 TURN RT
  * A noter que cette fonction est élementaire et ne tiens pas compte de l'environnement.
  */
  static int choixMouvNonDirectACote(Position posCentre, Position posObjectif);

  /**
  *\brief cette fonction prend deux position et renvoie un entier decrivant la position en fonction de l'orientation
  *\return 1 si l'objet est à gauche, 2 en face, 3 à droite, et 0 si il est a plus de 1 case de distance
  */
  static int positionDirectParRapport(Position posCentre, Position posObjectif);

  std::string toString() const;

  Position& operator=(const Position& other);
};

#endif
