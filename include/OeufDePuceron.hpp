#ifndef OEUFDEPUCERON_H
#define OEUFDEPUCERON_H

#include "Dynamique.hpp"
#include "Position.hpp"

/*!
 * \file OeufDePuceron.hpp
 * \brief Declaration de la classe OeufDePuceron
 * \author Clara Moriceau and Elsa Laval
 * \version 0.1
 */

/*! \class OeufDePuceron
   * \brief Classe representant les oeufs de pucerons.
   *
   * Elle herite de Dynamique.
   */

class OeufDePuceron : public Dynamique {

	private :
		bool porteParFourmi; /*!< Booleen qui permet de savoir si l'oeuf est porte par une fourmi.*/
		bool adjacentFourmiliere; /*!< Booleen qui permet de savoir si l'oeuf est a cote d'une fourmiliere.*/
		static int tempsFormation; /*!< Entier qui decrit le temps restant a l'oeuf avant d'etre cree. */

	public :
		/*!
    		 *  \brief Constructeur
    		 *
    		 *  Constructeur de la classe OeufDePuceron
    		 *
		 * \param porte : boolean qui precise si l'oeuf est porte par une fourmi.
    		 * \param adjacent : boolean qui precise si l'oeuf est adjacent a une fourmiliere.

		 * \param tempsE : entier qui donne le temps d'evolution de l'oeuf, c'est a dire le temps que mettra l'oeuf a faire une fourmi ou un puceron.

		 * \param unePos : position initiale de l'oeuf.
    		 */
		OeufDePuceron(bool porte, bool adjacent, int tempsE, Position unePos);

		~OeufDePuceron();

		/*!
   		  *  \brief Savoir si l'oeuf est porte par une fourmi
   		  *
   		  *  \return vrai si l'oeuf est porte par une fourmi, faux sinon.
   		  */
		bool getPorte();

		/*!
   		  *  \brief Changer le boolean porteFourmi.
   		  *
   		  *  \param porte : un boolean a vrai si l'oeuf est porte par une fourmi, faux sinon.
   		  */
		void setPorte(bool porte);

		/*!
   		  *  \brief Savoir si l'oeuf est pres d'une fourmiliere
   		  *
   		  *  \return vrai si l'oeuf est adjacent a une fourmiliere, faux sinon.
   		  */
		bool getAdjacent();

		/*!
   		  *  \brief Change le boolean adjacentFourmiliere
   		  *
   		  *  \param adjacent : un boolean a vrai si l'oeuf est adjacent a une fourmiliere, faux sinon.
   		  */
		void setAdjacent(bool adjacent);

		/*!
   		  *  \brief Recuperer le temps de formation de l'oeuf
   		  *
   		  *  \return le temps de formation restant a l'oeuf.
   		  */

		int getTempsFormation();

		/*!
   		  *  \brief Change le temps de formation de l'oeuf
   		  *
   		  *  \param tempsF : le nouveau temps de formation de l'oeuf.
   		  */

		void setTempsFormation(int tempsF);
		/*!
   		  *  \brief Redéfinition de la méthode miseAjour
   		  *
   		  *
   		  *  \return 1 si on doit effacer la fleur
   		  */
		Agent* miseAJour(Environnement *e);

		/*!
   		  *  \brief Vérifie si l'oeuf se situe à côté d'une fourmilière
		  *  Modifie le boolean adjacentFourmiliere
   		  *
   		  */
		void nextToFourmiliere(Environnement *e);

		 /**
  		 * \brief Renvoie le type de la case
  		 *
  		 * Renvoie une chaîne représentant le type de la case.
  		 */
  		virtual std::string getTypeCase() const;
};
#endif
