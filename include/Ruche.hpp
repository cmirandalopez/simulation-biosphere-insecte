//! Cette classe représente une case de type Ruche.
#ifndef INCLUDE_RUCHE_HPP_
#define INCLUDE_RUCHE_HPP_

/**
 * \file Ruche.hpp
 * \brief Déclaration de la classe Ruche
 * \author Carlos Miranda
 * \version 0.1
 */

#include <string>
#include "Statique.hpp"
#include "Case.hpp"

/** \class Ruche
 *  \brief Classe représentant une case de type Ruche.
 *  Cette classe représente une case de type Ruche. Le but est de caractériser
 *  les cases autour desquelles les abeilles pourront former des
 *  alvéoles. Cette classe est vide, elle ne sert qu'à identifier un type de
 *  case.
 */
class Ruche: public Statique {
public:
/*!
 *  \brief Constructeur
 *
 *  Constructeur de la classe Environnement.
 */
 Ruche();

 Ruche(int x, int y);

 ~Ruche();

/**
   * \brief Renvoie le type de la case
   *
   * Renvoie une chaîne représentant le type de la case.
   */
  virtual std::string getTypeCase() const;
};

#endif  // INCLUDE_RUCHE_HPP_
