#ifndef INCLUDE_FOURMI_HPP_
#define INCLUDE_FOURMI_HPP_

/*!
* \file Fourmi.hpp
* \brief Declaration de la classe Fourmi
* \author FRAGNAUD Cedric
* \author MARCUSANU Ana
* \author PROTAIS Francois
* \version 0.1
*/

#include "Agent.hpp"
#include "OeufDePuceron.hpp"
#include "Environnement.hpp"
#include "Position.hpp"
#include "Case.hpp"
#include <iostream>
#include <list>

/*! \class Fourmi
* \brief Classe representant les fourmis.
*
* Elle herite de Agent.
*/
class Fourmi: public Agent {

    #ifdef UNIT_TEST
        friend class TestFourmi;
    #endif

	private:
		Case *oeufPorte=NULL; /*!< Désigne l'oeuf porté par la fourmi (à null si elle n'en porte pas).*/



        /*!
    	 *  \brief La fourmi vole l'oeuf d'un puceron.
    	 *
    	 *  Méthode qui fait pointer oeufPorte vers un oeuf de puceron, et modifie l'etat de cet oeuf (porteParFourmi <- true).
    	 *
		 * \param oeuf : oeuf que la fourmi vole.
		 * \param e : on supprime l'oeuf de la case où il se trouvait
    	*/
		void volerUnOeuf(Case *oeuf, Environnement *e);


		/*!
    	 *  \brief La fourmi pose l'oeuf qu'elle porte.
    	 *
    	 *  Méthode qui fait pointer oeufPorte vers null, et modifie l'etat de l'oeuf précedemment porté (porteParFourmi <- false).
    	 *
		 * \param pos : position à laquelle la fourmi dépose l'oeuf.
		 * \param e : on pose l'oeuf sur une case de l'environnement
    	*/
		void poserOeuf(Position pos, Environnement *e);

	public:
		/*!
    	 *  \brief Constructeur
    	 *
    	 *  Constructeur de la classe Fourmi, avec un oeuf.
    	 *
		 * \param PdV : nombre de point de vie de la fourmi.
    	 * \param position : position de la fourmi.
		 * \param PdA : point d'attaque de la fourmi.
		 * \param oeuf: oeuf porté par la fourmi
    	*/
		Fourmi(int PdV, Position position, int PdA, OeufDePuceron *oeuf);

        /*!
    	 *  \brief Destructeur
    	 *
    	 *  Desttructeur de la classe Fourmi
    	*/
	    virtual ~Fourmi();

	    // class virtual à declarer :
        void attaquer(Agent *const a) const override;

		Case *getOeufPorte() const;

        void bouger(Position p, Environnement *const e) override;

        std::string decider(const std::list<Case*> *cases) const override;

        std::list<Case *> voirEnvironnement(const Environnement *const env) const override;

        bool effectuerAction(Environnement *const env) override;

        virtual std::string getTypeAgent() const;
};


#endif
