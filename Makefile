#Makefile projetCPP
OPT = -Wall
CXX = g++
SRCDIR = src
BUILDDIR = build
TESTDIR = test
TARGET = bin/main

SRCEXT = cpp
SOURCES = $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJETS = $(patsubst $(SRCDIR)/%, $(BUILDDIR)/%, $(SOURCES:.$(SRCEXT)=.o))
TESTSRC = $(filter-out src/main.cpp, $(SOURCES))
TESTOBJ = $(patsubst $(SRCDIR)/%, $(BUILDDIR)/%, $(TESTSRC:.$(SRCEXT)=.o))
CFLAGS = -g -Wall -std=c++11
LDFLAGS = -lcppunit -DUNIT_TEST
INC = -I include

# Edition de liens et production de l'exécutable
# Il suffit de faire make
$(TARGET): $(OBJETS)
	@mkdir -p bin
	$(CXX) $^ -o $(TARGET)

# Compilation des sources
# Exemple : make build/Puceron.o
$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	$(CXX) $(CFLAGS) $(INC) -c -o $@ $< -D DEBUG

# Tests
# Exemple : make test/TestAbeille.o
$(TESTDIR)/%.o : $(TESTDIR)/%.$(SRCEXT)
	@mkdir -p $(TESTDIR)
	$(CXX) $(CFLAGS) $(INC) -c -o $@ $< $(LDFLAGS)

$(TESTDIR)/%.exe : $(TESTDIR)/%.o $(TESTOBJ)
	@mkdir -p $(TESTDIR)
	$(CXX) $(CFLAGS) $(INC) $^ -o $@ $(LDFLAGS)

# Nettoyage
clean:
	rm -rf $(BUILDDIR)/* $(TESTDIR)/*.o $(TESTDIR)/*.exe $(TARGET)

# Documentation
doc:
	doxygen doc/Doxyfile
	make -C doc/latex all

.PHONY: clean doc
