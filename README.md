#### Projet de simulation d'une biosphère d'insectes

### Informations générales

## Motivation

Ce projet s'inscrit dans le cadre du cours Génie Logiciel en C++ du département GM de l'Institut National des Sciences Appliquées de Rouen.
Le but est de mettre en place une démarche de génie logiciel par le biais du développement d'un projet informatique en C++.

## Objectifs

- Suivre une démarche de Génie Logiciel, i.e. mise en place de phases d'analyse, de test et d'intégration.
- Développer une application fonctionnelle dans le délai imparti.
- Mettre en pratique les connaissances du langage C++.

## Groupe

Ce projet a été développé par :
- Cédric FRAGNAUD <cedric.fragnaud@insa-rouen.fr>
- Elsa LAVAL <elsa.laval@insa-rouen.fr>
- Ana MARCUSANU <ana.marcusanu@insa-rouen.fr>
- Carlos MIRANDA LOPEZ <carlos.miranda_lopez@insa-rouen.fr>
- Clara MORICEAU <clara.moriceau@insa-rouen.fr>
- Jorge OCHOA MAGANA <jorge.ochoa_magana@insa-rouen.fr>
- François PROTAIS <francois.protais@insa-rouen.fr>

## Sujet du projet

Ce projet a pour but de simuler une biosphère composée de 3 types d'agents : des abeilles, des fourmis et des pucerons. Chaque type d'agent se voit associé un ou plusieurs types de cases :
- Les abeilles sont associées aux fleurs (elles doivent prendre leur pollen), à la ruche et aux alvéoles (elles doivent ramener le pollen à côté des ruches pour produire des alvéoles, qui elles produiront de nouvelles abeilles).
- Les pucerons sont associées aux fleurs (ils doivent les manger pour pondre un oeuf qui produira un nouveau puceron).
- Les fourmis sont associées aux oeuf des pucerons (elles doivent les voler) et aux fourmilières (elles doivent ramener les oeufs de puceron à côté des fourmilières pour que ceux-ci produisent des fourmis et non pas des pucerons).
Tous les agents peuvent bouger et attaquer. Cependant, chaque type d'agent posséde un champ de vision particulier.
A chaque tour, l'environnement se met à jour, i.e. les fleurs peuvent regagner du pollen, les alvéoles et les oeufs peuvent éclore...

### Informations techniques

## Système de fichiers

- _bin_ : ce dossier contient le fichier binaire à exécuter (main)
- _build_ : ce dossier contient les fichiers objets (.o) servant à la compilation du programme
- _doc_ : ce dossier contient les différents éléments de documentation, notamment une documentation doxygen
- _include_ : ce dossier contient les fichiers en-tête C++ (.hpp)
- _src_ : ce dossier contient les sources du projet (.cpp)
- _test_ : ce dossier contient les sources des tests (.cpp) et éventuellement les fichiers exécutables des tests (.exe)

## Compilation et exécution des différents éléments

Il est possible de compiler chaque source avec la commande _make_ :
- _make_ compile toutes les sources de _src_, produits les objets dans _build_ et l'exécutable dans _bin_. Il suffit alors d'exécuter _./bin/main_.
- _make build/xxx.o_ compile le fichier src/xxx.o et écrit le fichier build/xxx.o
- _make test/xxx.exe_ compile et fait l'édition de liens pour le fichier test/xxx.cpp. Il produit un fichier test/xxx.exe dans le dossier _test_.
- _make debug_ agit comme _make_ mais ajoute un drapeau (-D DEBUG) permettant l'affichage d'informations de débuggage.
Un fichier _data.example_ est fourni pour le donner en tant qu'entrée à l'exécutable. Après compilation, il suffit d'exécuter _./bin/main < data.example_.
On peut afficher une séquence des résultats graphiques avec la commande :
for i in res_gagt*; do cat $i; sleep 0.25; c; done
et
for i in res_genv*; do cat $i; sleep 0.25; c; done

## Fichiers de résultats

Le projet utilise une sérialisation simple des données en texte pour stockage. L'exécution d'une simulation peut produire 4 types de fichiers :
- Ceux finissant par _envXXX.txt_ correspondent à l'environnement.
- Ceux finissant par _agtXXX.txt_ correspondent aux agents
- Ceux finissant par _gagtXXX.txt_ correspondent à une représentation graphique des agents
- Ceux finissant par _genvXXX.txt_ correspondent à une représentation graphique de l'environnement
