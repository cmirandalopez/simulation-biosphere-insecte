#include "Position.hpp"
#include <string>
#include <stdlib.h>
#include<sstream>

#ifdef DEBUG
	#include <iostream>
#endif

template <typename T>
std::string to_string(T value)
{
  //create an output string stream
  std::ostringstream os ;

  //throw the value into the string stream
  os << value ;

  //convert the string stream into a string and return
  return os.str() ;
}


Position::Position(int x, int y, char orientation) {
  Position::orientation = orientation;
  Position::x = x;
  Position::y = y;
};

Position::Position(int x, int y) {
  Position::orientation = 'x';
  Position::x = x;
  Position::y = y;
};

Position::Position() {
  Position::orientation = 'x';
  Position::x = 0;
  Position::y = 0;
};

int Position::getX() const {
	return this->x;
}

void Position::setX(const int x) {
	this->x = x;
}

int Position::getY() const {
	return this->y;
}

void Position::setY(const int y) {
	this->y = y;
}

char Position::getOrientation() const {
	return this->orientation;
}

void Position::setOrientation(const char orientation) {
	this->orientation = orientation;
}

// FONCTION SERVANT A l'AI SIMPLE
	/**
	*\brief cette fonction prend deux positions et renvoie un entier decrivant la position en fonction de l'orientation
	*\return 1 si l'objet est à gauche, 2 en face, 3 à droite, et 0 si il est a plus de 1 case de distance
	*/
	int Position::positionDirectParRapport(Position posCentre, Position posObjectif){
		int dX = posCentre.getX() - posObjectif.getX();
		int dY = posCentre.getY() - posObjectif.getY();
		if (abs(dX)+abs(dY) > 1)
			return 0;
		if (posCentre.getOrientation() == 'N'){
			if (dX != 0)
				if (dX > 0)
					return 1;
				else
					return 3;
			else
				return 2;
      }
		if (posCentre.getOrientation() == 'O'){
			if (dY != 0)
				if (dY > 0)
					return 3;
				else
					return 1;
			else
				return 2;
      }
		if (posCentre.getOrientation() == 'S'){
			if (dX != 0)
				if (dX > 0)
					return 3;
				else
					return 1;
			else
				return 2;
      }
		if (posCentre.getOrientation() == 'E'){
			if (dY != 0)
				if (dY > 0)
					return 1;
				else
					return 3;
			else
				return 2;
      }
  return 0;
	};


/**
*\brief Cette fonction prend deux positions et donne le chemin a prendre pour se rendre à l'objet.
*\ return 1 MV LT, 2 MV FW, 3 MV RT, 4 TURN  LT, 5 TURN  AROUND, 6 TURN  RT
* A noter que cette fonction est élementaire et ne tiens pas compte de l'environnement.
*/
		int Position::choixMouvNonDirectACote(Position posCentre, Position posObjectif){
			int dX = posCentre.getX() - posObjectif.getX();
			int dY = posCentre.getY() - posObjectif.getY();
			if (posCentre.getOrientation() == 'N'){
				if (dY != 0)
					if (dY > 0)
						return 2;
					else
						return 5;
				else
					if (dX>0)
						return 1;
					else
						return 3;
          }
			if (posCentre.getOrientation() == 'O'){
				if (dX != 0)
					if (dX > 0)
						return 2;
					else
						return 5;
				else
					if (dY>0)
						return 3;
					else
						return 1;
          }
			if (posCentre.getOrientation() == 'S'){
				if (dY != 0)
					if (dY > 0)
						return 5;
					else
						return 2;
				else
					if (dX>0)
						return 3;
					else
						return 1;
          }
			if (posCentre.getOrientation() == 'E'){
				if (dX != 0)
					if (dX > 0)
						return 5;
					else
						return 2;
				else
					if (dY>0)
						return 1;
					else
						return 3;
          }
      return 0;
			};


			/*!
			*\brief convertit une position et une direction en une chaine de caracteres "MOUV  posX posY" ou "TURN  or"
			*/
	std::string Position::convertitEnMouv(Position pos, int DIR) {
		int x = pos.getX();
		int y = pos.getY();
		char ori = pos.getOrientation();
		std::string nori;
		switch(DIR) {
			case 1 :
					switch(ori){
						case 'N' :
							 	x = x-1;
								break;
						case 'S' :
								x = x+1;
								break;
						case 'O' :
							 	y = y+1;
								break;
						case 'E' :
								y = y-1;
								break;
					}
					return "MOUV " + to_string(x) + " " + to_string(y);
					break;
			case 2 :
					switch(ori) {
						case 'N' :
								y = y-1;
								break;
						case 'S' :
								y = y+1;
								break;
						case 'O' :
								x = x-1;
								break;
						case 'E' :
								x = x+1;
								break;
					}
					return "MOUV " + to_string(x) + " " + to_string(y);
					break;
			case 3 :
					switch(ori) {
						case 'N' :
								x = x+1;
								break;
						case 'S' :
								x = x-1;
								break;
						case 'O' :
								y = y-1;
								break;
						case 'E' :
								y = y+1;
								break;
					}
					return "MOUV " + to_string(x) + " " + to_string(y);
					break;
			case 4 :
					switch(ori) {
						case 'N' :
								nori = "O";
								break;
						case 'S' :
								nori = "E";
								break;
						case 'O' :
								nori = "S";
								break;
						case 'E' :
								nori = "N";
								break;
					}
					return "TURN " + nori;
					break;
			case 5 :
					switch(ori) {
						case 'N' :
								nori = "S";
								break;
						case 'S' :
						 		nori = "N";
								break;
						case 'O' :
								nori = "E";
								break;
						case 'E' :
						 		nori = "O";
								break;
					}
					return "TURN " + nori;
					break;
			case 6 :
					switch(ori) {
						case 'N' :
								nori = "E";
								break;
						case 'S' :
								nori = "O";
								break;
						case 'O' :
								nori = "N";
								break;
						case 'E' :
								nori = "S";
								break;
					}
					return "TURN " + nori;
					break;
		}
    return "ERREUR";
	};

  std::string Position::toString() const {
    	return to_string(x) + " " + to_string(y);
  }

Position& Position::operator=(const Position& other) {
    if (this != &other) {
        this->x = other.getX();
        this->y = other.getY();
        this->orientation = other.getOrientation();
    }
    return *this;
}
