#include "Agent.hpp"


Agent::Agent(int PdV, Position pos, int PdA) :
points_vie(PdV),
position(pos),
points_attaque(PdA) {}

Agent::~Agent() {}

Position Agent::getPosition() const {
  return position;
}

int Agent::getPA() const {
  return points_attaque;
}

int Agent::getPV() const {
  return points_vie;
}

void Agent::seFaireAttaquer(int pa) {
  points_vie -= pa;
}

std::string Agent::getTypeAgent() const {
  return std::string("Agent");
}

void Agent::attaquer(Agent *const a) const {}

void Agent::bouger(Position p, Environnement *const e) {}

std::string Agent::decider(const std::list<Case *> *cases) const {
  return std::string("ERREUR");
}

std::list<Case*> Agent::voirEnvironnement(const Environnement *const env) const {
    return std::list<Case*>();
}

bool Agent::effectuerAction(Environnement *const env) {
  return false;
}

int Agent::getPollen() const {
    return 0;
};

Case *Agent::getOeufPorte() const {
    return nullptr;
};