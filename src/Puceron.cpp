#include "Puceron.hpp"
#include <string>
#include <sstream>
#include <cstdlib>  // ABS
#include <algorithm>  // max_element
#include <typeinfo>
#include "Position.hpp"
#include "Agent.hpp"
#include "Case.hpp"
#include "Environnement.hpp"
#include "OeufDePuceron.hpp"
#include "Fleur.hpp"
#include "Dynamique.hpp"

using namespace std;

using std::string;
using std::stringstream;

Puceron::Puceron(int PdV, Position position, int PdA): Agent(PdV, position, PdA) {}

Puceron::~Puceron() {}

// heritée de agent
void Puceron::attaquer(Agent *const a) const {
    a->seFaireAttaquer(Agent::points_attaque);
}

void Puceron::bouger(Position p, Environnement *const env) {
    env->setAgentAt(position.getX(), position.getY(), NULL);
    position = p;
    env->setAgentAt(p.getX(), p.getY(), this);
}

std::string Puceron::decider(const std::list<Case*> *cases) const {
    std::list<std::string> prio;

    int mouvement[6] = {0}; /*1 MV LT, 2 MV FW, 3 MV RT, 4 TURN LT, 5 TURN AROUND, 6 TURN RT*/
    Position pos;
    int DIR;

    for (auto it = cases->begin(); it != cases->end(); it++) {
        pos = (*it)->getPosition();
            int dX = pos.getX() - position.getX();
            int dY = pos.getY() - position.getY();
            DIR = Position::positionDirectParRapport(position, pos);
            if (DIR) { // valide que si DIR !=0 <=> case adjacente
                mouvement[DIR-1] -= 1000; // cela veut dire qu'il y a un objet dans cette direction, on interdit le mouv
                    if (typeid((*it)) == typeid(Fleur *)) {
                        prio.push_back("EATF "+ pos.toString());
                    }
            }
            else { //si case non adjacente
                DIR = Position::choixMouvNonDirectACote(position, pos); // il serait bien de proposer un autre mouvement si le mouvement est obstrué, mais rajoute beaucoup de if ?
                if (typeid((*it)) == typeid(Fleur *)) {
                            mouvement[DIR-1] += 6 - abs(dX) - abs(dY);
                }
            }

    }
    if (!prio.empty()) {
        return prio.front(); // normalement on choisit un random, un shuffle ? (ici le premier)
    }
    else {
        DIR= std::distance(mouvement, std::max_element(mouvement, mouvement + 5))+1; // cela nous permet de prendre le tableau le plus incrémenté
        return Position::convertitEnMouv(position, DIR); // on retourne le mouvement converti
    }
    return "ERREUR";
}

std::list<Case*> Puceron::voirEnvironnement(const Environnement *const env) const {
    char ori = position.getOrientation();
    int x = position.getX();
    int y = position.getY();
    int i, j;
    std::list<Case*> listeCases;
// ON MET DANS LA LISTE QUE LES CASES OÙ IL Y A QUELQUECHOSE

    // Orientation Est
    if (ori == 'E' and x != env->getLongueur()-1) {
        // On ajoute les cases devant le puceron (à droite)
        for (i=min(x+1, env->getLongueur()-1);i<=min(x+2, env->getLongueur()-1);i++) {
            for (j=max(y-1, 0);j<=min(y+1, env->getHauteur()-1);j++) {
                if ((env->getCaseAt(i, j)->getTypeCase() != std::string("Case")) or (env->getAgentAt(i, j) != NULL)) {
                    listeCases.push_back(env->getCaseAt(i, j));
                }
            }
        }
        // On rajoute les cases latérales
        if (y != env->getHauteur()-1 && ((env->getCaseAt(x, y+1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x, y+1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x, y+1));
        }
        if (y != 0 && ((env->getCaseAt(x, y-1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x, y-1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x, y-1));
        }
    }
    // Orientation Ouest
    if (ori == 'O' and x != 0) {
        // On ajoute les cases devant le puceron (à gauche)
        for (i=max(x-1, 0);i>=max(x-2, 0);i--) {
            for (j=max(y-1, 0);j<=min(y+1, env->getHauteur()-1);j++) {
                if ((env->getCaseAt(i, j)->getTypeCase() != std::string("Case")) or (env->getAgentAt(i, j) != NULL)) {
                    listeCases.push_back(env->getCaseAt(i, j));
                }
            }
        }
        // On rajoute les cases latérales
        if (y != env->getHauteur()-1 && ((env->getCaseAt(x, y+1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x, y+1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x, y+1));
        }
        if (y != 0 && ((env->getCaseAt(x, y-1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x, y-1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x, y-1));
        }
    }
    // Orientation Nord
    if (ori == 'N' and y != 0) {
        // On ajoute les cases devant le puceron (en haut)
        for (i=max(y-1, 0);i>=max(y-2, 0);i--) {
            for (j=max(x-1, 0);j<=min(x+1, env->getLongueur()-1);j++) {
                if ((env->getCaseAt(j, i)->getTypeCase() != std::string("Case")) or (env->getAgentAt(j, i) != NULL)) {
                    listeCases.push_back(env->getCaseAt(j, i));
                }
            }
        }
        // On rajoute les cases latérales
        if (x != env->getLongueur()-1 && ((env->getCaseAt(x+1, y)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x+1, y) != NULL))) {
            listeCases.push_back(env->getCaseAt(x+1, y));
        }
        if (x != 0 && ((env->getCaseAt(x-1, y)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x-1, y) != NULL))) {
            listeCases.push_back(env->getCaseAt(x-1, y));
        }
    }
    // Orientation Sud
    if (ori == 'S' and y != env->getHauteur()-1) {
        // On ajoute les cases devant le puceron (en bas)
        for (i=min(y+1, env->getHauteur()-1);i<=min(y+2, env->getHauteur()-1);i++) {
            for (j=max(x-1, 0);j<=min(x+1, env->getLongueur()-1);j++) {
                if ((env->getCaseAt(j, i)->getTypeCase() != std::string("Case")) or (env->getAgentAt(j, i) != NULL)) {
                    listeCases.push_back(env->getCaseAt(j, i));
                }
            }
        }
        // On rajoute les cases latérales
        if (x != env->getLongueur()-1 && ((env->getCaseAt(x+1, y)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x+1, y) != NULL))) {
            listeCases.push_back(env->getCaseAt(x+1, y));
        }
        if (x != 0 && ((env->getCaseAt(x-1, y)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x-1, y) != NULL))) {
            listeCases.push_back(env->getCaseAt(x-1, y));
        }
    }

    return listeCases;
}

bool Puceron::effectuerAction(Environnement *const env) {
    #ifdef DEBUG
        std::cerr <<  "-- POSITION : " << position.toString() << " " << position.getOrientation() << std::endl;
    #endif
    list<Case*> vue = voirEnvironnement(env);
    #ifdef DEBUG
        //std::cerr <<  "-- VOIT : " << vue->size() << std::endl;
    #endif
    int x, y;
    string choix = decider(&vue);
    #ifdef DEBUG
        std::cerr <<  "-- " << choix << std::endl;
    #endif
    stringstream  stream(choix);
    char action[4];
    if (choix != string("ERREUR")) {
        switch (choix[0]) {
            case 'M':
                stream >> action;
                stream >> x;
                stream >> y;
                if (x>=0 && y>=0 && x<env->getLongueur() && y <env->getHauteur()){ // on verifie ici que l'abeille ne veut pas sortir de l'env
                  env->setAgentAt(position.getX(), position.getY(), NULL);
                  env->setAgentAt(x, y, this);
                  position.setX(x);
                  position.setY(y);
                }
                else {
                  if (x<0 || x>= env->getLongueur()){ // si l'abeille cherche a sortir de l'environnement on l'oriente dans  un sens normale.
                    position.setOrientation('O');
                  } else {
                    position.setOrientation('S');
                  }
                }
                break;
            case 'T':
                position.setOrientation(choix[5]);
                break;
            case 'E':
            stream >> action;
                stream >> x;
                stream >> y;
                if (typeid(*(env->getCaseAt(x, y))) == typeid(Fleur)) {
                    manger(env->getCaseAt(x, y), env);
                }
                break;
            case 'A':
            stream >> action;
                stream >> x;
                stream >> y;
                attaquer(env->getAgentAt(x, y));
                break;
        }
    }
    #ifdef DEBUG
        std::cerr <<  "-- POSITION FINAL: " << position.toString() << " " << position.getOrientation() << std::endl;
    #endif
    #ifdef DEBUG
        std::cerr <<  "-- VIE : " << getPV()  << std::endl;
    #endif
    return (getPV() < 1);
}

//propre à puceron
void Puceron::manger(Case *const fleur, Environnement *const env) {
    fleur->setTempsDEvol(fleur->getTempsDEvol()-1);
}

std::string Puceron::getTypeAgent() const {
  return std::string("Puceron");
}
