#include "Environnement.hpp"
#include <typeinfo>
#include <vector>
#include "Dynamique.hpp"

#ifdef DEBUG
  #include <iostream>
#endif

Environnement::Environnement(int l, int h) :
grille(h) {
    longueur = l;  // correspond au nb de colonnes
    hauteur = h;  // correspond au nbr de lignes

    int i, j;
    for (i = 0; i < h; ++i) {
        grille[i] = std::vector<Case *>(l);
    }

    for (i = 0; i < h; ++i) {
        for (j = 0; j < l; ++j) {
            grille[i][j] = new Case(j, i);
        }
    }
}

Environnement::~Environnement() {
  for (int i = 0; i < hauteur; ++i) {
        for (int j = 0; j < longueur; ++j) {
            delete grille[i][j];
        }
    }
}

Environnement::Environnement(int l, int h, vector<vector<Case*> > tab) {
  longueur = l;
  hauteur = h;
  grille = tab;
}

int Environnement::getHauteur() const {
  return Environnement::hauteur;
}

int Environnement::getLongueur() const {
  return Environnement::longueur;
}

vector<vector<Case*> > Environnement::getGrille() const {
  return Environnement::grille;
}

Case* Environnement::getCaseAt(int x, int y) const {
  if (x < longueur && y < hauteur && y >= 0 && x >= 0) {
      return grille[y][x];
  }
  return NULL;
}

void Environnement::setCaseAt(int x, int y, Case* cel) {
  if (x < longueur && y < hauteur && y >= 0 && x >= 0) {
    delete grille[y][x];
    grille[y][x] = cel;
  }
}

void Environnement::setAgentAt(int x, int y, Agent* a) {
  #ifdef DEBUG_
    std::cerr << x << ":" << y << std::endl;
  #endif
  if (x < longueur && y < hauteur && y >= 0 && x >= 0) {
    grille[y][x]->setAgent(a);
  }
}

Agent *Environnement::getAgentAt(int x, int y) const {
  if (x < longueur && y < hauteur && y >= 0 && x >= 0) {
    return grille[y][x]->getAgent();
  }
  return NULL;
}
//Appelle la fonction miseajour sur toutes les cases dynamiques de la simulation
//Retourne la listes des nouveaux agents à inclure dans la simulation (éclosion d'un oeuf etc ... )
std::vector<Agent*> Environnement::miseAJour() {
	Agent* res;
	std::vector<Agent *> agents;
	for (int i=0; i< longueur; i++) {
		for(int j=0; j< hauteur; j++) {
			if (typeid(grille[j][i])== typeid(Dynamique *)) {
				res=((Dynamique *) grille[j][i])->miseAJour(this);
				// Si res != NULL on ajoute l'agent dans les listes d'agents de la simulation
				if (res!= NULL) {
					agents.push_back(res);
				}
			}
		}
	}
	 return agents;
}
