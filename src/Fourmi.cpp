#include "Fourmi.hpp"
#include <string>
#include <typeinfo>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <cstdlib>  // ABS
#include <algorithm>  // max_element
#include "Position.hpp"
#include "Agent.hpp"
#include "Case.hpp"
#include "Environnement.hpp"
#include "OeufDePuceron.hpp"
#include "Fourmiliere.hpp"

using namespace std;

Fourmi::Fourmi(int PdV, Position position, int PdA, OeufDePuceron *oeuf) :
Agent(PdV, position, PdA) {
  oeufPorte = oeuf;
}

Fourmi::~Fourmi() {}

// heritée de agent
void Fourmi::attaquer(Agent *const a) const {
    a->seFaireAttaquer(points_attaque);
}

void Fourmi::bouger(Position p, Environnement *const env) {
    env->setAgentAt(position.getX(), position.getY(), NULL);
    position = p;
    env->setAgentAt(p.getX(), p.getY(), this);
}


std::string Fourmi::decider(const std::list<Case *> *cases) const {
    std::list<std::string> prio;
    int mouvement[6] = {0}; /*1 MV LT, 2 MV FW, 3 MV RT, 4 TURN LT, 5 TURN AROUND, 6 TURN RT*/
    Position pos;
    int DIR;

    for (auto it = cases->begin(); it != cases->end(); it++) {
        pos = (*it)->getPosition();
        int dX = pos.getX() - position.getX();
        int dY = pos.getY() - position.getY();
        DIR = Position::positionDirectParRapport(position, pos);
        if (DIR) {  // valide que si DIR !=0 <=> case adjacente
            // cela veut dire qu'il y a un objet dans cette direction, on interdit le mouv
            mouvement[DIR-1] -= 1000;
            if (oeufPorte == NULL) {
                // si l'on ne porte pas d'oeuf, on peut en voler un
                if (typeid((*it)) == typeid(OeufDePuceron*)) {
                    prio.push_back("SEGG " + pos.toString());
                }
            }
        } else {  // si case non adjacente
                   DIR = Position::choixMouvNonDirectACote(position, pos); // il serait bien de proposer un autre mouvement si le mouvement est obstrué, mais rajoute beaucoup de if ?
            if (oeufPorte == NULL) {
                if (typeid((*it)) == typeid(OeufDePuceron*)) {
                    // on rajoute un point qui depend de la distance
                    mouvement[DIR-1] += 16 - abs(dX) - abs(dY);
                }
            } else {
                    // si on porte un oeuf, on veut le ramener à la fourmilière
                    if (typeid((*it)) == typeid(Fourmiliere*)) {
                      if (((dX == 2)&&(dY == 0)) || ((dX == 0)&&(dY == 2)))   { //si on est à 2 cases de la fourmilière, on peut poser l'oeuf
                          Position posOeuf; //position où on à l'intention de poser l'oeuf
                          switch(position.getOrientation()) {
                            case 'N' : {
                            posOeuf.setX(position.getX());
                            posOeuf.setY(position.getY()-1);}
                            case 'S' : {
                            posOeuf.setX(position.getX());
                            posOeuf.setY(position.getY()+1);}
                            case 'O' : {
                            posOeuf.setX(position.getX()-1);
                            posOeuf.setY(position.getY());}
                            case 'E' : {
                            posOeuf.setX(position.getX()+1);
                            posOeuf.setY(position.getY());}
                          }
                          prio.push_back("LEGG " + posOeuf.toString());
                          } else {
                        mouvement[DIR-1] += 16 - abs(dX) - abs(dY);
                      }
                      }
                    }
                }
            }
        if (!prio.empty()) {
            // normalement on choisit un random, un shuffle ? (ici le premier)
            return prio.front();
        } else {
            // cela nous permet de prendre le tableau le plus incrémenter
            DIR = std::distance(mouvement, std::max_element(mouvement, mouvement + 5)) + 1;
            // on retourne le mouvement converti
            return Position::convertitEnMouv(position, DIR);
        }
    return "ERREUR";
}

std::list<Case*> Fourmi::voirEnvironnement(const Environnement *const env) const {
    char ori = position.getOrientation();
    int x = position.getX();
    int y = position.getY();
    int i, j;
    int k = 0;
    std::list<Case*> listeCases;
// ON MET DANS LA LISTE QUE LES CASES OÙ IL Y A QUELQUECHOSE

	// Orientation Est
    if (ori == 'E' and x != env->getLongueur()-1) {
		// On ajoute les cases à doite (en entonnoir)
        for (i = min(x + 1, env->getLongueur()-1); i <= min(x + 5, env->getLongueur()-1); i++) {
            for (j = max(y - k, 0); j <= min(y + k, env->getHauteur()-1); j++) {
                if ((env->getCaseAt(i, j)->getTypeCase() != std::string("Case")) or (env->getAgentAt(i, j) != NULL)) {
                    listeCases.push_back(env->getCaseAt(i, j));
                }
            }
            k = k + 1;
        }
        // On rajoute les cases en diagonale devant
        if (x != 0 && y != env->getHauteur()-1 && ((env->getCaseAt(x-1, y+1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x-1, y+1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x-1, y+1));
        }
        if (x != 0 && y != 0 && ((env->getCaseAt(x-1, y-1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x-1, y-1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x-1, y-1));
        }
        // On rajoute les cases latérales
        if (y != env->getHauteur()-1 && ((env->getCaseAt(x, y+1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x, y+1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x, y+1));
        }
        if (y != 0 && ((env->getCaseAt(x, y-1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x, y-1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x, y-1));
        }
    }
    // Orientation Ouest
    if (ori == 'O' and x != 0) {
        // On ajoute les cases à gauche (en entonnoir)
        for (i = max(x - 1, 0); i >= max(x - 5, 0); i--) {
            for (j = max(y - k, 0); j <= min(y + k, env->getHauteur()-1); j++) {
                if ((env->getCaseAt(i, j)->getTypeCase() != std::string("Case")) or (env->getAgentAt(i, j) != NULL)) {
                    listeCases.push_back(env->getCaseAt(i, j));
                }
            }
            k = k + 1;
        }
        // On rajoute les cases en diagonale devant
        if (x != env->getLongueur()-1 && y != env->getHauteur()-1 && ((env->getCaseAt(x+1, y+1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x+1, y+1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x+1, y+1));
        }
        if (x != env->getLongueur()-1 && y != 0 && ((env->getCaseAt(x+1, y-1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x+1, y-1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x+1, y-1));
        }
        // On rajoute les cases latérales
        if (y != env->getHauteur()-1 && ((env->getCaseAt(x, y+1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x, y+1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x, y+1));
        }
        if (y != 0 && ((env->getCaseAt(x, y-1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x, y-1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x, y-1));
        }
    }
    // Orientation Nord
    if (ori == 'N' and y != 0) {
        // On rajoute les cases au dessus (en entonnoir)
        for (i = max(y - 1, 0); i >= max(y - 5, 0); i--) {
            for (j = max(x - k, 0); j <= min(x + k, env->getLongueur()-1); j++) {
                if ((env->getCaseAt(j, i)->getTypeCase() != std::string("Case")) or (env->getAgentAt(j, i) != NULL)) {
                    listeCases.push_back(env->getCaseAt(j, i));
                }
            }
            k = k + 1;
        }
        // On rajoute les cases en diagonale devant
        if (x != 0 && y != 0 && ((env->getCaseAt(x-1, y-1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x-1, y-1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x-1, y-1));
        }
        if (x != env->getLongueur()-1 && y != 0 && ((env->getCaseAt(x+1, y-1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x+1, y-1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x+1, y-1));
        }
        // On rajoute les cases latérales
        if (x != env->getLongueur()-1 && ((env->getCaseAt(x+1, y)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x+1, y) != NULL))) {
            listeCases.push_back(env->getCaseAt(x+1, y));
        }
        if (x != 0 && ((env->getCaseAt(x-1, y)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x-1, y) != NULL))) {
            listeCases.push_back(env->getCaseAt(x-1, y));
        }
    }
    // Orientation Sud
    if (ori == 'S' and y != env->getHauteur()-1) {
		// On ajoute les cases en bas (en entonnoir)
        for (i = min(y+1, env->getHauteur()-1); i <= min(y + 5, env->getHauteur()-1); i++) {
            for (j = max(x - k, 0); j <= min(x + k, env->getLongueur()-1); j++) {
                if ((env->getCaseAt(j, i)->getTypeCase() != std::string("Case")) or (env->getAgentAt(j, i) != NULL)) {
                    listeCases.push_back(env->getCaseAt(j, i));
                }
            }
            k = k + 1;
        }
        // On rajoute les cases en diagonale devant
        if (x != 0 && y != env->getHauteur()-1 && ((env->getCaseAt(x-1, y+1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x-1, y+1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x-1, y+1));
        }
        if (x != env->getLongueur()-1 && y != env->getHauteur()-1 && ((env->getCaseAt(x+1, y+1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x+1, y+1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x+1, y+1));
        }
        // On rajoute les cases latérales
        if (x != env->getLongueur()-1 && ((env->getCaseAt(x+1, y)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x+1, y) != NULL))) {
            listeCases.push_back(env->getCaseAt(x+1, y));
        }
        if (x != 0 && ((env->getCaseAt(x-1, y)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x-1, y) != NULL))) {
            listeCases.push_back(env->getCaseAt(x-1, y));
        }
    }
    return listeCases;
}

bool Fourmi::effectuerAction(Environnement *const env) {
  #ifdef DEBUG
      std::cerr <<  "-- POSITION : " << position.toString() << " " << position.getOrientation() << std::endl;
  #endif
  const list<Case*> vue = voirEnvironnement(env);
  #ifdef DEBUG
      //std::cerr <<  "-- VOIT : " << vue->size() << std::endl;
  #endif
  int x, y;
  std::string choix = decider(&vue);
    #ifdef DEBUG
        std::cerr <<  "-- " << choix << std::endl;
    #endif
    char action[4];
  std::stringstream  stream(choix);
  if (choix != string("ERREUR")) {
      switch (choix[0]) {
          case 'M':
            stream >> action;
              stream >> x;
              stream >> y;
             if (x>=0 && y>=0 && x<env->getLongueur() && y <env->getHauteur()){ // on verifie ici que l'abeille ne veut pas sortir de l'env
                  env->setAgentAt(position.getX(), position.getY(), NULL);
                  env->setAgentAt(x, y, this);
                  position.setX(x);
                  position.setY(y);
                }
                else {
                  if (x<0 || x>= env->getLongueur()){ // si l'abeille cherche a sortir de l'environnement on l'oriente dans  un sens normale.
                    position.setOrientation('O');
                  } else {
                    position.setOrientation('S');
                  }
                }
              break;
          case 'T':
              position.setOrientation(choix[5]);
              break;
          case 'S':
            stream >> action;
              stream >> x;
              stream >> y;
              if (typeid(*(env->getCaseAt(x, y))) == typeid(OeufDePuceron))
                volerUnOeuf(env->getCaseAt(x, y), env);
              break;
          case 'L':
            stream >> action;
              stream >> x;
              stream >> y;
              poserOeuf(Position(x, y), env);
              break;
          case 'A':
            stream >> action;
              stream >> x;
              stream >> y;
              attaquer(env->getAgentAt(x, y));
              break;
      }
  }
  #ifdef DEBUG
      std::cerr <<  "-- POSITION FINAL: " << position.toString() << " " << position.getOrientation() << std::endl;
  #endif
  #ifdef DEBUG
      std::cerr <<  "-- VIE : " << getPV()  << std::endl;
  #endif
  return (getPV() < 1);
}

// propre à fourmi
void Fourmi::volerUnOeuf(Case *oeuf, Environnement *env) {
    oeufPorte = oeuf;
    oeuf->setPorte(true);
    // Il faut supprimer l'oeuf de la case où il se trouvait :
    Case *c = new Case(oeuf->getPosition().getX(), oeuf->getPosition().getY());
    env->setCaseAt(oeuf->getPosition().getX(), oeuf->getPosition().getY(), c);
}

void Fourmi::poserOeuf(Position pos, Environnement *env) {
    env->setCaseAt(pos.getX(), pos.getY(), oeufPorte);
    oeufPorte->setPorte(false);
    // On vérifie qu'il y a bien un fourmilière
    if ((typeid(env->getCaseAt(pos.getX()+1, pos.getY())) == typeid(Fourmiliere)) ||
        (typeid(env->getCaseAt(pos.getX()-1, pos.getY())) == typeid(Fourmiliere)) ||
        (typeid(env->getCaseAt(pos.getX(), pos.getY()+1)) == typeid(Fourmiliere)) ||
        (typeid(env->getCaseAt(pos.getX(), pos.getY()-1)) == typeid(Fourmiliere))) {
    oeufPorte->setAdjacent(true);
  } else {oeufPorte->setAdjacent(false);}
    oeufPorte = NULL;
}

Case *Fourmi::getOeufPorte() const {
  return oeufPorte;
}

std::string Fourmi::getTypeAgent() const {
  return std::string("Fourmi");
}
