#include "Case.hpp"

Case::Case() {
    agent = NULL;
}

Case::~Case() {}

Case::Case(Position pos) : position(pos) {
    agent = NULL;
}

Case::Case(int x, int y) : position(x, y) {
    agent = NULL;
}

Position Case::getPosition() {
    return Case::position;
}

void Case::setPosition(Position pos) {
    Case::position = pos;
}

void Case::setAgent(Agent* a) {
    agent = a;
}

Agent *Case::getAgent() {
    return agent;
}

std::string Case::getTypeCase() const {
    return std::string("Case");
}

void Case::construire(int tempsConstruit) {}

void Case::perdrePollen() {}

bool Case::aPollen() {
    return false;
}

bool Case::getEnEvolution() const { return false;}

void Case::setPorte(bool porte) {}

void Case::setTempsDEvol(int tempsE) {}

int Case::getTempsDEvol() const {
    return 0;
}

void Case::setAdjacent(bool adjacent) {}

int Case::getNbVoisins() const {
    return 0;
}

int Case::getTempsCons() const {
    return 0;
}


int Case::getnbPollen() const {
    return 5;
}

bool Case::getAdjacent() const {
    return false;
}

bool Case::getPorte() const {
    return false;
}
