#include "Ruche.hpp"

Ruche::Ruche() {}

Ruche::Ruche(int x, int y) : Statique(x, y) {}

Ruche::~Ruche() {}

std::string Ruche::getTypeCase() const {
  return std::string("Ruche");
}
