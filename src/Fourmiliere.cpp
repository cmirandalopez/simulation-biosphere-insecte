#include "Fourmiliere.hpp"

Fourmiliere::Fourmiliere() {}

Fourmiliere::~Fourmiliere() {}

Fourmiliere::Fourmiliere(int x, int y) : Statique(x, y) {}

std::string Fourmiliere::getTypeCase() const {
  return std::string("Fourmiliere");
}
