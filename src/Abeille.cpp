#include "Abeille.hpp"
#include <stdlib.h>  // ABS
#include <algorithm>  // max_element
#include <typeinfo>
#include <string>
#include <sstream>
#include "Position.hpp"
#include "Agent.hpp"
#include "Case.hpp"
#include "Environnement.hpp"
#include "Fleur.hpp"
#include "Alveole.hpp"
#include "Ruche.hpp"

#ifdef DEBUG
    #include <iostream>
#endif

using std::max;
using std::min;
using std::list;
using std::string;
using std::max_element;
using std::abs;
using std::distance;
using std::stringstream;

Abeille::Abeille(int PdV, Position position, int PdA, int pol) :
Agent(PdV, position, PdA) {
    pollen = pol;
}



Abeille::~Abeille() {}

// heritée de agent
void Abeille::attaquer(Agent *const a) const {
    a->seFaireAttaquer(points_attaque);
}

void Abeille::bouger(Position p, Environnement *const e) {
    e->setAgentAt(position.getX(), position.getY(), NULL);
    e->setAgentAt(p.getX(), p.getY(), this);
    position = p;
}


int randint(const int min, const int max) {
    return min + (rand() % static_cast<int>(max - min + 1));
}


string Abeille::decider(const list<Case *> *cases) const {
    list<string> prio;
    int mouvement[6] = {0}; /*1 MV LT, 2 MV FW, 3 MV RT, 4 TURN LT, 5 TURN AROUND, 6 TURN RT*/
    mouvement[0] = randint(1, 6); mouvement[2] = randint(1, 6); mouvement[3] = randint(1, 6); mouvement[4] = randint(1, 6); mouvement[5] = randint(1, 6);
    Position pos;
    int DIR;
    Position posPourAlveole;
    for (auto it = cases->begin(); it != cases->end(); it++) {
        pos = (*it)->getPosition();
        int dX = std::abs(pos.getX() - position.getX());
        int dY = std::abs(pos.getY() - position.getY());
        DIR = Position::positionDirectParRapport(position, pos);
        if (DIR) {  // valide que si DIR !=0 <=> case adjacente
            mouvement[DIR - 1] -= 1000;  // cela veut dire qu'il y a un objet dans cette direction, on interdit le mouv
            if (pollen > 0) {  // si l'on a du pollen, on se preoccupe des alveoles
                if ((*it)->getTypeCase() == "Alveole") {
                    prio.push_back("CONS " + pos.toString());
                }
            } // autrement que des fleurs
            if ((*it)->getTypeCase() == "Fleur") {
                prio.push_back("BUTI " + pos.toString());
            }
        } else {  // si case non adjacente
            // il serait bien de proposer un autre mouvement si le mouvement est obstrué, mais rajoute beaucoup de if ?
            DIR = Position::choixMouvNonDirectACote(position, pos);
            if (pollen > 0) {
                if ((*it)->getTypeCase() == string("Alveole")) {
                    // on rajoute un point qui depend de la distance
                    mouvement[DIR - 1] += 16 -dX - dY;
                }
                if ((*it)->getTypeCase() == string("Fleur")) {
                    mouvement[DIR - 1] += 16 - dX - dY;
                    #ifdef DEBUG
                        std::cerr <<  "-- ABEILLE A VU FLEUR : " << pos.getX() << " " << pos.getX() << std::endl;
                    #endif
                }
                if ((*it)->getTypeCase() == string("Ruche")) {
                  if (((dX == 2) && (dY == 0)) || ((dY == 2) && (dX == 0))) {
                    if (dY == 0) {
                      if (pos.getX() - position.getX() > 0) {
                        posPourAlveole.setX(position.getX()+1);
                        posPourAlveole.setY(position.getY());
                      } else {
                        posPourAlveole.setX(position.getX()-1);
                        posPourAlveole.setY(position.getY());
                      }
                    } else {
                      if (pos.getY() - position.getY() > 0) {
                        posPourAlveole.setX(position.getX());
                        posPourAlveole.setY(position.getY()+1);
                      } else {
                        posPourAlveole.setX(position.getX());
                        posPourAlveole.setY(position.getY()-1);
                      }
                    }
                    prio.push_back("CONS " + posPourAlveole.toString());
                  } else {
                    mouvement[DIR - 1] += 2 * (16 - dX - dY);  // un peu plus prioritaire
                  }
                }
            } else {
                if ((*it)->getTypeCase() == string("Fleur")) {
                    mouvement[DIR - 1] += 4*(16 - dX - dY);
                    #ifdef DEBUG
                        std::cerr <<  "-- ABEILLE A VU FLEUR : " << pos.getX() << " " << pos.getX() << std::endl;
                    #endif
                }
            }
        }
    }
    if (!prio.empty()) {
        // normalement on choisit un random, un shuffle ? (ici le premier)
        return prio.front();
        #ifdef DEBUG
            std::cerr <<  "-- PRIO : " << prio.front() << std::endl;
        #endif
    } else {
        // cela nous permet de prendre le tableau le plus incrémenter
        DIR = distance(mouvement, max_element(mouvement, mouvement + 5)) + 1;
        // on retourne le mouvement converti
        return Position::convertitEnMouv(position, DIR);
    }
    // a noter que si elle ne peut pas bouger, alors l'abeille va necessairement tourner, cet algo est pacifique.
    return "ERREUR";
}

list<Case *> Abeille::voirEnvironnement(const Environnement *const env) const {
    char ori = position.getOrientation();
    int x = position.getX();
    int y = position.getY();
    int i, j;
    list<Case *> listeCases;
// ON MET DANS LA LISTE QUE LES CASES OÙ IL Y A QUELQUECHOSE

    // Orientation Nord ou Sud
    if (ori == 'N' || ori == 'S') {
      if (x != env->getLongueur()-1){
        // On ajoute les cases à droite (en entonnoir)
        for (i = min(x + 1, env->getLongueur() - 1); i <= min(x + 4, env->getLongueur() - 1); ++i) {
            for (j = max(y - (i - x), 0); j <= min(y + (i - x), env->getHauteur() - 1); ++j) {
                if ((env->getCaseAt(i, j)->getTypeCase() != std::string("Case")) or (env->getAgentAt(i, j) != NULL))  {
                    listeCases.push_back(env->getCaseAt(i, j));
                }
            }
        }
      }

      if (x != 0){
        // On ajoute les cases à gauche (en entonnoir)
        for (i = max(x - 1, 0); i >= max(x - 4, 0); --i) {
            for (j = max(y - (x - i), 0); j <= min(y + (x - i), env->getHauteur() - 1); ++j) {
                if ((env->getCaseAt(i, j)->getTypeCase() != std::string("Case")) or (env->getAgentAt(i, j) != NULL))  {
                    listeCases.push_back(env->getCaseAt(i, j));
                }
            }
        }
      }
      // On rajoute les cases devant l'abeille (au dessus si Nord, en dessous si Sud)
        if (ori == 'N' && y > 0 && ((env->getCaseAt(x, y-1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x, y-1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x, y - 1));
        } else if (ori == 'S' && y < env->getHauteur() - 1 && ((env->getCaseAt(x, y+1)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x, y+1) != NULL))) {
            listeCases.push_back(env->getCaseAt(x, y + 1));
        }

    }

    // Orientation Est ou Ouest
    if (ori == 'E' || ori == 'O') {
      // On ajoute les cases en bas (en entonnoir)
      if (y != env->getHauteur()-1){
        for (j = min(y + 1, env->getHauteur() - 1); j <= min(y + 4, env->getHauteur() - 1); ++j) {
            for (i = max(x - (j - y), 0); i <= min(x + (j - y), env->getLongueur() - 1); ++i) {
                if ((env->getCaseAt(i, j)->getTypeCase() != std::string("Case")) or (env->getAgentAt(i, j) != NULL)) {
                    listeCases.push_back(env->getCaseAt(i, j));
                }
            }
        }
      }
      // On ajoute les cases en haut (en entonnoir)
      if (y != 0){
        for (j = max(y - 1, 0); j >= max(y - 4, 0); --j) {
            for (i = max(x - (y - j), 0); i <= min(x + (y - j), env->getLongueur()-1); ++i) {
                if ((env->getCaseAt(i, j)->getTypeCase() != std::string("Case")) or (env->getAgentAt(i, j) != NULL)) {
                    listeCases.push_back(env->getCaseAt(i, j));
                }
            }
        }
      }
        // On rajoute les cases devant l'abeille (à droite si Est, à gauche si Ouest)
        if (ori == 'E' && x < env->getLongueur() - 1 && ((env->getCaseAt(x+1, y)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x+1, y) != NULL))) {
            listeCases.push_back(env->getCaseAt(x + 1, y));
        } else if (ori == 'O' && x > 0 && ((env->getCaseAt(x-1, y)->getTypeCase() != std::string("Case")) or (env->getAgentAt(x-1, y) != NULL))) {
            listeCases.push_back(env->getCaseAt(x - 1, y));
        }
    }
    return listeCases;
}

bool Abeille::effectuerAction(Environnement *const env) {
    #ifdef DEBUG
        std::cerr <<  "-- POSITION : " << position.toString() << " " << position.getOrientation() << std::endl;
    #endif
    list<Case*> vue = voirEnvironnement(env);
    #ifdef DEBUG
        // std::cerr <<  "-- VOIT : " << vue->size() << std::endl;
    #endif
    int x, y;
    string choix = decider(&vue);
    #ifdef DEBUG
        std::cerr <<  "-- " << choix << std::endl;
    #endif
    char action[4];
    stringstream stream(choix);
    if (choix != string("ERREUR")) {
        switch (choix[0]) {
            case 'M':
                stream >> action;
                stream >> x;
                stream >> y;
                if (x>=0 && y>=0 && x<env->getLongueur() && y <env->getHauteur()){ // on verifie ici que l'abeille ne veut pas sortir de l'env
                  env->setAgentAt(position.getX(), position.getY(), NULL);
                  env->setAgentAt(x, y, this);
                  position.setX(x);
                  position.setY(y);
                }
                else {
                  if (x<0 || x >= env->getLongueur()){ // si l'abeille cherche a sortir de l'environnement on l'oriente dans  un sens normale.
                    position.setOrientation('O');
                  } else {
                    position.setOrientation('S');
                  }
                }
                break;
            case 'T':
                position.setOrientation(choix[5]);
                break;
            case 'C':
                stream >> action;
                stream >> x;
                stream >> y;
                if (typeid(*(env->getCaseAt(x, y))) == typeid(Alveole)) {
                  construireAlveole(env->getCaseAt(x, y));
                } else {
                  creerAlveole(Position(x, y), env);
                }
                break;
            case 'B':
                stream >> action;
                stream >> x;
                stream >> y;
                if (env->getCaseAt(x, y)->getTypeCase() == string("Fleur"))
                  butiner(env->getCaseAt(x, y));
                break;
            case 'A':
                stream >> action;
                stream >> x;
                stream >> y;
                attaquer(env->getAgentAt(x, y));
                break;
        }
    }
    #ifdef DEBUG
        std::cerr <<  "-- POSITION FINAL: " << position.toString() << " " << position.getOrientation() << std::endl;
    #endif
    #ifdef DEBUG
        std::cerr <<  "-- VIE : " << getPV() << std::endl;
    #endif
    
    return (getPV() < 1);
}

// special à abeille
int Abeille::butiner(Case *const fleur) {
    if (fleur->aPollen()) {
        fleur->perdrePollen();
        pollen += 1;
    }
    return 0;
}

int Abeille::getPollen() const {
    return pollen;
}


void Abeille::creerAlveole(Position pos, Environnement *e) {
    if (getPollen() > 0) {
        int nbV = 0;
        // Trouver une alveole voisine pour setter le nb de voisins
        for (int i = max(0, pos.getX() - 1); i <= min(e->getLongueur(), pos.getX() + 1); i++) {
            for (int j = max(0, pos.getY() - 1); j <= min(e->getHauteur(), pos.getY() + 1); j++) {
                if (typeid(*(e->getCaseAt(i, j))) == typeid(Alveole)) {
                    // Dynamique d = dynamic_cast<Dynamique*>(e.getCas
                    // Alveole al = dynamic_cast<Alveole*>(e.getCaseAt(i, j));
                    // Alveole *al=&(e.getCaseAt(i, j)); // Autre type de cast, peut-être ça marche, peut-être ça marche pas
                    nbV = (static_cast<Alveole *>(e->getCaseAt(i, j)))->getNbVoisins() + 1;
                    break;  // très sale tout ça
                }
            }
        }
        int tps = 10;  // Temps de construction, arbitraire
        int tpsE = 5;  // Temps d'evolution, arbitraire
        delete e->getCaseAt(pos.getX(), pos.getY());
        Alveole *a = new Alveole(nbV, tps, false, pos, tpsE);
        e->setCaseAt(pos.getX(), pos.getY(), a);
        pollen -= 1;
    }
}

void Abeille::construireAlveole(Case *alveole) {
    if ((getPollen() > 0) && (!alveole->getEnEvolution())) {
        alveole->construire(1);
        pollen -= 1;
    }
}

std::string Abeille::getTypeAgent() const {
  return std::string("Abeille");
}
