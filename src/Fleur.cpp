#include "Fleur.hpp"
#include <string>
#include "Case.hpp"
#include "Position.hpp"
#include "Dynamique.hpp"
#include "Environnement.hpp"
#include "OeufDePuceron.hpp"

#ifdef DEBUG
	#include <iostream>
#endif

int Fleur::tempsMiseAJour=5;

Fleur::Fleur(int nbPollen, Position pos, int tempsDEvol) : Dynamique(tempsDEvol, pos) {
    pollen = nbPollen;
}

Fleur::~Fleur() {}

//Fonctions héritées de Dynamique
Agent* Fleur::miseAJour(Environnement *e)
{
	if (getTempsDEvol() == 0)
	{
		//La case Fleur devient une case OeufDePuceron
		OeufDePuceron *o = new OeufDePuceron(false, false, 10, this->getPosition());
   		e->setCaseAt(getPosition().getX(), getPosition().getY(), o);
	} 
	else {
		setTempsDEvol(getTempsDEvol()-1);
	}
	return NULL;
}

//fonctions de Fleur

int Fleur::getnbPollen() const {
    return pollen;
}

void Fleur::perdrePollen() {
    if (pollen > 0) {
        --pollen;
    }
}

bool Fleur::aPollen()
{
	 return pollen>0;
}

std::string Fleur::getTypeCase() const {
  return std::string("Fleur");
}
