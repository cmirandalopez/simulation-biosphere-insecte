#include "Dynamique.hpp"
#include <string>

#include "Case.hpp"
#include "Position.hpp"
#include "Environnement.hpp"

#ifdef DEBUG
	#include <iostream>
#endif

//Constructeur


Dynamique::Dynamique(int temps, Position pos) : Case(pos) {
	tempsDEvolution=temps;
}

Dynamique::~Dynamique() {}


void Dynamique::setTempsDEvol(int tempsE)
{
	if (tempsE<0) {
		tempsDEvolution=0;
	} else {
		tempsDEvolution=tempsE;
}
}

int Dynamique::getTempsDEvol() const {
	return this->tempsDEvolution;
}

std::string Dynamique::getTypeCase() const {
  return  std::string("Dynamique");
}
