#include "Simulation.hpp"
#include <cstdlib>
#include <exception>
#include <cmath>
#include <ctime>
#include "Ruche.hpp"
#include "Fourmiliere.hpp"
#include "Fleur.hpp"

#ifdef DEBUG
    #include <iostream>
#endif

#define AR_PROP 3  // Proportion abeilles-ruches
#define FF_PROP 3  // Proportion fourmis-fourmilières
#define PF_PROP 2  // Proportion pucerons-fleurs

/** Renvoie un biais selon l'ordre de grandeur de nombre. */
inline int determiner_biais(const int nombre);

/** Renvoie un entier aléatoire entre min et max */
inline int randint(const int min, const int max);

Simulation::Simulation(
    int hauteur,
    int longueur,
    int nb_abeilles,
    int nb_fourmis,
    int nb_pucerons) :
    environnement(longueur, hauteur),
    abeilles(),
    fourmis(),
    pucerons() {
    nb_iterations = 0;
    nb_abeilles_init = nb_abeilles;
    nb_fourmis_init = nb_fourmis;
    nb_pucerons_init = nb_pucerons;
    int biais;
    unsigned seed(std::time(0));
    std::srand(seed);  // Graine du générateur aléatoire
    #ifdef DEBUG
        std::cerr << "Graine du générateur aléatoire pour cette simulation : ";
        std::cerr << seed << std::endl;
    #endif

    if (hauteur * longueur < nb_abeilles + nb_fourmis + nb_pucerons) {
        throw std::string("Le nombre d'agents est trop important !");
    }

    // On ajoute un biais en fonction du nombre d'agents, pour chaque type de case associée
    biais = determiner_biais(nb_abeilles);
    nb_ruches_init = std::floor(nb_abeilles / AR_PROP) + biais;

    biais = determiner_biais(nb_fourmis);
    nb_fourmilieres_init = std::floor(nb_fourmis / FF_PROP) + biais;

    biais = determiner_biais(nb_pucerons);
    nb_fleurs_init = std::floor(nb_pucerons / PF_PROP) + biais;

    if (hauteur * longueur < nb_abeilles + nb_fourmis + nb_pucerons
        + nb_abeilles_init + nb_fourmilieres_init + nb_fleurs_init) {
        throw std::string("Le nombre d'agents est trop important !");
    }

    // On initialise les différentes cases et les agents
    init_ruches();
    init_fourmilieres();
    init_fleurs();
    init_agents();
}

Simulation::Simulation(const Simulation * const simulation) :
    environnement(0, 0),
    abeilles(),
    fourmis(),
    pucerons() {
    environnement = simulation->environnement;
    abeilles = simulation->abeilles;
    fourmis = simulation->fourmis;
    pucerons = simulation->pucerons;
    nb_ruches_init = simulation->nb_ruches_init;
    nb_fourmilieres_init = simulation->nb_fourmilieres_init;
    nb_fleurs_init = simulation->nb_fleurs_init;
    nb_abeilles_init = simulation->nb_abeilles_init;
    nb_fourmis_init = simulation->nb_fourmis_init;
    nb_pucerons_init = simulation->nb_pucerons_init;
    nb_iterations = simulation->nb_iterations;
}

Simulation::Simulation() :
    environnement(0, 0),
    abeilles(),
    fourmis(),
    pucerons() {
    nb_ruches_init = 0;
    nb_fourmilieres_init = 0;
    nb_fleurs_init = 0;
    nb_abeilles_init = 0;
    nb_fourmis_init = 0;
    nb_pucerons_init = 0;
    nb_iterations = 0;
}

Simulation::~Simulation() {
    for (auto a : abeilles) {
        delete a;
    }
    for (auto f : fourmis) {
        delete f;
    }
    for (auto p : pucerons) {
        delete p;
    }
}

void Simulation::set_iterations(const int nb_iterations) {
    this->nb_iterations = nb_iterations;
}

int Simulation::get_iterations() const {
    return nb_iterations;
}

void Simulation::set_environnement(Environnement * const environnement) {
    this->environnement = *environnement;
}

const Environnement *Simulation::get_environnement() const {
    return &environnement;
}

void Simulation::set_abeilles(std::vector<Abeille *> * const abeilles) {
    if (fourmis.size() + pucerons.size() + abeilles->size() < (unsigned) environnement.getHauteur() * environnement.getLongueur()) {
        this->abeilles = *abeilles;
        nb_abeilles_init = abeilles->size();
    } else {
        throw std::string("Trop d'agents !");
    }
}

std::vector<const Abeille *> Simulation::get_abeilles() const {
    return std::vector<const Abeille *>(abeilles.begin(), abeilles.end());
}

void Simulation::set_fourmis(std::vector<Fourmi *> * const fourmis) {
    if (abeilles.size() + pucerons.size() + fourmis->size() < (unsigned) environnement.getHauteur() * environnement.getLongueur()) {
        this->fourmis = *fourmis;
        nb_fourmis_init = fourmis->size();
    } else {
        throw std::string("Trop d'agents !");
    }
}

std::vector<const Fourmi *> Simulation::get_fourmis() const {
    return std::vector<const Fourmi *>(fourmis.begin(), fourmis.end());
}

void Simulation::set_pucerons(std::vector<Puceron *> * const pucerons) {
    if (abeilles.size() + fourmis.size() + pucerons->size() < (unsigned) environnement.getHauteur() * environnement.getLongueur()) {
        this->pucerons = *pucerons;
        nb_pucerons_init = pucerons->size();
    } else {
        throw std::string("Trop d'agents !");
    }
}

std::vector<const Puceron *> Simulation::get_pucerons() const {
    return std::vector<const Puceron *>(pucerons.begin(), pucerons.end());
}

void Simulation::executer_un() {
    std::vector<Agent *> n_agents;
    std::string s_abeille, s_fourmi, s_puceron, s_agent;

    // Chaque agent effectue une action
    auto abeille = abeilles.begin();
    while (abeille != abeilles.end()) {
        #ifdef DEBUG
            std::cerr << "Abeille effectue action" << std::endl;
        #endif
        if ((*abeille)->effectuerAction(&environnement)) {
            abeille = abeilles.erase(abeille);
        } else {
            ++abeille;
        }
    }

    auto fourmi = fourmis.begin();
    while (fourmi != fourmis.end()) {
        #ifdef DEBUG
            std::cerr << "Fourmi effectue action" << std::endl;
        #endif
        if ((*fourmi)->effectuerAction(&environnement)) {
            fourmi = fourmis.erase(fourmi);
        } else {
            fourmi++;
        }
    }

    auto puceron = pucerons.begin();
    while (puceron != pucerons.end()) {
        #ifdef DEBUG
            std::cerr << "Puceron effectue action" << std::endl;
        #endif
        if ((*puceron)->effectuerAction(&environnement)) {
            puceron = pucerons.erase(puceron);
        } else {
            puceron++;
        }
    }

    // On met à jour l'environnement et on rajoute des agents s'il y en a qui ont été produits.
    n_agents = environnement.miseAJour();

    s_abeille = std::string("Abeille");
    s_fourmi = std::string("Fourmi");
    s_puceron = std::string("Puceron");
    for (auto agent = n_agents.begin(); agent != n_agents.end(); ++agent) {
        s_agent = (*agent)->getTypeAgent();
        if (s_agent == s_abeille) {
            abeilles.push_back(static_cast<Abeille *>(*agent));
        } else if (s_agent == s_fourmi) {
            fourmis.push_back(static_cast<Fourmi *>(*agent));
        } else if (s_agent == s_puceron) {
            pucerons.push_back(static_cast<Puceron *>(*agent));
        }
    }
    nb_iterations++;
}

void Simulation::init_ruches() {
    /**
     * Cette implémentation crée des ruches quasi-carrées
     * à partir de la case (1;1)
     */
    int i_0 = 1, j_0 = 1;
    int n = std::floor(std::sqrt(nb_ruches_init)), r, i, j;
    for (i = i_0; i < n + i_0; i++) {
        for (j = j_0; j < n + j_0; j++) {
            environnement.setCaseAt(j, i, new Ruche(j, i));
        }
    }
    r = nb_ruches_init - n * n;
    for (j = j_0; j < r + j_0; j++) {
        environnement.setCaseAt(j, n + i_0, new Ruche(j, i));
    }
}

void Simulation::init_fourmilieres() {
    /**
     * Cette implémentation crée des fourmilières quasi-carrées
     * à partir de la case (longueur-2;longueur-2)
     */
    int i_0 = 1, j_0 = environnement.getLongueur() - 2;
    int n = std::floor(std::sqrt(nb_fourmilieres_init)), r, i, j;
    for (i = i_0; i < n + i_0; i++) {
        for (j = j_0; j > j_0 - n; j--) {
            environnement.setCaseAt(j, i, new Fourmiliere(j, i));
        }
    }
    r = nb_fourmilieres_init - n * n;
    for (j = j_0; j > j_0 - r; j--) {
        environnement.setCaseAt(j, n + i_0, new Fourmiliere(j, i));
    }
}

void Simulation::init_fleurs() {
    /**
     * Cette implémentation crée des fleurs aléatoirement
     */
    int i, j, nb_pollen, temps_evol, n = 0;
    int hauteur = environnement.getHauteur() - 1;
    int longueur = environnement.getLongueur() - 1;
    Position pos;
    while (n < nb_fleurs_init) {
        i = randint(0, hauteur);
        j = randint(0, longueur);
        nb_pollen = randint(0, 10);
        temps_evol = randint(0, 10);
        if (environnement.getCaseAt(j, i)->getTypeCase() == std::string("Case")) {
            pos.setX(j);
            pos.setY(i);
            environnement.setCaseAt(j, i, new Fleur(
                nb_pollen,
                pos,
                temps_evol));
            n++; // On n'incrémente que si on a posé la fleur
        }
    }
}

void Simulation::init_agents() {
    /**
     * Cette implémentation crée des agents aléatoirement
     */
    int i, j, n = 0;
    int hauteur = environnement.getHauteur() - 1;
    int longueur = environnement.getLongueur() - 1;
    int nb_agents = nb_abeilles_init + nb_fourmis_init + nb_pucerons_init;
    int nb_cases_vides = hauteur * longueur
        - (nb_ruches_init + nb_fourmilieres_init + nb_fleurs_init);
    int pa, pv;
    Position pos(0, 0, 'N');

    // On place tous les agents qui peuvent être placés sur des cases vides
    while (n < nb_agents && n < nb_cases_vides) {
        i = randint(0, hauteur);
        j = randint(0, longueur);
        if (environnement.getCaseAt(j, i)->getTypeCase() == std::string("Case")
            && environnement.getAgentAt(j, i) == NULL) {
            pv = randint(1, 10);
            pa = randint(1, 10);
            pos.setX(j);
            pos.setY(i);
            if (n < nb_abeilles_init) {
                Abeille *a = new Abeille(pv, pos, pa, 0);
                abeilles.push_back(a);
                environnement.setAgentAt(j, i, a);
            } else if (n < nb_fourmis_init + nb_abeilles_init) {
                Fourmi *f = new Fourmi(pv, pos, pa, NULL);
                fourmis.push_back(f);
                environnement.setAgentAt(j, i, f);
            } else {
                Puceron *p = new Puceron(pv, pos, pa);
                pucerons.push_back(p);
                environnement.setAgentAt(j, i, p);
            }
            n++;
        }
    }
    // On place le reste
    while (n < nb_agents) {
        i = randint(0, hauteur);
        j = randint(0, longueur);
        if (environnement.getAgentAt(j, i) == NULL) {
            pv = randint(1, 10);
            pa = randint(1, 10);
            pos.setX(j);
            pos.setY(i);
            if (n < nb_abeilles_init) {
                Abeille *a = new Abeille(pv, pos, pa, 0);
                abeilles.push_back(a);
                environnement.setAgentAt(j, i, a);
            } else if (n < nb_fourmis_init + nb_abeilles_init) {
                Fourmi *f = new Fourmi(pv, pos, pa, NULL);
                fourmis.push_back(f);
                environnement.setAgentAt(j, i, f);
            } else {
                Puceron *p = new Puceron(pv, pos, pa);
                pucerons.push_back(p);
                environnement.setAgentAt(j, i, p);
            }
        }
    }
}

inline int randint(const int min, const int max) {
    return min + (rand() % static_cast<int>(max - min + 1));
}

inline int determiner_biais(const int nombre) {
    int biais;
    if (nombre > 9) {
        biais = 5;
    } else if (nombre > 99) {
        biais = 50;
    } else if (nombre > 999) {
        biais = 500;
    } else {
        return 0;
    }
    return (std::rand() % static_cast<int>(2 * biais + 1)) - biais;
}
