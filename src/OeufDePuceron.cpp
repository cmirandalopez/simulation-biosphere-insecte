#include "OeufDePuceron.hpp"
#include <string>

#include "Case.hpp"
#include "Environnement.hpp"
#include "Position.hpp"
#include "Dynamique.hpp"
#include "Fourmi.hpp"
#include "Puceron.hpp"

using namespace std;

int OeufDePuceron::tempsFormation=1;

//Constructeur

OeufDePuceron::OeufDePuceron(bool porte, bool adjacent, int tempsE, Position unePos) : Dynamique(tempsE, unePos)
{
	porteParFourmi=porte;
	adjacentFourmiliere=adjacent;

}

OeufDePuceron::~OeufDePuceron() {}

//fonctions héritées de Dynamique
Agent* OeufDePuceron::miseAJour(Environnement *e)
{
	//Vérifier si l'oeuf a évolué
	if ((getTempsDEvol() == 0) and (!getPorte())) {
		if (getAdjacent()) {
				//On met une case à la position actuelle et plus un OeufDePuceron
				Case c(getPosition());
				e->setCaseAt(getPosition().getX(), getPosition().getY(), &c);
				// Ajoute une fourmi à l'environnement
				Fourmi *f=new Fourmi(10, getPosition(), 5, this);
				e->setAgentAt(getPosition().getX(), getPosition().getY(), f);
				return f;

		} else {
				//On met une case à la position actuelle et plus un OeufDePuceron
				Case c(getPosition());
				e->setCaseAt(getPosition().getX(), getPosition().getY(), &c);
				// Ajoute un puceron à l'environnement
				Puceron *p = new Puceron(5, getPosition(), 5);
				e->setAgentAt(getPosition().getX(), getPosition().getY(), p);
				return p;
		}
	}
	else if (!getPorte()) {
			setTempsDEvol(getTempsDEvol()-1);
			return NULL;
	}
	return NULL;
}


void OeufDePuceron::nextToFourmiliere(Environnement *e)
{
	Position position=this->getPosition();
	std::list<Case *> *listeCases = new std::list<Case *>;
	// Boucle pour remplir listeCases de toutes les cases qui entourent l'oeuf 
	for (int i=-1;i<2;i++) {
		for(int j=-1;j<2;j++) {
			listeCases->push_back(e->getCaseAt(position.getX()+i, position.getY()+j));
		}
	}
	// Parcours de la listeCases pour vérifier si l'une d'elle est une fourmilière
	for (auto it = listeCases->begin(); it != listeCases->end(); it++) {
		if ((*it)->getTypeCase() == std::string("Fourmiliere")) {
			this->setAdjacent(true);
		}
	}
}

//Accesseurs

bool  OeufDePuceron::getPorte()
{
	return porteParFourmi;
}

void OeufDePuceron::setPorte(bool porte)
{
	this->porteParFourmi=porte;
}

bool  OeufDePuceron::getAdjacent()
{
	return adjacentFourmiliere;
}


void OeufDePuceron::setAdjacent(bool adjacent)
{
	adjacentFourmiliere=adjacent;
}

int  OeufDePuceron::getTempsFormation()
{
	return tempsFormation;
}


void OeufDePuceron::setTempsFormation(int tempsF)
{
	tempsFormation=tempsF;
}


std::string OeufDePuceron::getTypeCase() const {
  return std::string("OeufDePuceron");
}
