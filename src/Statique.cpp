#include "Statique.hpp"

Statique::Statique() {}

Statique::~Statique() {}

Statique::Statique(int x, int y) : Case(x, y) {}

std::string Statique::getTypeCase() const {
  return std::string("Statique");
}
