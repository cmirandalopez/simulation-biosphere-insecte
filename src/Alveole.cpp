#include "Alveole.hpp"
#include <string>
#include "Abeille.hpp"
#include "Case.hpp"
#include "Position.hpp"

#include "Dynamique.hpp"
#include "Environnement.hpp"

//Constructeur
Alveole::Alveole(int nb, int temps, bool evolution, Position pos, int tempsDEvol) : Dynamique(tempsDEvol, pos)
{
	nbVoisins=nb;
	tempsConstruction=temps;
	enEvolution=evolution;
}

Alveole::~Alveole() {}

//fonctions héritées de Dynamique
Agent* Alveole::miseAJour(Environnement *e)
{
	if (getTempsCons() == 0) {
		lancerEvolution();
	}
	if (getTempsDEvol() == 0)
	{
		int PdV=10; //Nombre de points de vie arbitraires
		int PdA=5; //Nombre de points d'attaque arbitraires
		int pol=0; //Nombre de pollens arbitraires
		//On met une case à la position actuelle et plus une Alveole
		Case *c = new Case(position);
		Abeille *a = new Abeille(PdV, getPosition(), PdA, pol);
		c->setAgent(a);
		e->setCaseAt(position.getX(), position.getY(), c);
		return a;
	}
	else if (getEnEvolution()) {
			setTempsDEvol(getTempsDEvol()-1);
			return NULL;	
	}
	return NULL;
}

// Fonctions d'Alveole

int Alveole::getNbVoisins() const
{
	return nbVoisins;
}

void Alveole::setNbVoisins(int nombre)
{
	nbVoisins=nombre;
}

void Alveole::construire(int tempsConstruit)
{
	if (tempsConstruit < tempsConstruction)
	{
		tempsConstruction =tempsConstruction-tempsConstruit;
	} else {
		tempsConstruction = 0;
	}
}

int Alveole::getTempsCons() const
{
	return tempsConstruction;
}

void Alveole::lancerEvolution()
{
	enEvolution=true;
}


bool Alveole::getEnEvolution() const
{
	return this->enEvolution;
}

std::string Alveole::getTypeCase() const {
  return std::string("Alveole");
}
