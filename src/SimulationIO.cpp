#include "SimulationIO.hpp"
#include <iostream>
#include <fstream>
#include "Simulation.hpp"
#include "Ruche.hpp"
#include "Fourmiliere.hpp"
#include "Alveole.hpp"
#include "Fleur.hpp"
#include "OeufDePuceron.hpp"

SimulationIO::SimulationIO() :
    simulation(), fichier_lecture(), fichier_ecriture() {
    frequence_ecriture = 0;
    ecriture_graphique = false;
    nb_iterations_arret = 0;
}

SimulationIO::~SimulationIO() {}

SimulationIO::SimulationIO(const Simulation * const simulation) :
    simulation(simulation), fichier_lecture(), fichier_ecriture() {
    frequence_ecriture = 0;
    ecriture_graphique = false;
    nb_iterations_arret = 0;
}

void SimulationIO::menu_general() {
    int opt = 0;
    bool sortir = false;
    while (!sortir) {
        cout << "-*- Menu Général -*-" << endl;
        cout << "Entrez le numéro de l'option choisie puis tapez sur Entrée."
        << endl;
        cout << " 1. Menu de lecture" << endl;
        cout << " 2. Menu d'écriture" << endl;
        cout << " 3. Créer une simulation" << endl;
        cout << " 4. Menu d'exécution" << endl;
        cout << " 5. Information sur l'application" << endl;
        cout << " 6. Sortir de l'application" << endl;
        while (opt < 1 || opt > 6) {
            cin >> opt;
        }
        if (opt == 1) {
            menu_lecture();
        } else if (opt == 2) {
            menu_ecriture();
        } else if (opt == 3) {
            menu_creation();
        } else if (opt == 4) {
            menu_execution();
        } else if (opt == 5) {
            information();
        } else if (opt == 6) {
            sortir = true;
        }
        opt = 0;
    }
}

void SimulationIO::menu_lecture() {
    int opt = 0;
    bool sortir = false, valide = false;
    while (!sortir) {
        cout << "-*- Menu de lecture -*-" << endl;
        cout << "Entrez le numéro de l'option choisie puis tapez sur Entrée."
        << endl;
        cout << " 1. Lire une simulation à partir d'un ensemble de fichiers" << endl;
        cout << " 2. Revenir en arrière" << endl;
        while (opt < 1 || opt > 2) {
            cin >> opt;
        }
        if (opt == 1) {
            cout << "Les fichiers de simulation doivent être nommés ";
            cout << "XXX_env.txt (fichier d'environnement) ";
            cout << "et XXX_agt.txt (fichier d'agents)." << endl;
            while (!valide) {
                cout << "Entrez la racine des fichiers (XXX dans l'exemple) : ";
                cin >> fichier_lecture;
                int stat = lire();
                if (stat == 0) {
                    cout << "Lecture complétée." << endl;
                    valide = true;
                    sortir = true;
                } else {
                    cout << "Une erreur s'est produite." << endl;
                    if (stat == 1) {
                        cout << "Vérifiez le fichier de configuration.";
                    } else if (stat == 2) {
                        cout << "Vérifiez le fichier d'environnement.";
                    } else if (stat == 3) {
                        cout << "Vérifiez le fichier d'agents.";
                    }
                    cout << endl;
                }
            }
        } else if (opt == 2) {
            sortir = true;
        }
        opt = 0;
    }
}

int SimulationIO::lire() {
    std::ifstream f_env(fichier_lecture + "_env.txt");
    std::ifstream f_agt(fichier_lecture + "_agt.txt");
    if (f_env.good() && f_agt.good()) {
        int hauteur, longueur, x, y;
        int nb, tevol, inst, isevol, isadj;
        int pv, pa, pollen, oeufx, oeufy;
        char t_case, t_agent, ori;
        Abeille *a;
        Fourmi *f;
        Fleur *fl;
        Puceron *p;
        OeufDePuceron *o;
        std::vector<Abeille *> *abeilles = new std::vector<Abeille *>();
        std::vector<Fourmi *> *fourmis = new std::vector<Fourmi *>();
        std::vector<Puceron *> *pucerons = new std::vector<Puceron *>();
        f_env >> longueur >> hauteur;
        Environnement *env = new Environnement(longueur, hauteur);
        while (f_env >> x) {
            f_env >> y >> t_case;
            switch (t_case) {
                case 'R':
                    env->setCaseAt(x, y, new Ruche(x, y));
                break;
                case 'F':
                    env->setCaseAt(x, y, new Fourmiliere(x, y));
                break;
                case 'a':
                    f_env >> tevol >> nb >> inst >> isevol;
                    env->setCaseAt(x, y,
                        new Alveole(
                            nb,
                            tevol,
                            static_cast<bool>(isevol),
                            Position(x, y),
                            inst));
                break;
                case 'f':
                    f_env >> tevol >> nb;
                    fl = new Fleur(nb, Position(x, y), tevol);
                    env->setCaseAt(x, y, fl);
                break;
                case 'o':
                    f_env >> tevol >> isevol >> isadj;
                    env->setCaseAt(x, y,
                        new OeufDePuceron(
                            !static_cast<bool>(isevol),
                            static_cast<bool>(isadj),
                            tevol,
                            Position(x, y)));
                break;
                default:
                break;
            }
        }
        f_env.close();
        while (f_agt >> t_agent) {
            f_agt >> x >> y >> ori >> pv >> pa;
            switch (t_agent) {
                case 'a':
                    f_agt >> pollen;
                    a = new Abeille(pv, Position(x, y, ori), pa, pollen);
                    abeilles->push_back(a);
                    env->setAgentAt(x, y, a);
                break;
                case 'f':
                    f_agt >> oeufx >> oeufy;
                    if (oeufx >= 0 && oeufy >= 0) {
                        o = static_cast<OeufDePuceron *>(env->getCaseAt(oeufx, oeufy));
                        f = new Fourmi(pv, Position(x, y, ori), pa, o);
                    } else {
                        f = new Fourmi(pv, Position(x, y, ori), pa, NULL);
                    }
                    fourmis->push_back(f);
                    env->setAgentAt(x, y, f);
                break;
                case 'p':
                    p = new Puceron(pv, Position(x, y, ori), pa);
                    pucerons->push_back(p);
                    env->setAgentAt(x, y, p);
                break;
                default:
                break;
            }
        }
        f_agt.close();
        simulation.set_environnement(env);
        simulation.set_abeilles(abeilles);
        simulation.set_fourmis(fourmis);
        simulation.set_pucerons(pucerons);
    } else {
        if (f_env.good()) f_env.close();
        else return 2;
        if (f_agt.good()) f_agt.close();
        else return 3;
    }

    return 0;
}

void SimulationIO::menu_ecriture() {
    int opt = 0;
    bool sortir = false;
    while (!sortir) {
        cout << "-*- Menu d'écriture -*-" << endl;
        if (!fichier_ecriture.empty()) {
            cout << "La racine des fichiers actuelle est " << fichier_ecriture << endl;
        }
        cout << "Entrez le numéro de l'option choisie puis tapez sur Entrée."
        << endl;
        cout << " 1. Configurer l'écriture de la simulation" << endl;
        cout << " 2. Ecrire la simulation à l'étape actuelle" << endl;
        cout << " 3. Revenir en arrière" << endl;
        while (opt < 1 || opt > 3) {
            cin >> opt;
        }
        if (opt == 1) {
            configuration_ecriture();
        } else if (opt == 2) {
            if (fichier_ecriture.empty()) {
                cout << "Les fichiers de simulation seront nommés ";
                cout << "XXX_env.txt (fichier d'environnement) ";
                cout << "et XXX_agt.txt (fichier d'agents)." << endl;
                cout << "Entrez la racine des fichiers (XXX dans l'exemple) : ";
                cin >> fichier_ecriture;
            }
            ecrire();
        } else if (opt == 3) {
            sortir = true;
        }
        opt = 0;
    }
}

void SimulationIO::configuration_ecriture() {
    int fe = -1;
    char opt = ' ';
    cout << "-*- Configuration de l'écriture -*-" << endl;
    cout << "Fréquence d'écriture de la simulation : ";
    while (fe < 0) {
        cin >> fe;
    }
    frequence_ecriture = fe;
    cout << "Activer l'écriture graphique de l'environnement ";
    cout << "et des agents ? L'écriture aura lieu avec la ";
    cout << "même fréquence que l'écriture de la simulation.";
    cout << "(O/n) " << endl;
    while (opt != 'O' && opt != 'n' && opt != 'o' && opt != 'N') {
        cin >> opt;
    }
    ecriture_graphique = (opt == 'O' || opt == 'o');
    cout << "Entrez la racine des fichiers d'écriture : ";
    cin >> fichier_ecriture;
}

void SimulationIO::ecrire() const {
    std::ofstream f_env(fichier_ecriture + "_env" + std::to_string(simulation.get_iterations()) + ".txt");
    std::ofstream f_agt(fichier_ecriture + "_agt" + std::to_string(simulation.get_iterations()) + ".txt");

    if (f_env.good() && f_agt.good()) {
        int hauteur, longueur, i, j;
        char s = ' ';
        const Environnement *env = simulation.get_environnement();
        hauteur = env->getHauteur();
        longueur = env->getLongueur();
        f_env << hauteur << s << longueur << endl;
        for (i = 0; i < hauteur; ++i) {
            for (j = 0; j < longueur; ++j) {
                Case *c = env->getCaseAt(j, i);
                if (c->getTypeCase() == string("Ruche")) {
                    f_env << j << s << i << s << 'R' << endl;
                } else if (c->getTypeCase() == string("Fourmiliere")) {
                    f_env << j << s << i << s << 'F' << endl;
                } else if (c->getTypeCase() == string("Alveole")) {
                    f_env << j << s << i << s << 'a' << s;
                    f_env << c->getTempsDEvol() << s;
                    f_env << c->getNbVoisins() << s;
                    f_env << c->getTempsCons() << s;
                    f_env << (c->getEnEvolution() ? '1' : '0');
                    f_env << endl;
                } else if (c->getTypeCase() == string("Fleur")) {
                    f_env << j << s << i << s << 'f' << s;
                    f_env << c->getTempsDEvol() << s;
                    f_env << c->getnbPollen();
                    f_env << endl;
                } else if (c->getTypeCase() == string("OeufDePuceron")) {
                    f_env << j << s << i << s << 'o' << s;
                    f_env << c->getTempsDEvol() << s;
                    f_env << (c->getPorte() ? '1' : '0') << s;
                    f_env << (c->getAdjacent() ? '1' : '0');
                    f_env << endl;
                }
            }
        }
        f_env.close();

        for (auto abeille : simulation.get_abeilles()) {
            f_agt << 'a' << s << abeille->getPosition().getX() << s;
            f_agt << abeille->getPosition().getY() << s;
            f_agt << abeille->getPosition().getOrientation() << s;
            f_agt << abeille->getPV() << s;
            f_agt << abeille->getPA() << s;
            f_agt << abeille->getPollen();
            f_agt << endl;
        }

        for (auto fourmi : simulation.get_fourmis()) {
            f_agt << 'f' << s << fourmi->getPosition().getX() << s;
            f_agt << fourmi->getPosition().getY() << s;
            f_agt << fourmi->getPosition().getOrientation() << s;
            f_agt << fourmi->getPV() << s;
            f_agt << fourmi->getPA() << s;
            Case *o = fourmi->getOeufPorte();
            if (o != NULL) {
                f_agt << o->getPosition().getX() << s;
                f_agt << o->getPosition().getY();
            } else {
                f_agt << '-' << '1' << ' ' << '-' << '1';
            }
            f_agt << endl;
        }
        
        for (auto puceron : simulation.get_pucerons()) {
            f_agt << 'p' << s << puceron->getPosition().getX() << s;
            f_agt << puceron->getPosition().getY() << s;
            f_agt << puceron->getPosition().getOrientation() << s;
            f_agt << puceron->getPV() << s;
            f_agt << puceron->getPA() << s;
            f_agt << endl;
        }
        f_agt.close();

        if (ecriture_graphique) {
            ecrire_graphique();
        }
    } else {
        if (f_env.good()) f_env.close();
        if (f_agt.good()) f_agt.close();
    }
}

void SimulationIO::menu_creation() {
    int hauteur, longueur, nb_abeilles,
    nb_fourmis, nb_pucerons, nb_pas = -1;
    char aleat;
    bool valide = false;
    cout << "Entrez le nombre d'itérations à effectuer avant arrêt : ";
    while (nb_pas < 0) {
        cin >> nb_pas;
    }
    nb_iterations_arret = nb_pas;
    while (!valide) {
        cout << "Entrez la hauteur de l'environnement : ";
        cin >> hauteur;
        cout << "Entrez la longueur de l'environnement : ";
        cin >> longueur;
        cout << "Entrez le nombre d'abeilles : ";
        cin >> nb_abeilles;
        cout << "Entrez le nombre de fourmis : ";
        cin >> nb_fourmis;
        cout << "Entrez le nombre de pucerons : ";
        cin >> nb_pucerons;
        if (hauteur * longueur > nb_abeilles + nb_fourmis + nb_pucerons
            && hauteur > 0 && longueur > 0
            && nb_abeilles >= 0 && nb_fourmis >= 0 && nb_pucerons >= 0) {
            valide = true;
        } else {
            cout << "Ces valeurs doivent être positives et ";
            cout << "le nombre total d'agents doit être strictement";
            cout << " inférieur à l'aire de l'environnement.";
            cout << endl;
        }
    }
    valide = false;
    while (!valide) {
        cout << "Initialiser aléatoirement ? (O/n) ";
        cin >> aleat;
        if (aleat == 'O' || aleat == 'o') {
            cout << "Initialisation de la simulation en cours..." << endl;
            // On détruit la simulation et on rappelle le constructeur sur le même emplacement mémoire
            (&simulation)->~Simulation();
            new (&simulation) Simulation(hauteur, longueur,
                nb_abeilles, nb_fourmis, nb_pucerons);
            valide = true;
        } else if (aleat == 'N' || aleat == 'n') {
            remplir_environnement_interactif(
                longueur, hauteur, nb_abeilles, nb_fourmis, nb_pucerons);
            valide = true;
        } else {
            cout << "Option non reconnue." << endl;
        }
    }
}

void SimulationIO::remplir_environnement_interactif(
    int longueur, int hauteur, int nb_abeilles,
    int nb_fourmis, int nb_pucerons) {
    int x, y, ori, i, j, tevol, nb, isevol, isadj, inst, pv, pa;
    char _case, _agent;
    Environnement *env = new Environnement(longueur, hauteur);
    std::vector<Abeille> *abeilles = new std::vector<Abeille>();
    std::vector<Fourmi> *fourmis = new std::vector<Fourmi>();
    std::vector<Puceron> *pucerons = new std::vector<Puceron>();

    cout << "Vous avez choisi de remplir l'environnement de façon";
    cout << " interactive.\nPour chaque case, entrez une lettre parmi :";
    cout << endl;
    cout << "R (ruche), F (fourmiliere), a (alveole), f (fleur), o";
    cout << " (oeuf de puceron), n (rien)" << endl;
    cout << "et une lettre parmi :" << endl;
    cout << "a (abeille), f (fourmi), p (puceron), n (rien)" << endl;

    cout << "Entrez le temps d'évolution commun pour les cases dynamiques : ";
    cin >> tevol;

    for (i = 0; i < hauteur; ++i) {
        for (j = 0; j < longueur; ++j) {
            cout << "Case " << j << ":" << i << endl;
            cout << "Entrez une lettre pour la case et un autre pour l'agent :";
            cin >> _case >> _agent;
            switch (_case) {
                case 'a':
                    cout << "Entrez l'état d'évolution (0 ou 1)";
                    cout << " et le temps courant d'évolution :";
                    cin >> isevol >> inst;
                    env->setCaseAt(x, y,
                        new Alveole(
                            0,
                            tevol,
                            static_cast<bool>(isevol),
                            Position(x, y),
                            inst));
                break;
                case 'f':
                    cout << "Entrez le nombre de pollen : ";
                    cin >> nb;
                    env->setCaseAt(x, y, new Fleur(nb, Position(x, y), tevol));
                break;
                case 'o':
                    cout << "Entrez l'état d'évolution (0 ou 1)";
                    cout << " et si l'oeuf est adjacent à une";
                    cout << " fourmilière (0 ou 1) : ";
                    cin >> isevol >> isadj;
                    env->setCaseAt(x, y,
                        new OeufDePuceron(
                            !static_cast<bool>(isevol),
                            static_cast<bool>(isadj),
                            tevol,
                            Position(x, y)));
                break;
                default:
                break;
            }
            switch (_agent) {
                case 'a':
                    cout << "Entrez l'orientation, les points de vie, ";
                    cout << "les points d'attaque et le nombre";
                    cout << " de pollen de l'abeille : ";
                    cin >> ori >> pv >> pa >> nb;
                    abeilles->push_back(Abeille(pv, Position(x, y), pa, nb));
                    env->setAgentAt(x, y, &abeilles->back());
                break;
                case 'f':
                    cin >> x >> y;
                    if (x >= 0 && y >= 0) {
                        OeufDePuceron *o = static_cast<OeufDePuceron *>(env->getCaseAt(x, y));
                        fourmis->push_back(Fourmi(pv, Position(x, y), pa, o));
                    } else {
                        fourmis->push_back(Fourmi(pv, Position(x, y), pa, NULL));
                    }
                    env->setAgentAt(x, y, &fourmis->back());
                break;
                case 'p':
                    pucerons->push_back(Puceron(pv, Position(x, y), pa));
                    env->setAgentAt(x, y, &pucerons->back());
                break;
                default:
                break;
            }
        }
    }
}

void SimulationIO::menu_execution() {
    int opt = 0, nb_pas = -1;
    bool sortir = false;
    while (!sortir) {
        cout << "-*- Configuration de l'exécution -*-" << endl;
        cout << "La simulation actuelle est à l'itération " << simulation.get_iterations() << endl;
        cout << "Le nombre de pas avant arrêt est de " << nb_iterations_arret << endl;
        cout << " 1. Configurer le nombre de pas avant arrêt" << endl;
        cout << " 2. Exécuter jusqu'à l'arrêt" << endl;
        cout << " 3. Revenir en arrière" << endl;
        while (opt < 1 || opt > 3) {
            cin >> opt;
        }
        if (opt == 1) {
            cout << "Entrez le nombre de pas à exécuter : " << endl;
            while (nb_pas < 0) {
                cin >> nb_pas;
            }
            nb_iterations_arret = nb_pas;
        } else if (opt == 2) {
            executer();
        } else if (opt == 3) {
            sortir = true;
        }
        opt = 0;
    }
}

void SimulationIO::executer() {
    int i, i_max = simulation.get_iterations() + nb_iterations_arret;
    ecrire();
    for (i = 0; i < i_max; ++i) {
        cout << "##### Itération n°" << i + 1 << " #####" << endl;
        simulation.executer_un();
        if (frequence_ecriture != 0 && i % frequence_ecriture == 0) {
            ecrire();
        }
    }
}

void SimulationIO::information() {
    std::ifstream info("README.md");
    if (info.good()) {
        cout << "README.md" << endl;
        cout << info.rdbuf();
    }
}

void SimulationIO::ecrire_graphique() const {
    string s_alveole("Alveole"), s_case("Case"), s_fleur("Fleur");
    string s_fourmiliere("Fourmiliere"), s_odp("OeufDePuceron");
    string s_ruche("Ruche");
    std::ofstream genv(fichier_ecriture + "_genv" + std::to_string(simulation.get_iterations()) + ".txt");
    std::ofstream gagt(fichier_ecriture + "_gagt" + std::to_string(simulation.get_iterations()) + ".txt");
    for (auto &l : simulation.get_environnement()->getGrille()) {
        for (auto c : l) {
            if (c->getTypeCase() == s_alveole) {
                genv << "a";
            } else if (c->getTypeCase() == s_case) {
                genv << "_";
            } else if (c->getTypeCase() == s_fleur) {
                genv << "f";
            } else if (c->getTypeCase() == s_fourmiliere) {
                genv << "F";
            } else if (c->getTypeCase() == s_odp) {
                genv << "o";
            } else if (c->getTypeCase() == s_ruche) {
                genv << "R";
            }
        }
        genv << endl;
    }

    string s_abeille("Abeille"), s_fourmi("Fourmi"), s_puceron("Puceron");
    for (auto &l : simulation.get_environnement()->getGrille()) {
        for (auto c : l) {
            if (c->getAgent() != NULL) {
                if (c->getAgent()->getTypeAgent() == string("Abeille")) {
                    gagt << "a";
                } else if (c->getAgent()->getTypeAgent() == string("Fourmi")) {
                    gagt << "f";
                } else if (c->getAgent()->getTypeAgent() == string("Puceron")) {
                    gagt << "p";
                }
            } else {
                gagt << "_";
            }
        }
        gagt << endl;
    }
}


